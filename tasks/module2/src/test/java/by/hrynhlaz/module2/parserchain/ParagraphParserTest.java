package by.hrynhlaz.module2.parserchain;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;
import by.hrynhlaz.module2.service.composite.TextLeaf;
import by.hrynhlaz.module2.controller.parserchain.ParagraphParser;
import by.hrynhlaz.module2.repository.ParagraphRepository;
import by.hrynhlaz.module2.repository.TextRepository;
import by.hrynhlaz.module2.service.ParagraphService;
import by.hrynhlaz.module2.service.TextService;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class ParagraphParserTest {
    private static final Logger LOGGER = Logger.getLogger(ParagraphParserTest.class);
    private TextService textService = new TextService(new TextRepository());
    private ParagraphService paragraphService = new ParagraphService(new ParagraphRepository());

    @Test
    public void parseLine() {
        String inputText = "\tIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n" +
                "\tIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.\n" +
                "\tIt is a established fact that a reader will be of a page when looking at its layout.\n" +
                "\tBye.";
        int actualCount = 0;
        int expectedCount = 4;

        ParagraphParser paragraphParser = new ParagraphParser(textService, paragraphService);
        LinkedTextComposite text = paragraphParser.parseLine(inputText);
        for (TextLeaf element : text.getTextElements()) {
            actualCount++;
        }

        assertEquals("Count of paragraphs: ", expectedCount, actualCount);
    }
}