package by.hrynhlaz.module2.repository;

import by.hrynhlaz.module2.entity.Paragraph;

public class ByParagraphParentIdSpecification implements Specification<Paragraph> {
    private long parentId;

    public ByParagraphParentIdSpecification(long parentId) {
        this.parentId = parentId;
    }

    @Override
    public boolean match(Paragraph entity) {
        if (Long.compare(this.parentId, entity.getTextId()) == 0) {
            return true;
        }
        return false;
    }
}
