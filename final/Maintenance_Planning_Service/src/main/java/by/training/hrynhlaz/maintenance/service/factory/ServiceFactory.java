package by.training.hrynhlaz.maintenance.service.factory;

import by.training.hrynhlaz.maintenance.service.UserAccountService;
import by.training.hrynhlaz.maintenance.service.UserProfileService;
import by.training.hrynhlaz.maintenance.service.UserRoleService;
import by.training.hrynhlaz.maintenance.service.impl.UserAccountServiceImpl;
import by.training.hrynhlaz.maintenance.service.impl.UserProfileServiceImpl;
import by.training.hrynhlaz.maintenance.service.impl.UserRoleServiceImpl;

public class ServiceFactory {
    private static final ServiceFactory instance = new ServiceFactory();

    private final UserAccountService userAccountService = new UserAccountServiceImpl();
    private final UserProfileService userProfileService = new UserProfileServiceImpl();
    private final UserRoleService userRoleService = new UserRoleServiceImpl();

    private ServiceFactory() {}

    public static ServiceFactory getInstance() {
        return instance;
    }

    public UserAccountService getUserAccountService() {
        return userAccountService;
    }
    public UserProfileService getUserProfileService() {
        return userProfileService;
    }
    public UserRoleService getUserRoleService() {
        return userRoleService;
    }
}
