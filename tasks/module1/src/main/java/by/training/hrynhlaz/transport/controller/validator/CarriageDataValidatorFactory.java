package by.training.hrynhlaz.transport.controller.validator;

import by.training.hrynhlaz.transport.entity.CarTypeEnum;

public class CarriageDataValidatorFactory {
    public CarriageDataValidator build(CarTypeEnum carType) {
        CarriageDataValidator validator = null;
        switch(carType) {
            case COMPARTMENT_CAR :
                validator = new CompartmentCarDataValidator();
                break;
            case RESERVED_BERTHS_CAR :
                validator = new ReservedBerthsCarDataValidator();
                break;
            case REGULAR_SEATS_CAR :
                validator = new RegularSeatsCarDataValidator();
        }
        return validator;
    }
}
