package by.training.hrynhlaz.maintenance.dao.impl;

import by.training.hrynhlaz.maintenance.dao.BayDao;
import by.training.hrynhlaz.maintenance.dao.exception.DaoException;
import by.training.hrynhlaz.maintenance.entity.Bay;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SqlBayDao extends BayDao {

    private static final String SELECT_BY_ID_QUERY =
            "SELECT bay_id, voltage_level_id, bay_name FROM bay " +
                    "WHERE bay_id = ?";
    private static final String INSERT_QUERY =
            "INSERT INTO bay (voltage_level_id, bay_name) VALUES(?, ?)";
    private static final String UPDATE_QUERY =
            "UPDATE bay SET voltage_level_id = ?, bay_name = ? WHERE bay_id = ?";
    private static final String DELETE_QUERY =
            "DELETE FROM bay WHERE bay_id = ?";
    private static final String SELECT_ALL_QUERY =
            "SELECT bay_id, voltage_level_id, bay_name FROM bay";

    @Override
    public List<Bay> findAll() throws DaoException {
        List<Bay> result = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = this.connection.prepareStatement(SELECT_ALL_QUERY);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Bay entity = new Bay();
                entity.setBayId(resultSet.getLong("bay_id"));
                entity.setBayName(resultSet.getString("bay_name"));
                entity.setVLevelId(resultSet.getLong("voltage_level_id"));
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public Long create(Bay entity) throws DaoException {
        long id = 0;
        PreparedStatement statement = null;
        try{
            statement = this.connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            int i = 0;
            statement.setLong(++i, entity.getVLevelId());
            statement.setString(++i, entity.getBayName());

            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if(generatedKeys.next()){
                id = generatedKeys.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return id;
    }

    @Override
    public boolean delete(long id) throws DaoException {
        PreparedStatement statement = null;
        boolean result = false;
        try {
            statement = connection.prepareStatement(DELETE_QUERY);
            statement.setLong(1, id);
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public boolean update(Bay entity) throws DaoException {
        PreparedStatement statement = null;
        boolean result = false;
        try {
            statement = this.connection.prepareStatement(UPDATE_QUERY);
            int i = 0;
            statement.setLong(++i, entity.getVLevelId());
            statement.setString(++i, entity.getBayName());
            statement.setLong(++i, entity.getBayId());
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public Bay findById(Long id) throws DaoException {
        PreparedStatement statement = null;
        Bay bay = null;
        try {
            statement = this.connection.prepareStatement(SELECT_BY_ID_QUERY);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()) {
                bay = new Bay();
                bay.setBayId(resultSet.getLong("bay_id"));
                bay.setBayName(resultSet.getString("bay_name"));
                bay.setVLevelId(resultSet.getLong("voltage_level_id"));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return bay;
    }

    @Override
    public void close(Statement statement) throws DaoException {
        super.close(statement);
    }
}
