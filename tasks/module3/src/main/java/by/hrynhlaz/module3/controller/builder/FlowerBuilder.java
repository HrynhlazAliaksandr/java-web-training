package by.hrynhlaz.module3.controller.builder;

import by.hrynhlaz.module3.service.FlowerModel;

import java.util.List;

public interface FlowerBuilder {
    List<FlowerModel> buildFlower(String path);
}
