package by.training.hrynhlaz.maintenance.dao.impl;

import by.training.hrynhlaz.maintenance.dao.UserProfileDao;
import by.training.hrynhlaz.maintenance.dao.exception.DaoException;
import by.training.hrynhlaz.maintenance.entity.UserProfile;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SqlUserProfileDao extends UserProfileDao {

    private static final String SELECT_BY_ID_QUERY =
            "SELECT user_id, first_name, last_name, email, phone_number FROM user_profile " +
                    "WHERE user_id = ?";
    private static final String INSERT_QUERY =
            "INSERT INTO user_profile(user_id, first_name, last_name, email, phone_number) VALUES(?, ?, ?, ?, ?)";
    private static final String UPDATE_QUERY =
            "UPDATE user_profile SET first_name = ?, last_name = ?, email = ?, phone_number = ? WHERE user_id = ?";
    private static final String DELETE_QUERY =
            "DELETE FROM user_profile WHERE user_id = ?";
    private static final String SELECT_ALL_QUERY =
            "SELECT user_id, first_name, last_name, email, phone_number FROM user_profile";

    @Override
    public List<UserProfile> findAll() throws DaoException {
        List<UserProfile> result = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = this.connection.prepareStatement(SELECT_ALL_QUERY);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                UserProfile entity = new UserProfile();
                entity.setUserId(resultSet.getLong("user_id"));
                entity.setFirstName(resultSet.getString("first_name"));
                entity.setLastName(resultSet.getString("last_name"));
                entity.setEmail(resultSet.getString("email"));
                entity.setPhoneNumber(resultSet.getString("phone_number"));
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public Long create(UserProfile entity) throws DaoException {
        long id = -1;
        PreparedStatement statement = null;
        try{
            statement = this.connection.prepareStatement(INSERT_QUERY);
            int i = 0;
            statement.setLong(++i, entity.getUserId());
            statement.setString(++i, entity.getFirstName());
            statement.setString(++i, entity.getLastName());
            statement.setString(++i, entity.getEmail());
            statement.setString(++i, entity.getPhoneNumber());
            statement.executeUpdate();

            id = entity.getUserId();

        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return id;
    }

    @Override
    public boolean delete(long id) throws DaoException {
        PreparedStatement statement = null;
        boolean result = false;
        try {
            statement = connection.prepareStatement(DELETE_QUERY);
            statement.setLong(1, id);
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public boolean update(UserProfile entity) throws DaoException {
        PreparedStatement statement = null;
        boolean result = false;
        try {
            statement = this.connection.prepareStatement(UPDATE_QUERY);
            int i = 0;
            statement.setString(++i, entity.getFirstName());
            statement.setString(++i, entity.getLastName());
            statement.setString(++i, entity.getEmail());
            statement.setString(++i, entity.getPhoneNumber());
            statement.setLong(++i, entity.getUserId());
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public UserProfile findById(Long id) throws DaoException {
        PreparedStatement statement = null;
        UserProfile userProfile = null;
        try {
            statement = this.connection.prepareStatement(SELECT_BY_ID_QUERY);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()) {
                userProfile = new UserProfile();
                userProfile.setUserId(resultSet.getLong("user_id"));
                userProfile.setFirstName(resultSet.getString("first_name"));
                userProfile.setLastName(resultSet.getString("last_name"));
                userProfile.setEmail(resultSet.getString("email"));
                userProfile.setPhoneNumber(resultSet.getString("phone_number"));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return userProfile;
    }

    @Override
    public void close(Statement statement) throws DaoException {
        super.close(statement);
    }
}
