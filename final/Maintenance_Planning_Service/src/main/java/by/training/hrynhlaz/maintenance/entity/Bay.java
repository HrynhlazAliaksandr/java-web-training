package by.training.hrynhlaz.maintenance.entity;

public class Bay extends Entity {
    private long bayId;
    private String bayName;
    private long vLevelId;

    public Bay() {}

    public Bay(long bayId, String bayName, long vLevelId) {
        this.bayId = bayId;
        this.bayName = bayName;
        this.vLevelId = vLevelId;
    }

    public long getBayId() {
        return bayId;
    }

    public void setBayId(long bayId) {
        this.bayId = bayId;
    }

    public String getBayName() {
        return bayName;
    }

    public void setBayName(String bayName) {
        this.bayName = bayName;
    }

    public long getVLevelId() {
        return vLevelId;
    }

    public void setVLevelId(long vLevelId) {
        this.vLevelId = vLevelId;
    }
}
