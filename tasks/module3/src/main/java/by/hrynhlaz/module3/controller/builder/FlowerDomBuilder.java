package by.hrynhlaz.module3.controller.builder;

import by.hrynhlaz.module3.entity.Flower;
import by.hrynhlaz.module3.service.BaseFlowerService;
import by.hrynhlaz.module3.service.composite.FlowerEnum;
import by.hrynhlaz.module3.service.FlowerModel;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class FlowerDomBuilder implements FlowerBuilder {
    private static final Logger LOGGER = Logger.getLogger(FlowerDomBuilder.class);
    private DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    private EnumSet<FlowerEnum> withText;
    private FlowerModel flower = null;
    private BaseFlowerService<Flower> flowerService;

    public FlowerDomBuilder(BaseFlowerService<Flower> flowerService) {
        this.flowerService = flowerService;
        withText = EnumSet.range(FlowerEnum.NAME, FlowerEnum. WATERING_PER_WEEK);
    }

    public List<FlowerModel> buildFlower(String fileName) {
        List<FlowerModel> flowers = new ArrayList<>();
        Document document = null;
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(fileName);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            LOGGER.error(e.getMessage());
            return flowers;
        }

        NodeList nodeList = document.getDocumentElement().getChildNodes();
        for(int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if(!(node instanceof Element)) {continue;}
            flower = new FlowerModel(flowerService);
            parseChild(node);
            flowers.add(flower);
            flower.print();
        }
        return flowers;
    }


    private void parseChild(Node node) {
        NodeList childNodes = node.getChildNodes();

        for (int i = 0; i < childNodes.getLength(); i++) {
            Node cNode = childNodes.item(i);
            if(!(cNode instanceof Element)) {continue;}

            if(cNode.hasChildNodes()) {
                parseChild(cNode);
            }

            String content = cNode.getLastChild().getTextContent().trim();
            FlowerEnum temp = FlowerEnum.valueOf(cNode.getNodeName().toUpperCase());
            if (withText.contains(temp)) {
                temp.setValue(flower, content);
            }

            switch (cNode.getNodeName()) {
                case "average_plant_size" :
                    flower.setAverageUnit(cNode.getAttributes().getNamedItem("unit").getNodeValue());
                    break;
                case "temperature" :
                    flower.setTemperatureUnit(cNode.getAttributes().getNamedItem("unit").getNodeValue());
                    break;
                case "watering_per_week" :
                    flower.setWateringUnit(cNode.getAttributes().getNamedItem("unit").getNodeValue());
            }
        }
    }

}
