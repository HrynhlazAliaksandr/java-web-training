package by.training.hrynhlaz.transport.controller.validator;

import by.training.hrynhlaz.transport.util.Verifying;

import java.util.Map;

public class RegularSeatsCarDataValidator extends CarriageDataValidator {
    @Override
    public ValidationResult validateCarProperty (Map<String, String> carPropertyList) {
        ValidationResult validationResult = super.validateCarProperty(carPropertyList);

        if (!carPropertyList.containsKey("vacantSeatsCount")
                || (carPropertyList.get("vacantSeatsCount") == null) ) {
            validationResult.addErrMessage(320, "Vacant Seats Count property exists");
        } else {
            if (!Verifying.checkIntegerValue(carPropertyList.get("vacantSeatsCount"))) {
                validationResult.addErrMessage(321,
                        "Invalid number format in vacantSeatsCount property");
            }
        }
        return validationResult;
    }
}