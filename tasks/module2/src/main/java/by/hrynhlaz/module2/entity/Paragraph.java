package by.hrynhlaz.module2.entity;

public class Paragraph {
    private long paragraphId;
    private long textId;

    public Paragraph() {
        paragraphId = -1;
    }

    public long getParagraphId() {
        return paragraphId;
    }

    public void setParagraphId(long paragraphId) {
        this.paragraphId = paragraphId;
    }

    public long getTextId() {
        return textId;
    }

    public void setTextId(long textId) {
        this.textId = textId;
    }
}
