package by.hrynhlaz.module2.repository;

import by.hrynhlaz.module2.entity.Paragraph;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class ParagraphRepository implements Repository<Paragraph> {
    private List<Paragraph> paragraphs = new LinkedList<>();
    private AtomicLong sequencer = new AtomicLong(0);


    @Override
    public long create(Paragraph entity) {
        if (entity.getParagraphId() == -1) {
            long newId = sequencer.incrementAndGet();
            entity.setParagraphId(newId);
            paragraphs.add(entity);
            return newId;
        }
        return -1;
    }

    @Override
    public Paragraph read(long id) {
        for (Paragraph paragraph : paragraphs) {
            if (paragraph.getParagraphId() == id) {
                return paragraph;
            }
        }
        return null;
    }

    @Override
    public List<Paragraph> find(Specification<Paragraph> spec) {
        List<Paragraph> result = new LinkedList<>();
        for (Paragraph paragraph : paragraphs) {
            if (spec.match(paragraph)) {
                result.add(paragraph);
            }
        }
        return result;
    }

    @Override
    public List<Paragraph> getAll() {
        return paragraphs;
    }

    @Override
    public void update(Paragraph entity) {

    }

    @Override
    public void delete(long id) {

    }


}
