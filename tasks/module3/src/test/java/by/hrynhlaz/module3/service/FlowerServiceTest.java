package by.hrynhlaz.module3.service;

import by.hrynhlaz.module3.repository.FlowerRepository;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class FlowerServiceTest {
    private static final Logger LOGGER = Logger.getLogger(FlowerServiceTest.class);

    @Test
    public void create() {
        long flowerId;
        FlowerRepository repository = new FlowerRepository();
        FlowerService flowerService = new FlowerService(repository);
        FlowerModel flower = new FlowerModel(flowerService);
        FlowerModel loadedFlower= new FlowerModel(flowerService);
        flower.setName("Begonia");
        flower.setSoil("soil");
        flower.setOrigin("Guadeloupe");
        flower.setStemColor("lime");
        flower.setLeafColor("Green");
        flower.setSize("25");
        flower.setAverageUnit("cm");
        flower.setTemperatureUnit("celsius");
        flower.setTemperature("32");
        flower.setWateringUnit("ml");
        flower.setWatering("250");
        flower.setLighting("photophilous");
        flower.setMultiplying("leaves");

        flowerId = flower.save(0);
        flower.print();

        LOGGER.info(flowerId);

        loadedFlower.load(flowerId);
        loadedFlower.print();

        assertTrue("Begonia".equals(loadedFlower.getName()));
    }
}