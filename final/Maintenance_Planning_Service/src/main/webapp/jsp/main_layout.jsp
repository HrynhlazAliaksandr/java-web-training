<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="lang" tagdir="/WEB-INF/tags" %>
<%@ page import="by.training.hrynhlaz.maintenance.controller.command.CommandType" %>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/layout" %>

<c:if test="${ empty pageContext.request.session.getAttribute('user') }">
        <jsp:forward page="/controller?command=${CommandType.SHOW_LOGIN_FORM_COMMAND}"/>
</c:if>

<c:choose>
    <c:when test="${not empty requestScope.get('lang')}">
        <fmt:setLocale value="${requestScope.get('lang')}"/>
        <c:set var="lng" value="${requestScope.get('lang')}" scope="application"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="${cookie['lang'].value}"/>
        <c:set var="lng" value="${cookie['lang'].value}" scope="application"/>
        </c:otherwise>
    </c:choose>

<fmt:setBundle basename="messages" scope="application"/>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Index</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
</head>

<body>

    <nav class="navbar  is-fixed-top is-white" role="navigation" aria-label="main navigation">
    <div class="container">
        <div id="navbar" class="navbar-menu">
            <div class="navbar-end">
            <%--
            <c:if test="${ user.getRole() eq 'admin' }">

                    <a class="navbar-item is-success" href="${pageContext.request.contextPath}/controller?command=${CommandEnum.VIEW_USERS}">
                        <fmt:message key="links.user.list" />
                    </a>

                    <a class="navbar-item is-warning" href="${pageContext.request.contextPath}/controller?command=view_users">
                        <fmt:message key="links.user.add" />
                    </a>

                <div class="navbar-item is-spaced"></div>
            </c:if>

            <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link" >
                    <c:out value="${user.getLogin()}" />
                </a>

                <div class="navbar-dropdown is-boxed">
                    <a class="navbar-item" href="${pageContext.request.contextPath}/controller?command=${CommandEnum.SHOW_USER_PROFILE}&id=${user.userId}">
                        Logout
                    </a>

                    <hr class="navbar-divider">
                    <a class="navbar-item" href="${pageContext.request.contextPath}/controller?command=${CommandEnum.LOGOUT}">
                        Logout
                    </a>
                </div>
            </div>
--%>
            <div class="navbar-item is-spaced"></div>

            <lang:lang></lang:lang>

        </div>
    </div>
</nav>









    <section class="hero is-dark is-bold is-medium">
        <div  class="hero-body" style="background-image:url('csm_2020.jpg'); background-position: bottom;">



            <div class="container">
                <h1 class="title">
                    <fmt:message key="app.title"/>
                </h1>
                <h2 class="subtitle">
                    <fmt:message key="app.subtitle"/>
                </h2>
            </div>
        </div>
<%--
        <div class="hero-foot">
            <nav class="tabs">
                <div class="container">

                    <c:choose>
                        <c:when test="${ user.getRole() eq 'admin' }" >
                            <jsp:include page="../jsp/admin_nav_bar.jsp"/>
                        </c:when>
                        <c:otherwise>
                            <jsp:include page="../jsp/user_nav_bar.jsp"/>
                        </c:otherwise>
                    </c:choose>
                </div>
            </nav>
        </div>
--%>
    </section>










    <section class="section">
    <div class="container">
      <h1 class="title">
        Hello World
      </h1>
      <p class="subtitle">
        My first website with <strong>Bulma</strong>!
      </p>

    <p>
    <c:if test="${not empty pageContext.request.session.getAttribute('user') }">
        <c:out value="${user.getLogin()}" />
</c:if>

<c:if test="${ empty pageContext.request.session.getAttribute('user') }">
        user is empty
</c:if>


<c:if test="${not empty pageContext.request.session.getAttribute('currentView') }">
        <c:out value="${currentView}" />
</c:if>


    </p>


    </div>
  </section>

<c:if test="${not empty pageContext.request.session.getAttribute('currentView') }">
        <c:out value="${currentView}" />
        <%--<jsp:include page="/jsp/login.jsp"/> --%>
        <jsp:include page="${currentView}"/>
</c:if>




    <section class="footer">
        <div class="content has-text-centered">

        </div>
    </section>
</body>
</html>




<%--
    <c:if test="${ not empty user }">
        <c:set var="currentView" value="main" scope="session"/>
        <jsp:forward page="/jsp/main_layout.jsp"/>
    </c:if>
        <jsp:forward page="/jsp/login.jsp"/>

--%>