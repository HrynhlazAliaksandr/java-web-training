package by.training.hrynhlaz.transport.entity;

public abstract class Carriage {
    CarTypeEnum carType;
    int carNumber;
    double baggageWeight;
    double baggageCapacity;

    protected Carriage() {
    }

    public void setBaggageWeight(double baggageWeight) {
        this.baggageWeight = baggageWeight;
    }

    public void setCarNumber(int carNumber) {
        this.carNumber = carNumber;
    }

    public double getBaggageCapacity() {
        return baggageCapacity;
    }

    public double getBaggageWeight() {
        return baggageWeight;
    }

    public int getCarNumber() {
        return carNumber;
    }

    public CarTypeEnum getCarType() {
        return carType;
    }

    public abstract int getSeatsCount();

    public abstract int getVacantSeatsCount();

    @Override
    public String toString() {
        String carInfo = "\nCar type : " + carType +
        "\nCar number : " + carNumber +
        "\nBaggage capacity : " + getBaggageCapacity() +
        "\nBaggage weight : " + getBaggageWeight();
        return carInfo;
    }
}