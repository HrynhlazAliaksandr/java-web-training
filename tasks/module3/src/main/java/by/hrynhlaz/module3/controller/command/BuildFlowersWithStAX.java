package by.hrynhlaz.module3.controller.command;

import by.hrynhlaz.module3.controller.builder.FlowerBuilder;
import by.hrynhlaz.module3.service.FlowerModel;

import java.util.List;

public class BuildFlowersWithStAX implements Command<FlowerModel>{
    private FlowerBuilder builder;

    public BuildFlowersWithStAX(FlowerBuilder builder) {
        this.builder = builder;
    }

    @Override
    public List<FlowerModel> execute(String path) {
        return builder.buildFlower(path);
    }
}
