package by.training.hrynhlaz.maintenance.dao.impl;

import by.training.hrynhlaz.maintenance.dao.DeviceDao;
import by.training.hrynhlaz.maintenance.dao.exception.DaoException;
import by.training.hrynhlaz.maintenance.entity.Device;
import by.training.hrynhlaz.maintenance.entity.UserProfile;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SqlDeviceDao extends DeviceDao {

    private static final String SELECT_BY_ID_QUERY =
            "SELECT device_id, device_name, bay_id FROM device " +
                    "WHERE device_id = ?";
    private static final String INSERT_QUERY =
            "INSERT INTO device(device_name, bay_id) VALUES(?, ?)";
    private static final String UPDATE_QUERY =
            "UPDATE device SET device_name = ?, bay_id = ? WHERE device_id = ?";
    private static final String DELETE_QUERY =
            "DELETE FROM device WHERE device_id = ?";
    private static final String SELECT_ALL_QUERY =
            "SELECT device_id, device_name, bay_id FROM device";

    @Override
    public List<Device> findAll() throws DaoException {
        List<Device> result = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = this.connection.prepareStatement(SELECT_ALL_QUERY);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Device entity = new Device();
                entity.setDeviceId(resultSet.getLong("device_id"));
                entity.setDeviceName(resultSet.getString("device_name"));
                entity.setBayId(resultSet.getLong("bay_id"));
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public Long create(Device entity) throws DaoException {
        long id = 0;
        PreparedStatement statement = null;
        try{
            statement = this.connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            int i = 0;
            statement.setString(++i, entity.getDeviceName());
            statement.setLong(++i, entity.getBayId());
            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if(generatedKeys.next()){
                id = generatedKeys.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return id;
    }

    @Override
    public boolean delete(long id) throws DaoException {
        PreparedStatement statement = null;
        boolean result = false;
        try {
            statement = connection.prepareStatement(DELETE_QUERY);
            statement.setLong(1, id);
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public boolean update(Device entity) throws DaoException {
        PreparedStatement statement = null;
        boolean result = false;
        try {
            statement = this.connection.prepareStatement(UPDATE_QUERY);
            int i = 0;
            statement.setString(++i, entity.getDeviceName());
            statement.setLong(++i, entity.getBayId());
            statement.setLong(++i, entity.getDeviceId());
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public Device findById(Long id) throws DaoException {
        PreparedStatement statement = null;
        Device device = null;
        try {
            statement = this.connection.prepareStatement(SELECT_BY_ID_QUERY);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()) {
                device = new Device();
                device.setDeviceId(resultSet.getLong("device_id"));
                device.setDeviceName(resultSet.getString("device_name"));
                device.setBayId(resultSet.getLong("bay_id"));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return device;
    }

    @Override
    public void close(Statement statement) throws DaoException {
        super.close(statement);
    }
}
