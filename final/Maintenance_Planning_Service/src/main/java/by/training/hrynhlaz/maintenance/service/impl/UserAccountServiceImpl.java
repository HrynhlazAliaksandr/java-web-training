package by.training.hrynhlaz.maintenance.service.impl;

import by.training.hrynhlaz.maintenance.dao.EntityTransaction;
import by.training.hrynhlaz.maintenance.dao.UserDao;
import by.training.hrynhlaz.maintenance.dao.UserProfileDao;
import by.training.hrynhlaz.maintenance.dao.exception.DaoException;
import by.training.hrynhlaz.maintenance.dao.factory.DaoFactory;
import by.training.hrynhlaz.maintenance.dto.UserAccountDto;
import by.training.hrynhlaz.maintenance.dto.UserDto;
import by.training.hrynhlaz.maintenance.entity.UserAccount;
import by.training.hrynhlaz.maintenance.entity.UserProfile;
import by.training.hrynhlaz.maintenance.service.UserAccountService;
import by.training.hrynhlaz.maintenance.service.exception.ServiceException;

import java.util.ArrayList;
import java.util.List;

public class UserAccountServiceImpl implements UserAccountService {
    @Override
    public UserAccountDto login(String login, String password) throws ServiceException {
        DaoFactory daoObjectFactory = DaoFactory.getInstance();
        UserDao userAccountDao = daoObjectFactory.getUserAccountDao();
        EntityTransaction entityTransaction = new EntityTransaction();
        UserAccountDto userAccount = null;

        try {
            entityTransaction.begin(false, userAccountDao);
            userAccount = userAccountDao.login(login, password);
        } catch (DaoException e) {
            throw new ServiceException(e);
        } finally {
            try {
                entityTransaction.end();
            } catch (DaoException e) {

            }
        }
        return userAccount;
    }

    @Override
    public List<UserDto> selectAll() throws ServiceException {
        DaoFactory daoObjectFactory = DaoFactory.getInstance();
        UserDao userAccountDao = daoObjectFactory.getUserAccountDao();
        EntityTransaction entityTransaction = new EntityTransaction();
        List<UserDto> users = new ArrayList<>();
        try {
            entityTransaction.begin(false, userAccountDao);
            users.addAll(userAccountDao.selectAll());
        } catch (DaoException e) {
            throw new ServiceException(e);
        } finally {
            try {
                entityTransaction.end();
            } catch (DaoException e) {

            }
        }
        return users;
    }

    @Override
    public boolean deleteAccount(long id) throws ServiceException {
        DaoFactory daoObjectFactory = DaoFactory.getInstance();
        UserDao userAccountDao = daoObjectFactory.getUserAccountDao();
        UserProfileDao userProfileDao = daoObjectFactory.getUserProfileDao();
        EntityTransaction entityTransaction = new EntityTransaction();
        boolean result = false;
        try {
            entityTransaction.begin(true, userAccountDao, userProfileDao);
            boolean result1 = userProfileDao.delete(id);
            boolean result2 = userAccountDao.delete(id);
            result = result1 && result2;
            if(result) {
                entityTransaction.commit();
            }
        } catch (DaoException e) {
            try {
                entityTransaction.rollback();
            } catch (DaoException ex) {}

            throw new ServiceException(e);
        } finally {
            try {
                entityTransaction.end();
            } catch (DaoException e) {

            }
        }
        return result;
    }

    @Override
    public boolean addAccount(UserAccount userAccount, UserProfile userProfile) throws ServiceException {
        DaoFactory daoObjectFactory = DaoFactory.getInstance();
        UserDao userAccountDao = daoObjectFactory.getUserAccountDao();
        UserProfileDao userProfileDao = daoObjectFactory.getUserProfileDao();
        EntityTransaction entityTransaction = new EntityTransaction();
        boolean result = false;
        try {
            entityTransaction.begin(true, userAccountDao, userProfileDao);
            long userId = userAccountDao.create(userAccount);
            userProfile.setUserId(userId);
            long profileId = userProfileDao.create(userProfile);
            result = (userId != -1) && (userId == profileId);
            if(result) {
                entityTransaction.commit();
            }
        } catch (DaoException e) {
            try {
                entityTransaction.rollback();
            } catch (DaoException ex) {}

            throw new ServiceException(e);
        } finally {
            try {
                entityTransaction.end();
            } catch (DaoException e) {

            }
        }
        return result;
    }

    @Override
    public UserAccount findById(long id) throws ServiceException {
        DaoFactory daoObjectFactory = DaoFactory.getInstance();
        UserDao userAccountDao = daoObjectFactory.getUserAccountDao();
        EntityTransaction entityTransaction = new EntityTransaction();
        UserAccount usersAccount = null;
        try {
            entityTransaction.begin(false, userAccountDao);
            usersAccount = userAccountDao.findById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        } finally {
            try {
                entityTransaction.end();
            } catch (DaoException e) {

            }
        }
        return usersAccount;
    }

    @Override
    public boolean updateAccount(UserAccount account) throws ServiceException {
        DaoFactory daoObjectFactory = DaoFactory.getInstance();
        UserDao userAccountDao = daoObjectFactory.getUserAccountDao();
        EntityTransaction entityTransaction = new EntityTransaction();
        boolean result = false;
        try {
            entityTransaction.begin(false, userAccountDao);
            result = userAccountDao.update(account);
        } catch (DaoException e) {
            throw new ServiceException(e);
        } finally {
            try {
                entityTransaction.end();
            } catch (DaoException e) {

            }
        }
        return result;
    }

    @Override
    public void logout(String login) throws ServiceException {

    }
}
