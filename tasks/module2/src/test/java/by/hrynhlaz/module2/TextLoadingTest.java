package by.hrynhlaz.module2;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;
import by.hrynhlaz.module2.controller.loaderchain.ParagraphLoader;
import by.hrynhlaz.module2.controller.loaderchain.SentenceLoader;
import by.hrynhlaz.module2.controller.loaderchain.WordLoader;
import by.hrynhlaz.module2.controller.parserchain.ParagraphParser;
import by.hrynhlaz.module2.controller.parserchain.SentenceParser;
import by.hrynhlaz.module2.controller.parserchain.WordParser;
import by.hrynhlaz.module2.repository.ParagraphRepository;
import by.hrynhlaz.module2.repository.SentenceRepository;
import by.hrynhlaz.module2.repository.TextRepository;
import by.hrynhlaz.module2.repository.WordRepository;
import by.hrynhlaz.module2.service.ParagraphService;
import by.hrynhlaz.module2.service.SentenceService;
import by.hrynhlaz.module2.service.TextService;
import by.hrynhlaz.module2.service.WordService;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertNotNull;

@RunWith(JUnit4.class)
public class TextLoadingTest {
    private static final Logger LOGGER = Logger.getLogger(TextLoadingTest.class);

    private TextService textService = new TextService(new TextRepository());
    private ParagraphService paragraphService = new ParagraphService(new ParagraphRepository());
    private SentenceService sentenceService = new SentenceService(new SentenceRepository());
    private WordService wordService = new WordService(new WordRepository());

    @Test
    public void printText() {
        String inputText ="\tIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n" +
                "\tIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.\n" +
                "\tIt is a established fact that a reader will be of a page when looking at its layout.\n" +
                "\tBye.";

        ParagraphParser paragraphParser = new ParagraphParser(textService, paragraphService);
        SentenceParser sentenceParser = new SentenceParser(paragraphService, sentenceService);
        WordParser wordParser = new WordParser(sentenceService, wordService);

        paragraphParser.linkWith(sentenceParser);
        sentenceParser.linkWith(wordParser);

        LinkedTextComposite text = paragraphParser.parseLine(inputText);

        LOGGER.info(text.getText());

        // Save and clear text model
        long textId = text.save(-1);
        text = null;

        // Load text entity from repository and build new composite text model
        ParagraphLoader paragraphLoader = new ParagraphLoader(textService, paragraphService);
        SentenceLoader sentenceLoader = new SentenceLoader(paragraphService, sentenceService);
        WordLoader wordLoader = new WordLoader(sentenceService, wordService);

        paragraphLoader.linkWith(sentenceLoader);
        sentenceLoader.linkWith(wordLoader);

        text = paragraphLoader.loadChain(textId);
        LOGGER.info(text.getText());

        assertNotNull(text);
    }

}
