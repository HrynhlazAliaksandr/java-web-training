package by.training.hrynhlaz.transport.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum CarTypeEnum {
    COMPARTMENT_CAR, RESERVED_BERTHS_CAR, REGULAR_SEATS_CAR;

    public static Optional<CarTypeEnum> fromString(String type){
        return	Stream.of(CarTypeEnum.values()).filter(t -> t.name().equalsIgnoreCase(type)).findFirst();
    }
}
