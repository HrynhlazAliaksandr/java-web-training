package by.training.hrynhlaz.transport.util;

public class Verifying {
    public static boolean checkIntegerValue(String value) {
        try{
            Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static boolean checkDoubleValue(String value) {
        try{
            Double.parseDouble(value);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
