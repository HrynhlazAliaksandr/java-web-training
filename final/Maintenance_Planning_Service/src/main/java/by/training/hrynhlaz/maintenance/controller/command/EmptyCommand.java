package by.training.hrynhlaz.maintenance.controller.command;

import by.training.hrynhlaz.maintenance.ConfigurationManager;
import by.training.hrynhlaz.maintenance.MessageManager;
import javax.servlet.http.HttpServletRequest;


public class EmptyCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute("nullPage",
                MessageManager.getProperty("message.nullPage"));
        return null;
    }
}
