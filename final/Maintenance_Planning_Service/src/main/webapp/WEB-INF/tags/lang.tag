<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="navbar-item">
  <div class="dropdown is-hoverable">

    <div class="dropdown-trigger">
      <button class="button" aria-haspopup="true" aria-controls="dropdown-menu4">
        <span>
        <c:choose>
          <c:when test="${lng == 'en'}">
            <fmt:message key="links.lang.en"/>
          </c:when>
          <c:otherwise>
            <fmt:message key="links.lang.ru"/>
          </c:otherwise>
        </c:choose>
      </span>
      <span class="icon is-small">
        <i class="fas fa-angle-down" aria-hidden="true"></i>
      </span>
      </button>
    </div>

    <div class="dropdown-menu" id="dropdown-menu4" role="menu">
      <div class="dropdown-content">
        <div class="dropdown-item">
          <a href="${pageContext.request.contextPath}/controller?lang=en" class="dropdown-item ${lng == 'en' ? 'is-active' : ''}">
            <fmt:message key="links.lang.en"/>
          </a>
        </div>

        <div class="dropdown-item">
          <a href="${pageContext.request.contextPath}/controller?lang=ru" class="dropdown-item ${lng == 'ru' ? 'is-active' : ''}">
            <fmt:message key="links.lang.ru"/>
          </a>
        </div>
      </div>
    </div>

  </div>
</div>





