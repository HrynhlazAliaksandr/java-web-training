package by.hrynhlaz.module2.controller.loaderchain;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;
import by.hrynhlaz.module2.entity.Sentence;
import by.hrynhlaz.module2.service.model.ParagraphModel;
import by.hrynhlaz.module2.repository.BySentenceParentIdSpecification;
import by.hrynhlaz.module2.service.ParagraphService;
import by.hrynhlaz.module2.service.SentenceService;
import java.util.List;

public class SentenceLoader extends TextLoader{
    private ParagraphService paragraphService;
    private SentenceService sentenceService;

    public SentenceLoader(ParagraphService paragraphService, SentenceService sentenceService) {
        this.paragraphService = paragraphService;
        this.sentenceService = sentenceService;
    }

    @Override
    public LinkedTextComposite loadChain(long parentId) {
        ParagraphModel paragraphModel = new ParagraphModel(paragraphService);
        List<Sentence> sentences = sentenceService.find(new BySentenceParentIdSpecification(parentId));
        for (Sentence sentence : sentences) {
            LinkedTextComposite sentenceModel = nextBuild(sentence.getSentenceId());
            paragraphModel.load(sentence.getSentenceId());
            paragraphModel.addText(sentenceModel);
        }
        return paragraphModel;
    }
}
