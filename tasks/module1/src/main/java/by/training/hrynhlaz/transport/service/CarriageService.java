package by.training.hrynhlaz.transport.service;

import by.training.hrynhlaz.transport.entity.Carriage;
import by.training.hrynhlaz.transport.repository.Repository;
import by.training.hrynhlaz.transport.repository.specification.Specification;
import org.apache.log4j.Logger;
import java.util.Comparator;
import java.util.List;

public class CarriageService implements Service<Carriage>{
    private static final Logger LOGGER = Logger.getLogger(CarriageService.class);
    private Repository carriageRepository;

    public CarriageService(Repository repository) {
        carriageRepository = repository;
    }

    @Override
    public void saveCarriage(Carriage carriage) {
        if(carriage != null) {
            carriageRepository.add(carriage);
            LOGGER.info(carriage.toString());
        } else {
            LOGGER.warn("Invalid car value in addCar void.");
        }
    }

    @Override
    public int calculateTotalPassengerCount() {
        int totalSeatsCount = 0;
        int vacantSeatsCount = 0;
        List<Carriage> carriageList = carriageRepository.getCarriageList();

        for(Carriage carriage : carriageList) {
            totalSeatsCount += carriage.getSeatsCount();
            vacantSeatsCount += carriage.getVacantSeatsCount();
        }
        return totalSeatsCount - vacantSeatsCount;
    }

    @Override
    public double calculateTotalBaggageWeight() {
        double weight = 0;
        List<Carriage> carriageList = carriageRepository.getCarriageList();

        for(Carriage carriage : carriageList) {
            weight += carriage.getBaggageWeight();
        }
        return weight;
    }

    @Override
    public List<Carriage> sort(Comparator<Carriage> comparator) {
        List<Carriage> sortedCarriageList = carriageRepository.getCarriageList();
        sortedCarriageList.sort(comparator);
        return sortedCarriageList;
    }

    @Override
    public List<Carriage> getCarriagesBySpec(Specification<Carriage> spec) {
        return carriageRepository.findBy(spec);
    }

}
