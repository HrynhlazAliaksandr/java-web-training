package by.training.hrynhlaz.maintenance.dao;

import by.training.hrynhlaz.maintenance.dao.exception.DaoException;
import org.apache.log4j.Logger;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class ConnectionPoolImpl implements CustomConnectionPool {
    private final static Logger LOGGER = Logger.getLogger(ConnectionPoolImpl.class);
    private static ConnectionPoolImpl instance = new ConnectionPoolImpl();
    private BlockingQueue<Connection> availableConnections;
    private Queue<Connection> usedConnections;
    private final static int DEFAULT_POOL_SIZE = 10;
    private final static String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private final static String URL = "jdbc:mysql://localhost:3306/maintenance_db";

    private ConnectionPoolImpl() {
        Properties prop = new Properties();
        prop.put("user", "root");
        prop.put("password", "alien1");
        prop.put("characterEncoding", "UTF-8");
        prop.put("useUnicode", "true");
        prop.put("serverTimezone", "UTC");

        try {
            Class.forName(JDBC_DRIVER);
        } catch (ClassNotFoundException e) {
            LOGGER.error("failed to register MySQL JDBC driver : ", e);
        }
        availableConnections = new LinkedBlockingDeque<>(DEFAULT_POOL_SIZE);
        usedConnections = new ArrayDeque<>();
        try {
            for(int i = 0; i < DEFAULT_POOL_SIZE; i++) {
                Connection connection = DriverManager.getConnection(URL, prop);
                Connection proxyConnection = new ProxyConnection(connection);
                availableConnections.add(proxyConnection);
            }
        } catch (SQLException e) {
            LOGGER.error("failed to get JDBC connection : ", e);
        }
    }

    public static ConnectionPoolImpl getInstance() {
        return instance;
    }

    @Override
    public Connection getConnection() throws DaoException {
        Connection connection = null;
        try {
            connection = availableConnections.take();
            usedConnections.offer(connection);
        } catch (InterruptedException e) {
            LOGGER.error(e);
            throw new DaoException();
        }
        return connection;
    }

    @Override
    public void releaseConnection(Connection connection) throws DaoException {
        if (!(connection instanceof ProxyConnection)) {
            throw new DaoException("bad connection");
        }
        usedConnections.remove(connection);
        availableConnections.offer(connection);
    }

    @Override
    public void destroyPool() {
        for(int i = 0; i < DEFAULT_POOL_SIZE; i++) {
            try {
                ProxyConnection connection = (ProxyConnection) availableConnections.take();
                connection.reallyClose();
            } catch (InterruptedException | SQLException e) {
                LOGGER.error(e);
            }
        }
        deregisterDrivers();
    }

    private void deregisterDrivers() {
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            try {
                DriverManager.deregisterDriver(drivers.nextElement());
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
    }

}
