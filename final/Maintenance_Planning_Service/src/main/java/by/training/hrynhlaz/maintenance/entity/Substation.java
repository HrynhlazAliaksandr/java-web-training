package by.training.hrynhlaz.maintenance.entity;

public class Substation extends Entity {
    private long substationId;
    private String substationName;

    public Substation() {}

    public Substation(long substationId, String substationName) {
        this.substationId = substationId;
        this.substationName = substationName;
    }

    public long getSubstationId() {
        return substationId;
    }

    public void setSubstationId(long substationId) {
        this.substationId = substationId;
    }

    public String getSubstationName() {
        return substationName;
    }

    public void setSubstationName(String substationName) {
        this.substationName = substationName;
    }

}
