package by.hrynhlaz.module3.controller;

import by.hrynhlaz.module3.controller.builder.FlowerStAXBuilder;
import by.hrynhlaz.module3.entity.Flower;
import by.hrynhlaz.module3.repository.FlowerRepository;
import by.hrynhlaz.module3.repository.Repository;
import by.hrynhlaz.module3.service.BaseFlowerService;
import by.hrynhlaz.module3.service.FlowerModel;
import by.hrynhlaz.module3.service.FlowerService;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FlowerStAXBuilderTest {

    @Test
    public void buildFlower() throws Exception {
        Repository<Flower> flowerRepository = new FlowerRepository();
        BaseFlowerService<Flower> flowerService = new FlowerService(flowerRepository);
        FlowerStAXBuilder builder = new FlowerStAXBuilder(flowerService);
        Path resourcePath = Paths.get(this.getClass().getClassLoader().getResource("Valid_Flowers.xml").toURI());
        List<FlowerModel> flowers = builder.buildFlower(resourcePath.toString());
        int expected = 10;
        int actual = flowers.size();
        assertEquals(expected, actual);
    }
}