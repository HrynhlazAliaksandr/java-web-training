package by.training.hrynhlaz.transport.controller.carbuilder;

import by.training.hrynhlaz.transport.controller.carbuilder.BaseCarBuilder;
import by.training.hrynhlaz.transport.entity.Carriage;
import by.training.hrynhlaz.transport.entity.CompartmentCar;
import org.apache.log4j.Logger;

import java.util.Map;

public class CompartmentCarBuilder extends BaseCarBuilder {
    private static Logger LOGGER = Logger.getLogger(CarBuilder.class);

    CompartmentCar newCar = new CompartmentCar();

    @Override
    public Carriage build(Map<String, String> propertyMap) {
        String newProperty;
        if ((newProperty = propertyMap.get("vacantLowerSeatsCount")) != null) {
            try{
                newCar.setVacantLowerSeatsCount(Integer.parseInt(newProperty));
            } catch (NumberFormatException e) {
                LOGGER.warn("\nInvalid number format in VacantLowerSeatsCount", e);
            }
        } else {
            LOGGER.warn("\nVacantLowerSeatsCount exists.");
            newCar.setVacantLowerSeatsCount(0);
        }

        if ((newProperty = propertyMap.get("vacantUpperSeatsCount")) != null) {
            try{
                newCar.setVacantUpperSeatsCount(Integer.parseInt(newProperty));
            } catch (NumberFormatException e) {
                LOGGER.warn("\nInvalid number format in VacantUpperSeatsCount property", e);
            }
        } else {
            LOGGER.warn("\nVacantUpperSeatsCount exists.");
            newCar.setVacantUpperSeatsCount(0);
        }

        if ((newProperty = propertyMap.get("carNumber")) != null) {
            try{
                newCar.setCarNumber(Integer.parseInt(newProperty));
            } catch (NumberFormatException e) {
                LOGGER.warn("\nInvalid number format in carNumber property", e);
            }
        } else {
            LOGGER.warn("\ncarNumber exists.");
            newCar.setCarNumber(0);
        }

        if ((newProperty = propertyMap.get("baggageWeight")) != null) {
            try{
                newCar.setBaggageWeight(Double.parseDouble(newProperty));
            } catch (NumberFormatException e) {
                LOGGER.warn("\nInvalid number format in baggageWeight property", e);
            }
        } else {
            LOGGER.warn("\nbaggageWeight property exists.");
            newCar.setBaggageWeight(0);
        }
        return newCar;
    }
}
