package by.training.hrynhlaz.maintenance.entity;

public class Device extends Entity {
    private long deviceId;
    private String deviceName;
    private long bayId;

    public Device() {}

    public Device(long deviceId, String deviceName, long bayId) {
        this.deviceId = deviceId;
        this.deviceName = deviceName;
        this.bayId = bayId;
    }

    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public long getBayId() {
        return bayId;
    }

    public void setBayId(long bayId) {
        this.bayId = bayId;
    }
}
