package by.hrynhlaz.module3.entity;

public class Flower {
    private long id;
    private String name;
    private String soil;
    private String origin;
    private String stem_color;
    private String leaf_color;
    private String averagePlantSize;
    private String averagePlantSizeUnit;
    private String temperature;
    private String temperatureUnit;
    private String lighting;
    private String wateringPerWeek;
    private String wateringPerWeekUnit;
    private String multiplying;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getStem_color() {
        return stem_color;
    }

    public void setStem_color(String stem_color) {
        this.stem_color = stem_color;
    }

    public String getLeaf_color() {
        return leaf_color;
    }

    public void setLeaf_color(String leaf_color) {
        this.leaf_color = leaf_color;
    }

    public String getAveragePlantSize() {
        return averagePlantSize;
    }

    public void setAveragePlantSize(String averagePlantSize) {
        this.averagePlantSize = averagePlantSize;
    }

    public String getAveragePlantSizeUnit() {
        return averagePlantSizeUnit;
    }

    public void setAveragePlantSizeUnit(String averagePlantSizeUnit) {
        this.averagePlantSizeUnit = averagePlantSizeUnit;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getTemperatureUnit() {
        return temperatureUnit;
    }

    public void setTemperatureUnit(String temperatureUnit) {
        this.temperatureUnit = temperatureUnit;
    }

    public String getLighting() {
        return lighting;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public String getWateringPerWeek() {
        return wateringPerWeek;
    }

    public void setWateringPerWeek(String wateringPerWeek) {
        this.wateringPerWeek = wateringPerWeek;
    }

    public String getWateringPerWeekUnit() {
        return wateringPerWeekUnit;
    }

    public void setWateringPerWeekUnit(String wateringPerWeekUnit) {
        this.wateringPerWeekUnit = wateringPerWeekUnit;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }
}
