package by.hrynhlaz.module2.service.model;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;
import by.hrynhlaz.module2.service.composite.TextLeaf;
import by.hrynhlaz.module2.entity.Sentence;
import by.hrynhlaz.module2.service.SentenceService;
import java.util.LinkedList;
import java.util.List;

public class SentenceModel extends LinkedTextComposite {
    private Sentence sentence = new Sentence();
    private SentenceService sentenceService;
    private List<TextLeaf> words = new LinkedList<>();

    public SentenceModel(SentenceService service) {
        super(TextLevel.SENTENCE);
        this.sentenceService = service;
    }

    @Override
    public void addText(TextLeaf text) {
        words.add(text);
    }

    @Override
    public List<TextLeaf> getTextElements() {
        return words;
    }

    @Override
    public TextLevel getLevel() {
        return super.getLevel();
    }

    @Override
    public String getText() {
        StringBuilder resultString = new StringBuilder();
        for (TextLeaf word : words) {
            resultString.append(word.getText());
        }
        return resultString.toString();
    }

    @Override
    public void load(long id) {
        sentence = sentenceService.read(id);
    }

    @Override
    public long save(long parentId) {
        sentence.setParagraphId(parentId);
        long sentenceId = sentenceService.create(sentence);
        for (TextLeaf word : words) {
            word.save(sentenceId);
        }
        return sentenceId;
    }

    @Override
    public int leafCount() {
        return words.size();
    }
}
