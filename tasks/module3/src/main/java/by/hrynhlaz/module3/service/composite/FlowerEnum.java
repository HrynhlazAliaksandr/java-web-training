package by.hrynhlaz.module3.service.composite;

import by.hrynhlaz.module3.service.FlowerModel;

public enum FlowerEnum {
    NAME("name") {
        public void setValue(FlowerModel flower, String value) {
            flower.setName(value);
        }
    },
    ORIGIN("origin") {
        public void setValue(FlowerModel flower, String value) {
            flower.setOrigin(value);
        }
    },
    SOIL("soil") {
        public void setValue(FlowerModel flower, String value) {
            flower.setSoil(value);
        }
    },
    MULTIPLYING("multiplying") {
        public void setValue(FlowerModel flower, String value) {
            flower.setMultiplying(value);
        }
    },
    STEM_COLOR("stem_color") {
        public void setValue(FlowerModel flower, String value) {
            flower.setStemColor(value);
        }
    },
    LEAF_COLOR("leaf_color") {
        public void setValue(FlowerModel flower, String value) {
            flower.setLeafColor(value);
        }
    },
    AVERAGE_PLANT_SIZE("average_plant_size") {
        public void setValue(FlowerModel flower, String value) {
            flower.setSize(value);
        }
    },
    TEMPERATURE("temperature") {
        public void setValue(FlowerModel flower, String value) {
            flower.setTemperature(value);
        }
    },
    LIGHTING("lighting") {
        public void setValue(FlowerModel flower, String value) {
            flower.setLighting(value);
        }
    },
    WATERING_PER_WEEK("watering_per_week") {
        public void setValue(FlowerModel flower, String value) {
            flower.setWatering(value);
        }
    },
    FLOWERS("flowers"),
    FLOWER("flower"),
    VISUAL_PARAMETERS("visual_parameters"),
    GROWINGTIPS("growingtips");

    private String value;
    private FlowerEnum(String value) {
        this.value = value;
    }

    public void setValue(FlowerModel flower, String value) {

    }
}