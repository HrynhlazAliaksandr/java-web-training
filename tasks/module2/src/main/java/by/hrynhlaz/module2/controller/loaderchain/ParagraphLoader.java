package by.hrynhlaz.module2.controller.loaderchain;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;
import by.hrynhlaz.module2.entity.Paragraph;
import by.hrynhlaz.module2.service.model.TextModel;
import by.hrynhlaz.module2.repository.ByParagraphParentIdSpecification;
import by.hrynhlaz.module2.service.ParagraphService;
import by.hrynhlaz.module2.service.TextService;
import java.util.List;

public class ParagraphLoader extends TextLoader{
    private TextService textService;
    private ParagraphService paragraphService;

    public ParagraphLoader(TextService textService, ParagraphService paragraphService) {
        this.textService = textService;
        this.paragraphService = paragraphService;
    }

    @Override
    public LinkedTextComposite loadChain(long parentId) {
        TextModel textModel = new TextModel(textService);
        textModel.load(parentId);
        List<Paragraph> paragraphs = paragraphService.find(new ByParagraphParentIdSpecification(parentId));
        for (Paragraph paragraph : paragraphs) {
            LinkedTextComposite paragraphModel = nextBuild(paragraph.getParagraphId());
            paragraphModel.load(paragraph.getParagraphId());
            textModel.addText(paragraphModel);
        }
        return textModel;
    }

}
