package by.training.hrynhlaz.maintenance.dto;

import by.training.hrynhlaz.maintenance.entity.Entity;

public class UserAccountDto extends Entity {
    private long userId;
    private String login;
    private String role;

    public UserAccountDto() {}

    public UserAccountDto(long userId, String login, String role) {
        this.userId = userId;
        this.login = login;
        this.role = role;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
