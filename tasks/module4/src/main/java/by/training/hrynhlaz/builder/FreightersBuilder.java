package by.training.hrynhlaz.builder;

import by.training.hrynhlaz.model.Box;
import by.training.hrynhlaz.model.Freighter;
import by.training.hrynhlaz.model.Harbor;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

public class FreightersBuilder {

    public enum XMLFreighterTags { FREIGHTERS, FREIGHTER, CARGO, BOX }
    private static final Logger LOGGER = Logger.getLogger(FreightersBuilder.class);
    private Harbor harbor;
    private FreighterHandler fh;
    private XMLReader reader;

    public FreightersBuilder() {
        fh = new FreighterHandler();
        try {
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(fh);
        } catch(SAXException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public Queue<Freighter> buildFreighter(String fileName) {
        try {
            reader.parse(fileName);
        } catch (SAXException e) {
            System.err.print("SAX parser exception: " + e);
        } catch (IOException e) {
            System.err.print("I/О exception: " + e);
        }
        return fh.getFreighters();
    }

    class FreighterHandler extends DefaultHandler {
        private Queue<Box> cargo;
        private Queue<Freighter> freighters = new LinkedList<>();
        int boxId;
        int freighterId;

        public Queue<Freighter> getFreighters() {
            return freighters;
        }

        public void startElement(String uri, String localName, String qName, Attributes attrs) {
            switch (XMLFreighterTags.valueOf(localName.toUpperCase())) {
                case BOX:
                    boxId = Integer.parseInt(attrs.getValue("box_id"));
                    break;
                case CARGO:
                    cargo = new LinkedList<>();
                    break;
                case FREIGHTER:
                    freighterId = Integer.parseInt(attrs.getValue("freighter_id"));
                    break;
            }
        }

        public void endElement(String uri, String localName, String qName) {
            Freighter freighter = null;
            switch (XMLFreighterTags.valueOf(localName.toUpperCase())) {
                case BOX:
                    cargo.add(new Box(boxId));
                    break;
                case FREIGHTER:
                    freighter = new Freighter(freighterId, cargo);
                    LOGGER.info("Freighter #" + freighterId + " add " + cargo.size() + " boxes");
                    for(Box box : cargo) {
                        LOGGER.info("box id: " + box.getBoxId());
                    }
                    freighters.add(freighter);
                    break;
            }
        }
    }
}
