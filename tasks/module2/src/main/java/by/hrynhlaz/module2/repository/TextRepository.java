package by.hrynhlaz.module2.repository;

import by.hrynhlaz.module2.entity.Text;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class TextRepository implements Repository<Text> {
    private List<Text> texts = new LinkedList<>();
    private AtomicLong sequencer = new AtomicLong(0);

    @Override
    public long create(Text entity) {
        if (entity.getTextId() == -1) {
            long newId = sequencer.incrementAndGet();
            entity.setTextId(newId);
            texts.add(entity);
            return newId;
        }
        return -1;
    }

    @Override
    public Text read(long id) {
        for (Text text : texts) {
            if (text.getTextId() == id) {
                return text;
            }
        }
        return null;
    }

    @Override
    public List<Text> find(Specification<Text> spec) {
        List<Text> result = new LinkedList<>();
        for (Text text : texts) {
            if (spec.match(text)) {
                result.add(text);
            }
        }
        return result;
    }

    @Override
    public List<Text> getAll() {
        return texts;
    }

    @Override
    public void update(Text entity) {

    }

    @Override
    public void delete(long id) {

    }

}
