package by.training.hrynhlaz.maintenance.dao;

import by.training.hrynhlaz.maintenance.dao.exception.DaoException;

import java.sql.Connection;
import java.sql.SQLException;

public class EntityTransaction {
    private Connection connection;
    private CustomConnectionPool connectionPool = ConnectionPoolImpl.getInstance();

    public void begin(boolean transaction, BaseDao dao, BaseDao ... daos) throws DaoException {
        connection = connectionPool.getConnection();
        if (transaction) {
            try {
                connection.setAutoCommit(false);
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        }

        dao.setConnection(connection);
        for(BaseDao daoElement : daos) {
            daoElement.setConnection(connection);
        }
    }

    public void end() throws DaoException {
        try {
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new DaoException(e);
        }

        connectionPool.releaseConnection(connection);
        connection = null;
    }

    public void commit() throws DaoException {
        try {
            connection.commit();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    public  void rollback() throws DaoException {
        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }
}
