package by.training.hrynhlaz.transport.controller.carbuilder;

import by.training.hrynhlaz.transport.entity.CarTypeEnum;
import by.training.hrynhlaz.transport.entity.Carriage;
import org.apache.log4j.Logger;

import java.util.Map;

public class CarBuilder {
    private static Logger LOGGER = Logger.getLogger(CarBuilder.class);

    CarBuilderFactory builderFactory = new CarBuilderFactory();
    BaseCarBuilder concreteCarBuilder;

    public Carriage buildCar(Map<String, String> carSpecification) {
        CarTypeEnum carType = null;
        Carriage carriage = null;
        if ((carSpecification != null) && (!carSpecification.isEmpty())) {
            try {
                carType = CarTypeEnum.valueOf(
                        carSpecification.get("type").toUpperCase());
            } catch (IllegalArgumentException e) {
                LOGGER.warn("Invalid Car Type parameter value", e);
            } catch (NullPointerException e) {
                LOGGER.warn("Invalid Car Type parameter value", e);
            }

            if (carType != null) {
                concreteCarBuilder = builderFactory.build(carType);
                carriage = concreteCarBuilder.build(carSpecification);
            }

        } else {
            LOGGER.warn("Missing сфк specification.");
        }
        return carriage;
    }
}
