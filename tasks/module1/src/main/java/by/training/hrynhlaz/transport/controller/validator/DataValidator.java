package by.training.hrynhlaz.transport.controller.validator;

import by.training.hrynhlaz.transport.entity.CarTypeEnum;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.Optional;

public class DataValidator {
    private static final Logger LOGGER = Logger.getLogger(DataValidator.class);

    public ValidationResult validateCarProperty(Map<String, String> entryMap) {
        CarriageDataValidator validator;
        ValidationResult validationResult = new ValidationResult();
        CarriageDataValidatorFactory validatorFactory = new CarriageDataValidatorFactory();
        CarTypeEnum carriageType;

        Optional<CarTypeEnum> validatorType = CarTypeEnum.fromString(entryMap.get("type").toUpperCase());

        if (validatorType.isPresent()) {
            validator = validatorFactory.build(validatorType.get());
        } else {
            validationResult.addErrMessage(300,
                    "Invalid Carriage type parameter value.");
            return validationResult;
        }

        return validator.validateCarProperty(entryMap);
    }
}