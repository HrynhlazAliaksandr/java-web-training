package by.training.hrynhlaz.maintenance.dao;

import by.training.hrynhlaz.maintenance.dao.exception.DaoException;
import by.training.hrynhlaz.maintenance.dto.UserDto;
import by.training.hrynhlaz.maintenance.entity.UserAccount;
import by.training.hrynhlaz.maintenance.dto.UserAccountDto;

import java.util.List;

public abstract class UserDao extends BaseDao<Long, UserAccount>{
    public abstract UserAccountDto login(String login, String password) throws DaoException;
    public abstract List<UserDto> selectAll() throws DaoException;
    public abstract void logout(String login) throws DaoException;
}
