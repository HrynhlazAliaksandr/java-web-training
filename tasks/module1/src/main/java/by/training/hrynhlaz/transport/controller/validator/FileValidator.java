package by.training.hrynhlaz.transport.controller.validator;

import org.apache.log4j.Logger;

import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileValidator {

    public ValidationResult ValidateFile(String pathToFile) {
        ValidationResult validationResult = new ValidationResult();
        Path path;

        if (pathToFile == null || pathToFile.isEmpty()) {
            validationResult.addErrMessage(100, "Missing file path");
            return validationResult;
        }

        try {
            path = Paths.get(pathToFile);
        } catch (InvalidPathException e) {
            validationResult.addErrMessage(101, "The path string contains invalid characters, "
                    + "or the path string is invalid for other file system specific reasons.");
            return validationResult;
        } catch (SecurityException e) {
            validationResult.addErrMessage(102, "The Security manager is invoked to check "
                    + "read access to the file.");
            return validationResult;
        }

        if (!Files.exists(path)) {
            validationResult.addErrMessage(103, "The file does not exist.");
        }

        return validationResult;
    }
}
