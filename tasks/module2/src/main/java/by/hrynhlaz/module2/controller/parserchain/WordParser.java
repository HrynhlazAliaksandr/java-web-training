package by.hrynhlaz.module2.controller.parserchain;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;
import by.hrynhlaz.module2.service.model.SentenceModel;
import by.hrynhlaz.module2.service.model.WordModel;
import by.hrynhlaz.module2.service.SentenceService;
import by.hrynhlaz.module2.service.WordService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordParser extends TextParser {
    private SentenceService sentenceService;
    private WordService wordService;

    public WordParser(SentenceService sentenceService, WordService wordService) {
        this.sentenceService = sentenceService;
        this.wordService = wordService;
    }

    @Override
    public LinkedTextComposite parseLine(String line) {
        String prefix;
        String word;
        String suffix;
        Pattern pat = Pattern.compile("([\\'\\(]+)?(\\w+|[\\-])([\\.\\,\\'\\)\\:]+)?(\\n)?");
        Matcher mat = pat.matcher(line);
        LinkedTextComposite sentence = new SentenceModel(sentenceService);

        while (mat.find()) {
            prefix = mat.group().replaceAll("(\\w+|[\\-])([\\.\\,\\'\\)\\:!]+)?(\\n)?", "");
            word = mat.group().replaceAll("([\\'\\(]+)|([\\.\\,\\'\\)\\:]+)?(\\n)?", "");
            suffix = mat.group().replaceAll("([\\'\\(]+)?(\\w+)|(\\n)|(\\-)", "");
            WordModel wordModel = new WordModel(wordService);
            wordModel.setPrefix(prefix);
            wordModel.setWord(word);
            wordModel.setSuffix(suffix);
            wordModel.setLastWord(mat.group().matches(".+\\n"));
            sentence.addTextNode(wordModel);
        }
        return sentence;
    }
}
