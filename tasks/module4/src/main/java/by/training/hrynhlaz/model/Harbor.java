package by.training.hrynhlaz.model;

import org.apache.log4j.Logger;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Harbor {
    private final static Logger LOGGER = Logger.getLogger(Harbor.class);
    private volatile static Harbor uniqueInstance;
    private final static int DOCK_COUNT = 4;
    private final Semaphore semaphore = new Semaphore(DOCK_COUNT);
    private final Queue<Dock> docks = new LinkedList<>();
    private Warehouse warehouse = new Warehouse();

    private Harbor() {
        for (int i = 0; i < DOCK_COUNT; i++) {
            this.docks.add(new Dock(i, warehouse));
        }
    }

    public static Harbor getInstance() {
        if (uniqueInstance == null) {
            synchronized (Harbor.class) {
                if (uniqueInstance == null) {
                    uniqueInstance = new Harbor();
                }
            }
        }
        return uniqueInstance;
    }

    public synchronized Dock getDock() throws DockException {
        final long maxWaitMillis = 10000;
        try {
            if(semaphore.tryAcquire(maxWaitMillis, TimeUnit.MILLISECONDS)) {
                Dock dock = docks.poll();
                return dock;
            }
        } catch(InterruptedException e) {
            throw new DockException(e);
        }
        throw new DockException(":timed out...");
    }

    public void returnDock(Dock dock) {
        docks.add(dock);
        semaphore.release();
    }
}
