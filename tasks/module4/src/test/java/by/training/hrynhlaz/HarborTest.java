package by.training.hrynhlaz;

import by.training.hrynhlaz.Controller.HarborController;
import by.training.hrynhlaz.builder.FreightersBuilder;
import by.training.hrynhlaz.model.Freighter;
import by.training.hrynhlaz.model.Harbor;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Queue;

@RunWith(JUnit4.class)
public class HarborTest {
    private final static Logger LOGGER = Logger.getLogger(HarborTest.class);

    @Test
    public void start() throws URISyntaxException, InterruptedException {
        Harbor harbor = Harbor.getInstance();
        FreightersBuilder builder = new FreightersBuilder();
        Path resourcePath = Paths.get(this.getClass().getClassLoader().getResource("Freighters.xml").toURI());
        HarborController harborController = new HarborController(harbor, builder);
        Queue<Freighter> freighters = harborController.loadFreighters(resourcePath.toString());
        harborController.goFreighters(freighters);

        Thread.sleep(10000);

    }
}