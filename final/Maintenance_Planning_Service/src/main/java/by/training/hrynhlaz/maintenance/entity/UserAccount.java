package by.training.hrynhlaz.maintenance.entity;

public class UserAccount extends Entity {
    private long userId;
    private String login;
    private String password;
    private long roleId;

    public UserAccount() {}

    public UserAccount(long userId, String login, String password, long roleId) {
        this.userId = userId;
        this.login = login;
        this.password = password;
        this.roleId = roleId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }
}
