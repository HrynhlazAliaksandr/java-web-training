package by.training.hrynhlaz.transport.controller;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataBuilder {
    private static final Logger LOGGER = Logger.getLogger(DataBuilder.class);
    public Map<String, String> build(String inputLine) {
        Map<String, String> carDataMap = new HashMap<>();

        if ((inputLine != null) && (!inputLine.isEmpty())) {
            String[] internalList = inputLine.split(",");
            for (int i = 0; i < internalList.length; i++) {
                String[] entry = internalList[i].split(":");
                carDataMap.put(entry[0], entry[1]);
            }
        } else {
            LOGGER.warn("Input line is empty.");
        }
        return carDataMap;
    }
}