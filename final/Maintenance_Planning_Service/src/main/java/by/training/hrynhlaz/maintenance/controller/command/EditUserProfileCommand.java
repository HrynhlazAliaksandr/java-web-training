package by.training.hrynhlaz.maintenance.controller.command;

import by.training.hrynhlaz.maintenance.ConfigurationManager;
import by.training.hrynhlaz.maintenance.dto.UserAccountDto;
import by.training.hrynhlaz.maintenance.entity.UserProfile;
import by.training.hrynhlaz.maintenance.service.UserProfileService;
import by.training.hrynhlaz.maintenance.service.exception.ServiceException;
import by.training.hrynhlaz.maintenance.service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class EditUserProfileCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
        boolean result = false;
        String page = null;
        ServiceFactory serviceFactory = ServiceFactory.getInstance();
        UserProfileService userProfileService = serviceFactory.getUserProfileService();
        HttpSession session = request.getSession();
        UserAccountDto user = (UserAccountDto) session.getAttribute("user");
        UserProfile currentProfile = new UserProfile();
        currentProfile.setUserId(user.getUserId());
        currentProfile.setFirstName(request.getParameter("firstName"));
        currentProfile.setLastName(request.getParameter("lastName"));
        currentProfile.setEmail(request.getParameter("email"));
        currentProfile.setPhoneNumber(request.getParameter("phoneNumber"));
        try {
            result = userProfileService.update(currentProfile);
        } catch (ServiceException e) {
            //
        }
        page = ConfigurationManager.getProperty("path.page.main");
        return page;
    }
}






