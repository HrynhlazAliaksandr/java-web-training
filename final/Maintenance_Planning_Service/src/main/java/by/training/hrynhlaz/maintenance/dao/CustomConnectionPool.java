package by.training.hrynhlaz.maintenance.dao;

import by.training.hrynhlaz.maintenance.dao.exception.DaoException;

import java.sql.Connection;

public interface CustomConnectionPool {

    Connection getConnection() throws DaoException;

    void releaseConnection(Connection connection) throws DaoException;

    void destroyPool();
}
