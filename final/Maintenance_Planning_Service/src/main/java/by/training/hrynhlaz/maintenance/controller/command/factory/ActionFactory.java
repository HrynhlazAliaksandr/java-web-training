package by.training.hrynhlaz.maintenance.controller.command.factory;

import by.training.hrynhlaz.maintenance.MessageManager;
import by.training.hrynhlaz.maintenance.controller.command.ActionCommand;
import by.training.hrynhlaz.maintenance.controller.command.CommandType;
import by.training.hrynhlaz.maintenance.controller.command.EmptyCommand;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class ActionFactory {
    public ActionCommand defineCommand(HttpServletRequest request) {
        ActionCommand current = new EmptyCommand();
        String action = request.getParameter("command");
        if (action == null || action.isEmpty()) {
            return current;
        }
        try {
            CommandType commandType = CommandType.valueOf(action.toUpperCase());
            current = commandType.getCommand();
        } catch(IllegalArgumentException e) {
            request.getSession().setAttribute("wrongAction", action +
                    MessageManager.getProperty("message.wrongAction"));
        }
        return current;
    }
}
