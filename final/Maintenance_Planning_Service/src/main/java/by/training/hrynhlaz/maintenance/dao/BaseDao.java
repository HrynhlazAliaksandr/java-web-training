package by.training.hrynhlaz.maintenance.dao;

import by.training.hrynhlaz.maintenance.dao.exception.DaoException;
import by.training.hrynhlaz.maintenance.entity.Entity;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public abstract class BaseDao <K, T extends Entity> {
    protected Connection connection;

    public abstract List<T> findAll() throws DaoException;
    public abstract K create(T entity) throws DaoException;
    public abstract boolean delete(long id) throws DaoException;
    public abstract boolean update(T entity) throws DaoException;
    public abstract T findById(K id) throws DaoException;

    public void close(Statement statement) throws DaoException {
        try{
            if(statement != null) {
                statement.close();
            }
        } catch(SQLException e) {
            throw new DaoException(e);
        }
    }

    void setConnection(Connection connection) {
        this.connection = connection;
    }

}
