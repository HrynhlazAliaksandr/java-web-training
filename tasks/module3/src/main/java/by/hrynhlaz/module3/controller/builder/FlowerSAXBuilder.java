package by.hrynhlaz.module3.controller.builder;

import by.hrynhlaz.module3.entity.Flower;
import by.hrynhlaz.module3.service.BaseFlowerService;
import by.hrynhlaz.module3.service.composite.FlowerEnum;
import by.hrynhlaz.module3.service.FlowerModel;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class FlowerSAXBuilder implements FlowerBuilder {
    private static final Logger LOGGER = Logger.getLogger(FlowerSAXBuilder.class);
    private FlowerHandler fh;
    private XMLReader reader;
    private BaseFlowerService<Flower> flowerService;

    public FlowerSAXBuilder(BaseFlowerService<Flower> flowerService) {
        this.flowerService = flowerService;
        fh = new FlowerHandler();
        try {
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(fh);
        } catch(SAXException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public List<FlowerModel> buildFlower(String fileName) {
        List<FlowerModel> flowers = new ArrayList<>();
        try {
            reader.parse(fileName);
            flowers = fh.getFlowers();
        } catch (SAXException e) {
            System.err.print("SAX parser exception: " + e);
        } catch (IOException e) {
            System.err.print("I/О exception: " + e);
        }
        return flowers;
    }


    class FlowerHandler extends DefaultHandler {
        private FlowerEnum currentEnum = null;
        private EnumSet<FlowerEnum> withText;
        private FlowerModel flower = null;
        private List<FlowerModel> flowers;



        public FlowerHandler() {
            flowers = new ArrayList<>();
            withText = EnumSet.range(FlowerEnum.NAME, FlowerEnum. WATERING_PER_WEEK);
        }

        public List<FlowerModel> getFlowers() {
            return flowers;
        }

        public void startElement(String uri, String localName, String qName, Attributes attrs) {
            if ("flower".equals(localName)) {
                flower = new FlowerModel(flowerService);
            }
            FlowerEnum temp = FlowerEnum.valueOf(localName.toUpperCase());
            if(withText.contains(temp)) {
                currentEnum = temp;
            } else {
                currentEnum = null;
            }

            if ("average_plant_size".equals(localName)) {
                flower.setAverageUnit(attrs.getValue("unit"));
            }

            if ("temperature".equals(localName)) {
                flower.setTemperatureUnit(attrs.getValue("unit"));
            }

            if ("watering_per_week".equals(localName)) {
                flower.setWateringUnit(attrs.getValue("unit"));
            }
        }

        public void endElement(String uri, String localName, String qName) {
            if ("flower".equals(localName)) {
                flowers.add(flower);
                flower.print();
            }
            if(currentEnum != null) {
                currentEnum = null;
            }
        }

        public void characters(char[] ch, int start, int length) {
            String s = new String(ch, start, length).trim();
            if(currentEnum != null) {
                currentEnum.setValue(flower, s);
            }
        }
    }
}
