package by.training.hrynhlaz.maintenance.controller.command;

import by.training.hrynhlaz.maintenance.ConfigurationManager;
import by.training.hrynhlaz.maintenance.entity.UserAccount;
import by.training.hrynhlaz.maintenance.entity.UserProfile;
import by.training.hrynhlaz.maintenance.service.UserAccountService;
import by.training.hrynhlaz.maintenance.service.UserProfileService;
import by.training.hrynhlaz.maintenance.service.exception.ServiceException;
import by.training.hrynhlaz.maintenance.service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;

public class EditUserAccountCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        boolean result = false;
        String page = null;
        ServiceFactory serviceFactory = ServiceFactory.getInstance();
        UserAccountService userAccountService = serviceFactory.getUserAccountService();
        UserProfileService userProfileService = serviceFactory.getUserProfileService();
        long editUserId = Long.parseLong(request.getParameter("editUserId"));
        UserAccount newUser = new UserAccount();
        newUser.setUserId(editUserId);
        newUser.setLogin(request.getParameter("login"));
        newUser.setPassword(request.getParameter("password"));
        newUser.setRoleId(Long.parseLong(request.getParameter("role")));
        UserProfile newProfile = new UserProfile();
        newProfile.setUserId(editUserId);
        newProfile.setFirstName(request.getParameter("firstName"));
        newProfile.setLastName(request.getParameter("lastName"));
        newProfile.setEmail(request.getParameter("email"));
        newProfile.setPhoneNumber(request.getParameter("phoneNumber"));

        try {
            boolean result1 = userAccountService.updateAccount(newUser);
            boolean result2 = userProfileService.update(newProfile);
            result = result1 && result2;
        } catch (ServiceException e) {
            //
        }

        if(result) {
            page = ConfigurationManager.getProperty("path.page.main");
        } else {
            page = ConfigurationManager.getProperty("path.page.main");
        }



        return page;
    }
}
