package by.hrynhlaz.module3.service.composite;

import by.hrynhlaz.module3.entity.Flower;

import java.util.List;

public interface FlowerComposite extends FlowerLeaf {
    void addText(FlowerLeaf flower);
    List<FlowerLeaf> getFlowerElements();
    int leafCount();
}
