package by.training.hrynhlaz.transport.controller.validator;

import by.training.hrynhlaz.transport.util.Verifying;
import java.util.Map;

public class CarriageDataValidator {

    public ValidationResult validateCarProperty (Map<String, String> carPropertyList) {
        ValidationResult validationResult = new ValidationResult();

        if (!carPropertyList.containsKey("baggageWeight")
                || (carPropertyList.get("baggageWeight") == null) ) {
            validationResult.addErrMessage(330, "Baggage Weight property exists");
        } else {
            if (!Verifying.checkDoubleValue(carPropertyList.get("baggageWeight"))) {
                validationResult.addErrMessage(331,
                        "Invalid number format in baggageWeight property");
            }
        }

        if (!carPropertyList.containsKey("carNumber")
                || (carPropertyList.get("carNumber") == null) ) {
            validationResult.addErrMessage(340, "Vacant carNumber property exists");
        } else {
            if (!Verifying.checkIntegerValue(carPropertyList.get("carNumber"))) {
                validationResult.addErrMessage(341,
                        "Invalid number format in carNumber property");
            }
        }
        return validationResult;
    }
}