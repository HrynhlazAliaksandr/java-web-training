package by.hrynhlaz.module3.service;

import by.hrynhlaz.module3.entity.Flower;
import by.hrynhlaz.module3.service.composite.FlowerLeaf;
import org.apache.log4j.Logger;
import java.util.HashMap;
import java.util.Map;

public class FlowerModel implements FlowerLeaf {
    private static final Logger LOGGER = Logger.getLogger(FlowerModel.class);
    private String name;
    private String origin;
    private PlantingSoil soil;
    private Multiplying multiplying;
    private Map<VisualParameter, Map<Parameter, String>> visualParameters;
    private Map<Growingtip, Map<Parameter, String>> growingtips;
    private BaseFlowerService<Flower> flowerService;

    public FlowerModel(BaseFlowerService<Flower> flowerService) {
        this.flowerService = flowerService;

        visualParameters = new HashMap<>();
        Map<Parameter, String> parameters = new HashMap<>();
        parameters.put(Parameter.NAME, "stem_color");
        parameters.put(Parameter.UNIT, "");
        parameters.put(Parameter.VALUE, "");
        visualParameters.put(VisualParameter.STEM_COLOR, parameters);
        parameters = new HashMap<>();
        parameters.put(Parameter.NAME, "leaf_color");
        parameters.put(Parameter.UNIT, "");
        parameters.put(Parameter.VALUE, "");
        visualParameters.put(VisualParameter.LEAF_COLOR, parameters);
        parameters = new HashMap<>();
        parameters.put(Parameter.NAME, "average_plant_size");
        parameters.put(Parameter.UNIT, "");
        parameters.put(Parameter.VALUE, "");
        visualParameters.put(VisualParameter.AVERAGE_PLANT_SIZE, parameters);

        growingtips = new HashMap<>();
        parameters = new HashMap<>();
        parameters.put(Parameter.NAME, "temperature");
        parameters.put(Parameter.UNIT, "");
        parameters.put(Parameter.VALUE, "");
        growingtips.put(Growingtip.TEMPERATURE, parameters);
        parameters = new HashMap<>();
        parameters.put(Parameter.NAME, "lighting");
        parameters.put(Parameter.UNIT, "");
        parameters.put(Parameter.VALUE, "");
        growingtips.put(Growingtip.LIGHTING, parameters);
        parameters = new HashMap<>();
        parameters.put(Parameter.NAME, "watering_per_week");
        parameters.put(Parameter.UNIT, "");
        parameters.put(Parameter.VALUE, "");
        growingtips.put(Growingtip.WATERING_PER_WEEK, parameters);
    }
    public void setAverageUnit(String unit) {
        Map<Parameter, String> parameters = visualParameters.get(VisualParameter.AVERAGE_PLANT_SIZE);
        parameters.replace(Parameter.UNIT, unit);
        visualParameters.replace(VisualParameter.AVERAGE_PLANT_SIZE, parameters);
    }

    public void setTemperatureUnit(String unit) {
        Map<Parameter, String> parameters = growingtips.get(Growingtip.TEMPERATURE);
        parameters.replace(Parameter.UNIT, unit);
        growingtips.replace(Growingtip.TEMPERATURE, parameters);
    }

    public void setWateringUnit(String unit) {
        Map<Parameter, String> parameters = growingtips.get(Growingtip.WATERING_PER_WEEK);
        parameters.replace(Parameter.UNIT, unit);
        growingtips.replace(Growingtip.WATERING_PER_WEEK, parameters);
    }

    public void setTemperature(String temperature) {
        Map<Parameter, String> parameters = growingtips.get(Growingtip.TEMPERATURE);
        parameters.replace(Parameter.VALUE, temperature);
        growingtips.replace(Growingtip.TEMPERATURE, parameters);
    }

    public void setLighting(String lighting) {
        Map<Parameter, String> parameters = growingtips.get(Growingtip.LIGHTING);
        parameters.replace(Parameter.VALUE, lighting);
        growingtips.replace(Growingtip.LIGHTING, parameters);
    }

    public void setWatering(String watering) {
        Map<Parameter, String> parameters = growingtips.get(Growingtip.WATERING_PER_WEEK);
        parameters.replace(Parameter.VALUE, watering);
        growingtips.replace(Growingtip.WATERING_PER_WEEK, parameters);
    }


    public void setSize(String size) {
        Map<Parameter, String> parameters = visualParameters.get(VisualParameter.AVERAGE_PLANT_SIZE);
        parameters.replace(Parameter.VALUE, size);
        visualParameters.replace(VisualParameter.AVERAGE_PLANT_SIZE, parameters);
    }

    public void  setStemColor(String color) {
        Map<Parameter, String> parameters = visualParameters.get(VisualParameter.STEM_COLOR);
        parameters.replace(Parameter.VALUE, color);
        visualParameters.replace(VisualParameter.STEM_COLOR, parameters);
    }

    public void  setLeafColor(String color) {
        Map<Parameter, String> parameters = visualParameters.get(VisualParameter.LEAF_COLOR);
        parameters.replace(Parameter.VALUE, color);
        visualParameters.replace(VisualParameter.LEAF_COLOR, parameters);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setSoil(String soil) {
        this.soil = PlantingSoil.valueOf(soil.toUpperCase());
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = Multiplying.valueOf(multiplying.toUpperCase());
    }

    @Override
    public String getFlower() {
        return null;
    }

    @Override
    public long save(long parentId) {
        Map<Parameter, String> parameters;
        Flower flower = new Flower();
        flower.setName(name);
        flower.setOrigin(origin);
        flower.setSoil(soil.getValue());
        flower.setMultiplying(multiplying.getValue());
        parameters = visualParameters.get(VisualParameter.STEM_COLOR);
        flower.setStem_color(parameters.get(Parameter.VALUE));
        parameters = visualParameters.get(VisualParameter.LEAF_COLOR);
        flower.setLeaf_color(parameters.get(Parameter.VALUE));
        parameters = visualParameters.get(VisualParameter.AVERAGE_PLANT_SIZE);
        flower.setAveragePlantSize(parameters.get(Parameter.VALUE));
        flower.setAveragePlantSizeUnit(parameters.get(Parameter.UNIT));
        parameters = growingtips.get(Growingtip.TEMPERATURE);
        flower.setTemperature(parameters.get(Parameter.VALUE));
        flower.setTemperatureUnit(parameters.get(Parameter.UNIT));
        parameters = growingtips.get(Growingtip.LIGHTING);
        flower.setLighting(parameters.get(Parameter.VALUE));
        parameters = growingtips.get(Growingtip.WATERING_PER_WEEK);
        flower.setWateringPerWeek(parameters.get(Parameter.VALUE));
        flower.setWateringPerWeekUnit(parameters.get(Parameter.UNIT));

        return flowerService.create(flower);
    }

    @Override
    public void load(long id) {
        Map<Parameter, String> parameters = new HashMap<>();
        Flower flower = flowerService.read(id);
        if (flower == null) {
            return;
        }
        name = flower.getName();
        origin = flower.getOrigin();
        soil = PlantingSoil.valueOf(flower.getSoil().toUpperCase());
        multiplying = Multiplying.valueOf(flower.getMultiplying().toUpperCase());
        parameters.put(Parameter.NAME, "stem_color");
        parameters.put(Parameter.UNIT, "");
        parameters.put(Parameter.VALUE, flower.getStem_color());
        visualParameters.put(VisualParameter.STEM_COLOR, parameters);
        parameters = new HashMap<>();
        parameters.put(Parameter.NAME, "leaf_color");
        parameters.put(Parameter.UNIT, "");
        parameters.put(Parameter.VALUE, flower.getLeaf_color());
        visualParameters.put(VisualParameter.LEAF_COLOR, parameters);
        parameters = new HashMap<>();
        parameters.put(Parameter.NAME, "average_plant_size");
        parameters.put(Parameter.UNIT, flower.getAveragePlantSizeUnit());
        parameters.put(Parameter.VALUE, flower.getAveragePlantSize());
        visualParameters.put(VisualParameter.AVERAGE_PLANT_SIZE, parameters);

        parameters = new HashMap<>();
        parameters.put(Parameter.NAME, "temperature");
        parameters.put(Parameter.UNIT, flower.getTemperatureUnit());
        parameters.put(Parameter.VALUE, flower.getTemperature());
        growingtips.put(Growingtip.TEMPERATURE, parameters);
        parameters = new HashMap<>();
        parameters.put(Parameter.NAME, "lighting");
        parameters.put(Parameter.UNIT, "");
        parameters.put(Parameter.VALUE, flower.getLighting());
        growingtips.put(Growingtip.LIGHTING, parameters);
        parameters = new HashMap<>();
        parameters.put(Parameter.NAME, "watering_per_week");
        parameters.put(Parameter.UNIT, flower.getWateringPerWeekUnit());
        parameters.put(Parameter.VALUE, flower.getWateringPerWeek());
        growingtips.put(Growingtip.WATERING_PER_WEEK, parameters);
    }

    public void print() {
        StringBuilder flower = new StringBuilder("\n --- FLOWER ---");
        flower.append("\nName: " + name);
        flower.append("\nOrigin: " + origin);
        flower.append("\nSoil: " + soil.getValue());
        flower.append("\nMultiplying: " + multiplying.getValue());
        flower.append("\n" + visualParameters.get(VisualParameter.STEM_COLOR).get(Parameter.NAME) + ": ");
        flower.append(visualParameters.get(VisualParameter.STEM_COLOR).get(Parameter.VALUE) + " ");
        flower.append(visualParameters.get(VisualParameter.STEM_COLOR).get(Parameter.UNIT) + " ");

        flower.append("\n" + visualParameters.get(VisualParameter.LEAF_COLOR).get(Parameter.NAME) + ": ");
        flower.append(visualParameters.get(VisualParameter.LEAF_COLOR).get(Parameter.VALUE) + " ");
        flower.append(visualParameters.get(VisualParameter.LEAF_COLOR).get(Parameter.UNIT) + " ");

        flower.append("\n" + visualParameters.get(VisualParameter.AVERAGE_PLANT_SIZE).get(Parameter.NAME) + ": ");
        flower.append(visualParameters.get(VisualParameter.AVERAGE_PLANT_SIZE).get(Parameter.VALUE) + " ");
        flower.append(visualParameters.get(VisualParameter.AVERAGE_PLANT_SIZE).get(Parameter.UNIT) + " ");

        flower.append("\n" + growingtips.get(Growingtip.TEMPERATURE).get(Parameter.NAME) + ": ");
        flower.append(growingtips.get(Growingtip.TEMPERATURE).get(Parameter.VALUE) + " ");
        flower.append(growingtips.get(Growingtip.TEMPERATURE).get(Parameter.UNIT) + " ");

        flower.append("\n" + growingtips.get(Growingtip.LIGHTING).get(Parameter.NAME) + ": ");
        flower.append(growingtips.get(Growingtip.LIGHTING).get(Parameter.VALUE) + " ");
        flower.append(growingtips.get(Growingtip.LIGHTING).get(Parameter.UNIT) + " ");

        flower.append("\n" + growingtips.get(Growingtip.WATERING_PER_WEEK).get(Parameter.NAME) + ": ");
        flower.append(growingtips.get(Growingtip.WATERING_PER_WEEK).get(Parameter.VALUE) + " ");
        flower.append(growingtips.get(Growingtip.WATERING_PER_WEEK).get(Parameter.UNIT) + " ");

        LOGGER.info(flower.toString());
    }

    enum PlantingSoil {
        PODZOLIC("podzolic"),
        SOIL("soil"),
        SODPODZOLIC("sodpodzolic");

        private String value;
        private PlantingSoil(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    enum Multiplying {
        LEAVES("leaves"),
        CUTTINGS("cuttings"),
        SEEDS("seeds");

        private String value;
        private Multiplying(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }





}
