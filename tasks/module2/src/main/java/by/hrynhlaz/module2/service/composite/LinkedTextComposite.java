package by.hrynhlaz.module2.service.composite;

public abstract class LinkedTextComposite implements TextComposite {
    private final TextLevel level;
    private LinkedTextComposite parent;
    private LinkedTextComposite lastChild;

    public LinkedTextComposite(TextLevel level) {
        this.level = level;
    }

    public abstract int leafCount();

    public void addTextNode(LinkedTextComposite node) {
        int compare = Integer.compare(this.level.level, node.level.level );
        switch (compare) {
            case 0:
                if (parent != null) {
                    node.parent = parent;
                    parent.addText(node);
                    parent.lastChild = node;
                }
                break;
            case 1:
                if (parent != null) {
                    parent.addTextNode(node);
                }
                break;
            case -1:
                if (lastChild == null) {
                    node.parent = this;
                    addText(node);
                    this.lastChild = node;
                } else {
                    lastChild.addTextNode(node);
                }
                break;
            default:
                //
        }
    }

    public TextLevel getLevel() {
        return this.level;
    }

    public enum TextLevel {
        TEXT("TextModel", 0),
        PARAGRAPH("ParagraphModel", 1),
        SENTENCE("SentenceModel", 2),
        WORD("WordModel", 3);

        private final int level;
        private final String title;

        TextLevel(String title, int level) {
            this.title = title;
            this.level = level;
        }
        public int getLevel() {
            return this.level;
        }
        public String getTitle() {
            return title;
        }
    }

}
