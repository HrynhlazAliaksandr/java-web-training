package by.hrynhlaz.module2.controller.parserchain;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;

public abstract class TextParser implements ParserChain<LinkedTextComposite>{

    private TextParser next;

    @Override
    public ParserChain<LinkedTextComposite> linkWith(ParserChain<LinkedTextComposite> next) {
        this.next = (TextParser) next;
        return this.next;
    }

    LinkedTextComposite nextParse(String line) {
        if (next != null) {
            return next.parseLine(line);
        } else {
            return null;
        }
    }
}
