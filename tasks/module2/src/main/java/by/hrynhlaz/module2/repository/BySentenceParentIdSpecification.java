package by.hrynhlaz.module2.repository;

import by.hrynhlaz.module2.entity.Sentence;

public class BySentenceParentIdSpecification implements Specification<Sentence> {
    private long parentId;

    public BySentenceParentIdSpecification(long parentId) {
        this.parentId = parentId;
    }

    @Override
    public boolean match(Sentence entity) {
        if (Long.compare(this.parentId, entity.getParagraphId()) == 0) {
            return true;
        }
        return false;
    }
}
