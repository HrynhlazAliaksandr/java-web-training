package by.training.hrynhlaz.maintenance.dao.impl;

import by.training.hrynhlaz.maintenance.dao.UserRoleDao;
import by.training.hrynhlaz.maintenance.dao.exception.DaoException;
import by.training.hrynhlaz.maintenance.entity.UserRole;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SqlUserRoleDao extends UserRoleDao {

    private static final String SELECT_BY_ID_QUERY =
            "SELECT role_id, role_name FROM user_role " +
                    "WHERE role_id = ?";
    private static final String INSERT_QUERY =
            "INSERT INTO user_role (role_name) VALUES(?)";
    private static final String UPDATE_QUERY =
            "UPDATE user_role SET role_name = ? WHERE role_id = ?";
    private static final String DELETE_QUERY =
            "DELETE FROM user_role WHERE role_id = ?";
    private static final String SELECT_ALL_QUERY =
            "SELECT role_id, role_name FROM user_role";

    @Override
    public List<UserRole> findAll() throws DaoException {
        List<UserRole> result = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = this.connection.prepareStatement(SELECT_ALL_QUERY);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                UserRole entity = new UserRole();
                entity.setRoleId(resultSet.getLong("role_id"));
                entity.setRole(resultSet.getString("role_name"));
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public Long create(UserRole entity) throws DaoException {
        long id = 0;
        PreparedStatement statement = null;
        try{
            statement = this.connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            int i = 0;
            statement.setString(++i, entity.getRole());
            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if(generatedKeys.next()){
                id = generatedKeys.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return id;
    }

    @Override
    public boolean delete(long id) throws DaoException {
        PreparedStatement statement = null;
        boolean result = false;
        try {
            statement = connection.prepareStatement(DELETE_QUERY);
            statement.setLong(1, id);
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public boolean update(UserRole entity) throws DaoException {
        PreparedStatement statement = null;
        boolean result = false;
        try {
            statement = this.connection.prepareStatement(UPDATE_QUERY);
            int i = 0;
            statement.setString(++i, entity.getRole());
            statement.setLong(++i, entity.getRoleId());
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public UserRole findById(Long id) throws DaoException {
        PreparedStatement statement = null;
        UserRole userRole = null;
        try {
            statement = this.connection.prepareStatement(SELECT_BY_ID_QUERY);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()) {
                userRole = new UserRole();
                userRole.setRoleId(resultSet.getLong("role_id"));
                userRole.setRole(resultSet.getString("role_name"));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return userRole;
    }

    @Override
    public void close(Statement statement) throws DaoException {
        super.close(statement);
    }
}
