package by.training.hrynhlaz.maintenance.service;

import by.training.hrynhlaz.maintenance.entity.UserProfile;
import by.training.hrynhlaz.maintenance.service.exception.ServiceException;

public interface UserProfileService {
    UserProfile findById(Long id) throws ServiceException;
    long addProfile(UserProfile newProfile) throws ServiceException;
    boolean update(UserProfile userProfile) throws ServiceException;
}
