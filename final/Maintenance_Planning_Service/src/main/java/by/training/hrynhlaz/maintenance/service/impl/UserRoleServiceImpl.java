package by.training.hrynhlaz.maintenance.service.impl;

import by.training.hrynhlaz.maintenance.dao.EntityTransaction;
import by.training.hrynhlaz.maintenance.dao.UserRoleDao;
import by.training.hrynhlaz.maintenance.dao.exception.DaoException;
import by.training.hrynhlaz.maintenance.dao.factory.DaoFactory;
import by.training.hrynhlaz.maintenance.entity.UserRole;
import by.training.hrynhlaz.maintenance.service.UserRoleService;
import by.training.hrynhlaz.maintenance.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class UserRoleServiceImpl implements UserRoleService {
    private final static Logger LOGGER = Logger.getLogger(UserRoleServiceImpl.class);

    @Override
    public List<UserRole> findAll() throws ServiceException {
        DaoFactory daoObjectFactory = DaoFactory.getInstance();
        UserRoleDao userRoleDao = daoObjectFactory.getUserRoleDao();
        EntityTransaction entityTransaction = new EntityTransaction();
        List<UserRole> roles = new ArrayList<>();
        try {
            entityTransaction.begin(false, userRoleDao);
            roles.addAll(userRoleDao.findAll());
        } catch (DaoException e) {
            LOGGER.error(" inside the DAO ----- " + e.getMessage());
            throw new ServiceException(e);
        } finally {
            try {
                entityTransaction.end();
            } catch (DaoException e) {

            }
        }
        return roles;
    }
}
