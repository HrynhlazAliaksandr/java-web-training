package by.training.hrynhlaz.model;

import org.apache.log4j.Logger;
import java.util.LinkedList;
import java.util.Queue;

public class Freighter extends Thread {
    private final static Logger LOGGER = Logger.getLogger(Freighter.class);
    private final static int CAPACITY = 4;
    private Queue<Box> hold = new LinkedList<>();
    private boolean reading = false;
    private Harbor harbor;
    private int freighterId;

    public Freighter(int freighterId, Queue<Box> cargo) {
        this.freighterId = freighterId;
        if (!cargo.isEmpty()) {
            this.hold.addAll(cargo);
        }
    }

    public void setHarbor(Harbor harbor) {
       this.harbor = harbor;
    }

    private void loadFreighter(Warehouse storage) {
        try {
            for (int i = 0; i < CAPACITY; i++) {
                Box box = storage.UnloadFromRack();
                if (box == null) {
                    return;
                }
                hold.add(box);
                LOGGER.info("Freighter #" + this.freighterId
                        + " loaded box with id: " + box.getBoxId());
            }
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private void unloadFreighter(Warehouse storage) {
        int factCapacity = hold.size();
        try {
            for (int i = 0; i < factCapacity; i++) {
                Box box = hold.poll();
                storage.loadOnRack(box);
                LOGGER.info("Freighter #" + freighterId
                        + " uploaded box with id: " + box.getBoxId());
            }
        } catch(InterruptedException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public void run() {
        Dock dock = null;
        LOGGER.info("New Freighter started");
        try {
            dock = harbor.getDock();
            if(dock == null) {
                LOGGER.error("dock = null");
            }
            reading = true;
            LOGGER.info("Freighter #" + this.freighterId
                    + " took dock #" + dock.getDockId());
            if (hold.isEmpty()) {
                loadFreighter(dock.getWarehouse());
            } else {
                unloadFreighter(dock.getWarehouse());
            }
        } catch(DockException e) {
            LOGGER.error("Freighter #" + this.freighterId + " lost ->"
                    + e.getMessage());
        } finally {
            if (dock != null) {
                reading = false;
                LOGGER.info("Freighter #" + this.freighterId + " : "
                        + dock.getDockId() + " dock released");
                harbor.returnDock(dock);
            }
        }
    }

}
