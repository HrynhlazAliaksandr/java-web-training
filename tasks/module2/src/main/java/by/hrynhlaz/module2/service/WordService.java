package by.hrynhlaz.module2.service;

import by.hrynhlaz.module2.entity.Word;
import by.hrynhlaz.module2.repository.Specification;
import by.hrynhlaz.module2.repository.WordRepository;
import java.util.List;

public class WordService implements BaseTextService<Word>{
    private WordRepository wordRepository;

    public WordService(WordRepository repository) {
        wordRepository = repository;
    }

    @Override
    public long create(Word entity) {
        return wordRepository.create(entity);
    }

    @Override
    public Word read(long id) {
        return wordRepository.read(id);
    }

    @Override
    public void update(Word entity) {
        wordRepository.update(entity);
    }

    @Override
    public void delete(long id) {
        wordRepository.delete(id);
    }

    @Override
    public List<Word> find(Specification<Word> spec) {
        return wordRepository.find(spec);
    }

    @Override
    public List<Word> getAll() {
        return wordRepository.getAll();
    }
}
