package by.training.hrynhlaz.transport.controller;

import by.training.hrynhlaz.transport.controller.carbuilder.CarBuilder;
import by.training.hrynhlaz.transport.controller.validator.DataValidator;
import by.training.hrynhlaz.transport.controller.validator.FileValidator;
import by.training.hrynhlaz.transport.controller.validator.ValidationResult;
import by.training.hrynhlaz.transport.service.Service;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

public class CarriageController {
    private static final Logger LOGGER = Logger.getLogger(CarriageController.class);
    private Service carriageService;

    public CarriageController(Service carriageService) {
        this.carriageService = carriageService;
    }

    public ValidationResult loadCarriagesFromFile(String path) {
        DataBuilder dataBuilder = new DataBuilder();
        DataValidator dataValidator = new DataValidator();
        CarBuilder carBuilder = new CarBuilder();
        ValidationResult validationResult;

        validationResult = new FileValidator().ValidateFile(path);
        if(!validationResult.isValid()) {
            return validationResult;
        }

        List<String> carriageDataList = new DataFileReader().readData(path);

        if(carriageDataList == null || carriageDataList.isEmpty()) {
            validationResult = new ValidationResult();
            validationResult.addErrMessage(101, "carriageDataList is empty.");
            return validationResult;
        }

        for(String line : carriageDataList) {
            Map<String, String> carriageDataMap = dataBuilder.build(line);
            validationResult = dataValidator.validateCarProperty(carriageDataMap);
            if (validationResult.isValid()) {
                carriageService.saveCarriage(carBuilder.buildCar(carriageDataMap));
            } else {
                return validationResult;
            }
        }
        return validationResult;
    }
}
