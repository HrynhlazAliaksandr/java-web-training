<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="lang" tagdir="/WEB-INF/tags" %>

<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/layout" %>


<c:choose>
    <c:when test="${not empty requestScope.get('lang')}">
        <fmt:setLocale value="${requestScope.get('lang')}"/>
        <c:set var="lng" value="${requestScope.get('lang')}" scope="application"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="${cookie['lang'].value}"/>
        <c:set var="lng" value="${cookie['lang'].value}" scope="application"/>
        </c:otherwise>
    </c:choose>
<fmt:setBundle basename="messages" scope="application"/>


<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
        <link rel="stylesheet" href="${ pageContext.request.contextPath }/static/bulma.css">
    </head>
    <body>
        <section class="hero is-dark is-bold is-medium">
            <div  class="hero-body" style="background-image:url('${pageContext.request.contextPath}/static/csm_2020.jpg'); background-position: bottom; box-shadow:0 0 0 300px rgba(0, 0, 0, 0.33) inset;">
                <div class="container">
                    <h1 class="title">
                        <fmt:message key="app.title"/>
                    </h1>
                    <h2 class="subtitle">
                        <fmt:message key="app.subtitle"/>
                    </h2>
                </div>
            </div>
        </section>


        <div class="modal is-active">
            <div class="modal-background"></div>
                <div class="modal-card">
                    <form name="loginForm" method="POST" action="${ pageContext.request.contextPath }/controller">
                        <input type="hidden" name="command" value="login" />
                        <header class="modal-card-head has-background-black-ter">
                            <p class="modal-card-title"><fmt:message key="message.login.header"/></p>
                        </header>
                        <section class="modal-card-body">

                        <div class="field" style="margin-left : 30; margin-right : 30;">
                            <label class="label" for="login">
                                <fmt:message key="user.login" />:
                            </label>
                            <input class="input " type="text" name="login"  value="" />
                        </div>

                        <div class="field" style="margin-left : 30; margin-right : 30;">
                            <label class="label" for="password">
                                <fmt:message key="user.password" />:
                            </label>
                            <div class="control has-icons-left ">
                                <input class="input" type="password" name="password" />
                                <span class="icon is-small is-left">
                                    <i class="fas fa-lock"></i>
                                </span>
                            </div>
                        </div>
                        ${errorLoginPassMessage}<br/>
                        ${wrongAction}<br/>
                        ${nullPage}<br/>
                        </section>
                        <footer class="modal-card-foot">
                            <input class="button" type="submit" value="<fmt:message key='links.user.login' />" style="margin-left : 30; margin-right : 30;"/>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>