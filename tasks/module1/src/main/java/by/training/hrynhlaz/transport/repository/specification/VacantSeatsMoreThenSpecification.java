package by.training.hrynhlaz.transport.repository.specification;

import by.training.hrynhlaz.transport.entity.Carriage;

public class VacantSeatsMoreThenSpecification implements Specification<Carriage> {
    private int seatsCount;

    public VacantSeatsMoreThenSpecification(int seatsCount) {
        this.seatsCount = seatsCount;
    }

    public boolean isSatisfiedBy(Carriage entity) {
        return entity.getVacantSeatsCount() >= seatsCount;
    }
}
