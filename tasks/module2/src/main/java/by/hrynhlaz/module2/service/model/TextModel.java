package by.hrynhlaz.module2.service.model;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;
import by.hrynhlaz.module2.service.composite.TextLeaf;
import by.hrynhlaz.module2.entity.Text;
import by.hrynhlaz.module2.service.TextService;
import java.util.LinkedList;
import java.util.List;

public class TextModel extends LinkedTextComposite {
    private List<TextLeaf> paragraphs = new LinkedList<>();
    private Text text = new Text();
    private TextService textService;

    public TextModel(TextService service) {
        super(TextLevel.TEXT);
        textService = service;
    }

    @Override
    public void addText(TextLeaf text) {
        paragraphs.add(text);
    }

    @Override
    public List<TextLeaf> getTextElements() {
        return paragraphs;
    }

    @Override
    public TextLevel getLevel() {
        return super.getLevel();
    }

    @Override
    public String getText() {
        StringBuilder resultString = new StringBuilder();
        for (TextLeaf paragraph : paragraphs) {
            resultString.append("\t").append(paragraph.getText());
        }
        return resultString.toString();
    }

    @Override
    public void load(long id) {
        text = textService.read(id);
    }

    @Override
    public long save(long parentId) {
        long textId = textService.create(text);
        for (TextLeaf paragraph : paragraphs) {
            paragraph.save(textId);
        }
        return textId;
    }

    @Override
    public int leafCount() {
        return paragraphs.size();
    }
}
