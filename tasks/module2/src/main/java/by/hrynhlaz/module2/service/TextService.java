package by.hrynhlaz.module2.service;

import by.hrynhlaz.module2.entity.Text;
import by.hrynhlaz.module2.repository.Specification;
import by.hrynhlaz.module2.repository.TextRepository;
import java.util.List;

public class TextService implements BaseTextService<Text> {
    private TextRepository textRepository;

    public TextService(TextRepository textRepository) {
        this.textRepository = textRepository;
    }

    @Override
    public long create(Text entity) {
        return textRepository.create(entity);
    }

    @Override
    public Text read(long id) {
        return textRepository.read(id);
    }

    @Override
    public void update(Text entity) {
        textRepository.update(entity);
    }

    @Override
    public void delete(long id) {
        textRepository.delete(id);
    }

    @Override
    public List<Text> find(Specification<Text> spec) {
        return textRepository.find(spec);
    }


    @Override
    public List<Text> getAll() {
        return textRepository.getAll();
    }

}
