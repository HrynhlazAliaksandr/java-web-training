package by.training.hrynhlaz.maintenance.tag.custom;

import by.training.hrynhlaz.maintenance.dto.UserAccountDto;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.util.Properties;

@SuppressWarnings("serial")
public class LoadSessionFromCookie extends TagSupport {
    @Override
    public int doStartTag() throws JspException {

        HttpServletRequest req = (HttpServletRequest) pageContext.getRequest();
        HttpServletResponse resp = (HttpServletResponse) pageContext.getResponse();

        HttpSession session = req.getSession();
        if(session.isNew()) {

            Cookie[] cookies = req.getCookies();
            if (cookies != null) {
                Properties prop = new Properties();
                for (int i = 0; i < cookies.length; i++) {
                    Cookie c = cookies[i];
                    prop.put(c.getName(), c.getValue());
                }

                String userIdProp = null;
                String userLoginProp = null;
                String userRoleProp = null;

                if(prop.containsKey("userId")) {
                    userIdProp = prop.getProperty("userId");
                }

                if(prop.containsKey("userLogin")) {
                    userLoginProp = prop.getProperty("userLogin");
                }

                if(prop.containsKey("userRole")) {
                    userRoleProp = prop.getProperty("userRole");
                }

                if(userIdProp != null && userLoginProp != null && userRoleProp != null) {
                    UserAccountDto user = new UserAccountDto();
                    user.setUserId(Long.parseLong(userIdProp));
                    user.setLogin(prop.getProperty("userLoginProp"));
                    user.setRole(prop.getProperty("userRoleProp"));
                    session.setAttribute("user", user);
                }
            }
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}
