package by.hrynhlaz.module2.service.model;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;
import by.hrynhlaz.module2.service.composite.TextLeaf;
import by.hrynhlaz.module2.entity.Paragraph;
import by.hrynhlaz.module2.service.ParagraphService;
import java.util.LinkedList;
import java.util.List;

public class ParagraphModel extends LinkedTextComposite {
    private Paragraph paragraph = new Paragraph();
    private ParagraphService paragraphService;

    private List<TextLeaf> sentences = new LinkedList<>();

    public ParagraphModel(ParagraphService service) {
        super(TextLevel.PARAGRAPH);
        paragraphService = service;
    }

    @Override
    public void addText(TextLeaf text) {
        sentences.add(text);
    }

    @Override
    public List<TextLeaf> getTextElements() {
        return sentences;
    }

    @Override
    public TextLevel getLevel() {
        return super.getLevel();
    }

    @Override
    public String getText() {
        StringBuilder resultString = new StringBuilder();
        for (TextLeaf sentence : sentences) {
            resultString.append(sentence.getText());
        }
        return resultString.toString();
    }

    @Override
    public void load(long id) {
        paragraph = paragraphService.read(id);
    }

    @Override
    public long save(long parentId) {
        paragraph.setTextId(parentId);
        long paragraphId = paragraphService.create(paragraph);
        for (TextLeaf sentence : sentences) {
            sentence.save(paragraphId);
        }
        return paragraphId;
    }

    @Override
    public int leafCount() {
        return sentences.size();
    }
}
