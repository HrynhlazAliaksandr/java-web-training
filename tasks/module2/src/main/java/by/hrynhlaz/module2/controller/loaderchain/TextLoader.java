package by.hrynhlaz.module2.controller.loaderchain;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;

public abstract class TextLoader implements LoaderChain<LinkedTextComposite> {
    private TextLoader next;

    @Override
    public LoaderChain<LinkedTextComposite> linkWith(LoaderChain<LinkedTextComposite> next) {
        this.next = (TextLoader) next;
        return this.next;
    }

    protected LinkedTextComposite nextBuild(long id) {
        if (next != null) {
            return next.loadChain(id);
        } else {
            return null;
        }
    }
}
