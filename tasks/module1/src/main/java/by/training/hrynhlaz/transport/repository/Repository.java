package by.training.hrynhlaz.transport.repository;

import by.training.hrynhlaz.transport.repository.specification.Specification;

import java.util.List;

public interface Repository<T> {

    public void add(T entity);
    public List<T> getCarriageList();
    public List<T> findBy(Specification<T> spec);
}
