package by.hrynhlaz.module2.entity;

public class Word {
    private long wordId;
    private long sentenceId;
    private String prefix;
    private String word;
    private String suffix;
    private boolean lastWord;

    public Word() {
        wordId = -1;
    }

    public long getWordId() {
        return wordId;
    }

    public void setWordId(long wordId) {
        this.wordId = wordId;
    }

    public long getSentenceId() {
        return sentenceId;
    }

    public void setSentenceId(long sentenceId) {
        this.sentenceId = sentenceId;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public void setLastWord(boolean lastWord) {
        this.lastWord = lastWord;
    }

    public String getWord() {
        return word;
    }

    public String getSuffix() {
        return suffix;
    }

    public boolean isLastWord() {
        return lastWord;
    }
}
