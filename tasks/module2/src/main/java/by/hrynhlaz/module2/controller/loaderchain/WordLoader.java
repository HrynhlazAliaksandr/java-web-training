package by.hrynhlaz.module2.controller.loaderchain;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;
import by.hrynhlaz.module2.entity.Word;
import by.hrynhlaz.module2.service.model.SentenceModel;
import by.hrynhlaz.module2.service.model.WordModel;
import by.hrynhlaz.module2.repository.ByWordParentIdSpecification;
import by.hrynhlaz.module2.service.SentenceService;
import by.hrynhlaz.module2.service.WordService;
import java.util.List;

public class WordLoader extends TextLoader {
    private SentenceService sentenceService;
    private WordService wordService;

    public WordLoader(SentenceService sentenceService, WordService wordService) {
        this.sentenceService = sentenceService;
        this.wordService = wordService;
    }

    @Override
    public LinkedTextComposite loadChain(long parentId) {
        SentenceModel sentenceModel = new SentenceModel(sentenceService);
        List<Word> words = wordService.find(new ByWordParentIdSpecification(parentId));
        for (Word word : words) {
            WordModel wordModel = new WordModel(wordService);
            wordModel.load(word.getWordId());
            sentenceModel.addText(wordModel);
        }
        return sentenceModel;
    }
}
