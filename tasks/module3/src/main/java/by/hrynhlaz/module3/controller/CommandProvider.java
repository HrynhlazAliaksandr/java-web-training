package by.hrynhlaz.module3.controller;

import by.hrynhlaz.module3.controller.command.Command;
import by.hrynhlaz.module3.controller.command.CommandName;
import by.hrynhlaz.module3.service.FlowerModel;

import java.util.HashMap;
import java.util.Map;

public class CommandProvider {
    private Map<CommandName, Command<FlowerModel>> repository = new HashMap<>();

    public Command<FlowerModel> getCommand(String name){
        CommandName commandName = null;
        Command<FlowerModel> command = null;
        try {
            commandName = CommandName.valueOf(name.toUpperCase());
            command = repository.get(commandName);
        } catch(IllegalArgumentException | NullPointerException e) {
            command = repository.get(CommandName.WRONG_REQUEST);
        }
        return command;
    }

    public void setCommand(CommandName commandName, Command<FlowerModel> command) {
        repository.put(commandName, command);
    }
}
