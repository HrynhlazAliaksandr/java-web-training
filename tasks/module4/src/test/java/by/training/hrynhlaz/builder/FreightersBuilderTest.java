package by.training.hrynhlaz.builder;

import by.training.hrynhlaz.model.Freighter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Queue;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class FreightersBuilderTest {

    @Test
    public void buildFreighter() throws URISyntaxException {
        FreightersBuilder builder = new FreightersBuilder();
        Path resourcePath = Paths.get(this.getClass().getClassLoader().getResource("Freighters.xml").toURI());
        Queue<Freighter> freighters = builder.buildFreighter(resourcePath.toString());
        int expected = 7;
        int actual = freighters.size();
        assertEquals(expected, actual);
    }
}