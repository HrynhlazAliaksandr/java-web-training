<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="lang" tagdir="/WEB-INF/tags" %>

<c:if test="${ empty pageContext.request.session.getAttribute('user') }">
        <c:redirect url="login.jsp"></c:redirect>
</c:if>

<c:choose>
    <c:when test="${not empty requestScope.get('lang')}">
        <fmt:setLocale value="${requestScope.get('lang')}"/>
        <c:set var="lng" value="${requestScope.get('lang')}" scope="application"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="${cookie['lang'].value}"/>
        <c:set var="lng" value="${cookie['lang'].value}" scope="application"/>
        </c:otherwise>
    </c:choose>
<fmt:setBundle basename="messages" scope="application"/>

<html>
<head>
    <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Maintenance Users</title>
        <link rel="stylesheet" href="${ pageContext.request.contextPath }/static/bulma.css">
</head>

<body>
    <nav class="navbar  is-fixed-top is-white" role="navigation" aria-label="main navigation">
            <div class="container">
                <div id="navbar" class="navbar-menu">
                    <div class="navbar-end">
                    <c:if test="${ user.getRole() eq 'admin' }">

                        <a class="navbar-item is-success" href="${pageContext.request.contextPath}/controller?command=view_users">
                            <fmt:message key="links.user.list" />
                        </a>

                        <a class="navbar-item is-warning" href="${pageContext.request.contextPath}/controller?command=show_add_user_form">
                            <fmt:message key="links.user.add" />
                        </a>

                        <div class="navbar-item is-spaced"></div>
                    </c:if>

                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link" >
                            <c:out value="${user.getLogin()}" />
                        </a>

                        <div class="navbar-dropdown is-boxed">
                            <a class="navbar-item" href="${pageContext.request.contextPath}/controller?command=show_user_profile">
                                <fmt:message key="links.user.editProfile" />
                            </a>

                            <hr class="navbar-divider">
                            <a class="navbar-item" href="${pageContext.request.contextPath}/controller?command=logout">
                                <fmt:message key="links.user.logout" />
                            </a>
                        </div>
                    </div>
                <div class="navbar-item is-spaced"></div>

                <lang:lang></lang:lang>

            </div>
        </div>
    </nav>

    <section class="hero is-dark is-bold is-medium">
        <div  class="hero-body" style="background-image:url('${pageContext.request.contextPath}/static/csm_2020.jpg'); background-position: bottom; box-shadow:0 0 0 300px rgba(0, 0, 0, 0.33) inset;">
            <div class="container">
                <h1 class="title">
                    <fmt:message key="app.title"/>
                </h1>
                <h2 class="subtitle">
                    <fmt:message key="app.subtitle"/>
                </h2>
            </div>
        </div>
    </section>


        <div class="container">
            <div class="content">
                <table class="table is-striped is-hoverable is-narrow is-fullwidth">
                    <thead>

                        <fmt:message key="user.login" var="login"/>
                        <fmt:message key="user.firstName" var="firstName"/>
                        <fmt:message key="user.lastName" var="lastName"/>
                        <fmt:message key="user.email" var="email"/>
                        <fmt:message key="user.phoneNumber" var="phone"/>
                        <fmt:message key="user.role" var="role"/>

                        <th>${login}</th>
                        <th>${firstName}</th>
                        <th>${lastName}</th>
                        <th>${email}</th>
                        <th>${phone}</th>
                        <th>${role}</th>
                        <th></th>
                        <th></th>
                    </thead>

                    <tbody>
                    <c:forEach items="${userList}" var="user">
                        <tr>
                            <td><c:out value="${user.login}"/></td>
                            <td><c:out value="${user.firstName}"/></td>
                            <td><c:out value="${user.lastName}"/></td>
                            <td><c:out value="${user.email}"/></td>
                            <td><c:out value="${user.phoneNumber}"/></td>
                            <td><c:out value="${user.role}"/></td>
                            <td>
                                <form action="controller" method="POST">
                                    <input type="hidden" name="userIdParam" value="${user.userId}"/>
                                    <input type="hidden" name="command" value="SHOW_EDIT_USER_FORM"/>
                                    <input class="button" type="submit" value='<fmt:message key="links.user.edit"/>'/>
                                </form>
                            </td>
                            <td>
                                <form action="controller" method="POST">
                                    <input type="hidden" name="userIdParam" value="${user.userId}"/>
                                    <input type="hidden" name="command" value="delete_user"/>
                                    <input class="button" type="submit" value='<fmt:message key="links.user.delete"/>'/>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <br/>


<section class="footer">
            <div class="content has-text-centered">

            </div>
        </section>

    </body>
</html>