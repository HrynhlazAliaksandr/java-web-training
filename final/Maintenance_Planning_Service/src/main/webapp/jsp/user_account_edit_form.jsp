<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/layout" %>

<c:choose>
    <c:when test="${not empty requestScope.get('lang')}">
        <fmt:setLocale value="${requestScope.get('lang')}"/>
        <c:set var="lng" value="${requestScope.get('lang')}" scope="application"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="${cookie['lang'].value}"/>
        <c:set var="lng" value="${cookie['lang'].value}" scope="application"/>
        </c:otherwise>
    </c:choose>
<fmt:setBundle basename="messages" scope="application"/>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Edit User Profile</title>
        <link rel="stylesheet" href="${ pageContext.request.contextPath }/static/bulma.css">
    </head>
    <body>
        <nav class="navbar  is-fixed-top is-white" role="navigation" aria-label="main navigation">
            <div class="container">
                <div id="navbar" class="navbar-menu">
                    <div class="navbar-end">
                    <c:if test="${ user.getRole() eq 'admin' }">

                        <a class="navbar-item is-success" href="${pageContext.request.contextPath}/controller?command=view_users">
                            <fmt:message key="links.user.list" />
                        </a>

                        <a class="navbar-item is-warning" href="${pageContext.request.contextPath}/controller?command=show_add_user_form">
                            <fmt:message key="links.user.add" />
                        </a>

                        <div class="navbar-item is-spaced"></div>
                    </c:if>

                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link" >
                            <c:out value="${user.getLogin()}" />
                        </a>

                        <div class="navbar-dropdown is-boxed">
                            <a class="navbar-item" href="${pageContext.request.contextPath}/controller?command=show_user_profile">
                                <fmt:message key="links.user.editProfile" />
                            </a>

                            <hr class="navbar-divider">
                            <a class="navbar-item" href="${pageContext.request.contextPath}/controller?command=logout">
                                <fmt:message key="links.user.logout" />
                            </a>
                        </div>
                    </div>
                <div class="navbar-item is-spaced"></div>

                <lang:lang></lang:lang>

                </div>
            </div>
        </nav>
        <section class="hero is-dark is-bold is-medium">
            <div  class="hero-body" style="background-image:url('${pageContext.request.contextPath}/static/csm_2020.jpg'); background-position: bottom; box-shadow:0 0 0 300px rgba(0, 0, 0, 0.33) inset;">
                <div class="container">
                    <h1 class="title">
                        <fmt:message key="app.title"/>
                    </h1>
                    <h2 class="subtitle">
                        <fmt:message key="app.subtitle"/>
                    </h2>
                </div>
            </div>
        </section>

        <section class="section">
                <div class="container">
                    <form name="loginForm" method="POST" action="${ pageContext.request.contextPath }/controller">
                        <input type="hidden" name="command" value="EDIT_USER" />
                        <input type="hidden" name="editUserId" value="${userAccount.getUserId()}" />




                        <div class="field" style="margin-left : 30; margin-right : 50%;">
                            <label class="label" for="login">
                                <fmt:message key="user.login" />:
                            </label>
                            <input class="input " type="text" name="login"  value="${userAccount.getLogin()}" />
                        </div>

                        <div class="field" style="margin-left : 30; margin-right : 50%;">
                            <label class="label" for="password">
                                <fmt:message key="user.password" />:
                            </label>
                            <div class="control has-icons-left ">
                                <input class="input" type="text" name="password" value="${userAccount.getPassword()}" />
                                <span class="icon is-small is-left">
                                    <i class="fas fa-lock"></i>
                                </span>
                            </div>
                        </div>

                        <div class="field" style="margin-left : 30; margin-right : 50%;">
                            <label class="label" for="role">
                                <fmt:message key="user.role" />:
                            </label>
                            <div class="control">
                                <div class="select is-info">
                                    <select name="role">
                                        <c:forEach var="elem" items="${userRoles}">
                                            <option value="${elem.getRoleId()}"
                                            <c:if test="${ elem.getRoleId() eq  userAccount.getRoleId() }">
                                                selected="selected"
                                            </c:if>
                                            ><c:out value="${elem.getRole()}" /></option>

                                        </c:forEach>
                                    </select><br/>
                                </div>
                            </div>
                        </div>

                        <div class="field" style="margin-left : 30; margin-right : 50%;">
                            <label class="label" for="firstName">
                                <fmt:message key="user.firstName" />:
                            </label>
                            <input class="input " type="text" name="firstName"  value="${userProfile.getFirstName()}" />
                        </div>

                        <div class="field" style="margin-left : 30; margin-right : 50%;">
                            <label class="label" for="lastName">
                                <fmt:message key="user.lastName" />:
                            </label>
                            <input class="input " type="text" name="lastName"  value="${userProfile.getLastName()}" />
                        </div>

                        <div class="field" style="margin-left : 30; margin-right : 50%;">
                            <label class="label" for="email">
                                <fmt:message key="user.email" />:
                            </label>
                            <input class="input " type="text" name="email"  value="${userProfile.getEmail()}" />
                        </div>

                        <div class="field" style="margin-left : 30; margin-right : 50%;">
                            <label class="label" for="phoneNumber">
                                <fmt:message key="user.phoneNumber" />:
                            </label>
                            <input class="input " type="text" name="phoneNumber"  value="${userProfile.getPhoneNumber()}" />
                        </div>



                            <input class="button" type="submit" value="<fmt:message key='links.user.edit' />" style="margin-left : 30; "/>

                    </form>
                </div>
        </section>


        <section class="footer ">
            <div     class="content has-text-centered">

            </div>
        </section>
    </body>
</html>