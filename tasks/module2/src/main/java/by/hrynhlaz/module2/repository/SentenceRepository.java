package by.hrynhlaz.module2.repository;

import by.hrynhlaz.module2.entity.Sentence;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class SentenceRepository implements Repository<Sentence> {
    private List<Sentence> sentences = new LinkedList<>();
    private AtomicLong sequencer = new AtomicLong(0);

    @Override
    public long create(Sentence entity) {
        if (entity.getSentenceId() == -1) {
            long newId = sequencer.incrementAndGet();
            entity.setSentenceId(newId);
            sentences.add(entity);
            return newId;
        }
        return -1;
    }

    @Override
    public Sentence read(long id) {
        for (Sentence sentence : sentences) {
            if (sentence.getSentenceId() == id) {
                return sentence;
            }
        }
        return null;
    }

    @Override
    public List<Sentence> find(Specification<Sentence> spec) {
        List<Sentence> result = new LinkedList<>();
        for (Sentence sentence : sentences) {
            if (spec.match(sentence)) {
                result.add(sentence);
            }
        }
        return result;
    }

    @Override
    public List<Sentence> getAll() {
        return sentences;
    }

    @Override
    public void update(Sentence entity) {

    }

    @Override
    public void delete(long id) {

    }

}
