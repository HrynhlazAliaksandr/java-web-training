package by.training.hrynhlaz.maintenance.service.exception;

public class ServiceException extends Exception {
    private static final Long serialVersionUID = 1L;
    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
