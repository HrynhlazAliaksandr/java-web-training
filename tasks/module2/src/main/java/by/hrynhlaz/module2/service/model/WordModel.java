package by.hrynhlaz.module2.service.model;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;
import by.hrynhlaz.module2.service.composite.TextLeaf;
import by.hrynhlaz.module2.entity.Word;
import by.hrynhlaz.module2.service.WordService;
import java.util.Collections;
import java.util.List;

public class WordModel extends LinkedTextComposite {
    private WordService wordService;
    private Word word = new Word();

    public WordModel(WordService service) {
        super(TextLevel.WORD);
        this.wordService = service;
    }

    public void setPrefix(String prefix) {
        this.word.setPrefix(prefix);
    }

    public void setWord(String word) {
        this.word.setWord(word);
    }

    public void setSuffix(String suffix) {
        this.word.setSuffix(suffix);
    }

    public void setLastWord(boolean lastWord) {
        this.word.setLastWord(lastWord);
    }

    @Override
    public void addText(TextLeaf text) { }

    @Override
    public TextLevel getLevel() {
        return super.getLevel();
    }

    @Override
    public String getText() {
        return word.getPrefix()
                + word.getWord()
                + word.getSuffix()
                + (word.isLastWord() ? "\n" : " ");
    }

    @Override
    public void load(long id) {
        word = wordService.read(id);
    }

    @Override
    public List<TextLeaf> getTextElements() {
        return Collections.emptyList();
    }

    @Override
    public long save(long parentId) {
        word.setSentenceId(parentId);
        return wordService.create(word);
    }

    @Override
    public int leafCount() {
        return 0;
    }
}
