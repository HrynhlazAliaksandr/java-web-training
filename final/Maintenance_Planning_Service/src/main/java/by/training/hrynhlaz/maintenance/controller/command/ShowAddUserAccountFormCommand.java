package by.training.hrynhlaz.maintenance.controller.command;

import by.training.hrynhlaz.maintenance.ConfigurationManager;
import by.training.hrynhlaz.maintenance.dto.UserAccountDto;
import by.training.hrynhlaz.maintenance.entity.UserProfile;
import by.training.hrynhlaz.maintenance.entity.UserRole;
import by.training.hrynhlaz.maintenance.service.UserProfileService;
import by.training.hrynhlaz.maintenance.service.UserRoleService;
import by.training.hrynhlaz.maintenance.service.exception.ServiceException;
import by.training.hrynhlaz.maintenance.service.factory.ServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

public class ShowAddUserAccountFormCommand implements ActionCommand {
    private final static Logger LOGGER = Logger.getLogger(ShowAddUserAccountFormCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        List<UserRole> userRoles = null;
        ServiceFactory serviceFactory = ServiceFactory.getInstance();
        UserRoleService userRoleService = serviceFactory.getUserRoleService();

        try {
            userRoles = userRoleService.findAll();
            //userRoles.addAll(userRoleService.findAll());
            request.setAttribute("userRoles", userRoles);

        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
            //
        }
        page = ConfigurationManager.getProperty("path.page.addUserAccountForm");
        return page;
    }
}
