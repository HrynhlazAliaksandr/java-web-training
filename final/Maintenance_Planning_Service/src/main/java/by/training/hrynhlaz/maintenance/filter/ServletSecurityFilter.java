package by.training.hrynhlaz.maintenance.filter;

import by.training.hrynhlaz.maintenance.dto.UserAccountDto;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = { "/controller" }, servletNames = {"Controller"})
public class ServletSecurityFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        String command = req.getParameter("command");
        if(command != null && command.equalsIgnoreCase("login")) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        HttpSession session = req.getSession(true);

        UserAccountDto userAccount = (UserAccountDto) session.getAttribute("user");

         if(userAccount == null) {
            RequestDispatcher dispatcher = servletRequest.getServletContext().getRequestDispatcher("/jsp/login.jsp");
            dispatcher.forward(req, resp);
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);

    }

    @Override
    public void destroy() {

    }
}

