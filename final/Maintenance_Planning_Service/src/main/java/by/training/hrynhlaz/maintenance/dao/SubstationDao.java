package by.training.hrynhlaz.maintenance.dao;

import by.training.hrynhlaz.maintenance.entity.Substation;

public abstract class SubstationDao extends BaseDao<Long, Substation> {
}
