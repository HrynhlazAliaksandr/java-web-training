package by.training.hrynhlaz.transport.controller.carbuilder;

import by.training.hrynhlaz.transport.entity.Carriage;
import java.util.Map;

public abstract class BaseCarBuilder {
    public abstract Carriage build(Map<String, String> propertyMap);
}
