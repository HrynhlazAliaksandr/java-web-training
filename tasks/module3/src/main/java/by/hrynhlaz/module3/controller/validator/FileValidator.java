package by.hrynhlaz.module3.controller.validator;

import org.apache.log4j.Logger;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileValidator {
    private static final Logger LOGGER = Logger.getLogger(FileValidator.class);
    private ValidationResult validationResult = new ValidationResult();

    public ValidationResult validateFile(String pathToFile) {
        Path path;

        if (pathToFile == null || pathToFile.isEmpty()) {
            validationResult.addResult(100, "Missing file path");
            LOGGER.error("Missing file path");
            return validationResult;
        }

        try {
            path = Paths.get(pathToFile);
        } catch (InvalidPathException e) {
            validationResult.addResult(101, "The path string contains invalid characters, "
                    + "or the path string is invalid for other file system specific reasons.");
            LOGGER.error("The path string contains invalid characters, or the path string is invalid "
                    + "for other file system specific reasons.", e);
            return validationResult;
        } catch (SecurityException e) {
            validationResult.addResult(102, "The Security manager is invoked to check "
                    + "read access to the file.");
            LOGGER.error("The Security manager is invoked to check read access to the file.", e);
            return validationResult;
        }

        if (!Files.exists(path)) {
            validationResult.addResult(103, "The file does not exist.");
            LOGGER.error("The file does not exist.");
        }

        return validationResult;
    }
}
