<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:directive.attribute name="fieldName" required="true" description="field name to display" />
<jsp:directive.attribute name="beanName" required="true" description="some object" />
<c:if test="${not empty fieldName and not empty beanName}">
    <c:set var="className" value="" />
    <c:if test="${not empty validationResult and validationResult eq 'success'}">
        <c:set var="className" value="is-success" />
    </c:if>
    <c:if test="${not empty validationErrors and not empty validationErrors[fieldName]}">
        <c:set var="errors" value="${validationErrors[fieldName]}" />
        <c:set var="className" value="is-danger" />
    </c:if>

    <div class="field">
        <c:set var="bean" value="${requestScope[beanName]}" />
        <c:set var="inpId" value="input_${beanName}_${fieldName}" />

        <label class="label" for="${inpId}">
            <fmt:message key="${beanName}.${fieldName}" />:
        </label>
        <div class="control has-icons-right">
            <input class="input ${className}" type="text" name="${beanName}.${fieldName}"  value="${bean[fieldName]}" />

            <c:if test="${not empty validationResult and validationResult eq 'success'}">
                <span class="icon is-small is-right">
                    <i class="fas fa-check"></i>
                </span>
            </c:if>


            <c:if test="${not empty errors}">
                <span class="icon is-small is-right">
                    <i class="fas fa-exclamation-triangle"></i>
                </span>
            </c:if>
        </div>
            <c:if test="${not empty errors}">
                <%--<c:forEach items="errors" item="error">
                    <p class="help is-danger"><c:out value="${error}" /></p>
                </c:forEach> --%>
            </c:if>
    </div>
</c:if>