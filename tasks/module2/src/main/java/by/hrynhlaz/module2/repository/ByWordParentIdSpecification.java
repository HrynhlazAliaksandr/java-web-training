package by.hrynhlaz.module2.repository;

import by.hrynhlaz.module2.entity.Word;

public class ByWordParentIdSpecification implements Specification<Word>{
    private long parentId;

    public ByWordParentIdSpecification(long parentId) {
        this.parentId = parentId;
    }

    @Override
    public boolean match(Word entity) {
        if (Long.compare(this.parentId, entity.getSentenceId()) == 0) {
            return true;
        }
        return false;
    }
}
