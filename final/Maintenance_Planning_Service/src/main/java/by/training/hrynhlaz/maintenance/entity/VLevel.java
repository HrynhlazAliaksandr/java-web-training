package by.training.hrynhlaz.maintenance.entity;

public class VLevel extends Entity {
    private long vLevelId;
    private String vLevelName;
    private String vLevel;
    private long substationId;

    public VLevel() {}

    public VLevel(long vLevelId, String vLevelName, String vLevel, long substationId) {
        this.vLevelId = vLevelId;
        this.vLevelName = vLevelName;
        this.vLevel = vLevel;
        this.substationId = substationId;
    }

    public long getVLevelId() {
        return vLevelId;
    }

    public void setVLevelId(long vLevelId) {
        this.vLevelId = vLevelId;
    }

    public String getVLevelName() {
        return vLevelName;
    }

    public void setVLevelName(String vLevelName) {
        this.vLevelName = vLevelName;
    }

    public String getVLevel() {
        return vLevel;
    }

    public void setVLevel(String vLevel) {
        this.vLevel = vLevel;
    }

    public long getSubstationId() {
        return substationId;
    }

    public void setSubstationId(long substationId) {
        this.substationId = substationId;
    }
}
