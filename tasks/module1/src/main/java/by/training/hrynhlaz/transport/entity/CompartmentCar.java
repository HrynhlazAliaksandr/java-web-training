package by.training.hrynhlaz.transport.entity;

public class CompartmentCar extends Carriage {

    int lowerSeatsCount = 18;
    int upperSeatsCount = 18;
    int vacantLowerSeatsCount = 18;
    int vacantUpperSeatsCount = 18;

    public CompartmentCar() {
        carType = CarTypeEnum.COMPARTMENT_CAR;
        baggageCapacity = 100;
        baggageWeight = 0;
    }

    public int getVacantLowerSeatsCount() {
        return vacantLowerSeatsCount;
    }

    public void setVacantLowerSeatsCount(int count) {
        vacantLowerSeatsCount = count;
    }

    public int getVacantUpperSeatsCount() {
        return vacantUpperSeatsCount;
    }

    public void setVacantUpperSeatsCount(int count) {
        vacantUpperSeatsCount = count;
    }

    @Override
    public int getVacantSeatsCount() {
        return vacantLowerSeatsCount + vacantUpperSeatsCount;
    }

    @Override
    public int getSeatsCount() {
        return lowerSeatsCount + upperSeatsCount;
    }

    @Override
    public String toString() {
        String carInfo = super.toString();
        carInfo += "\nVacant upper seats count : " + getVacantUpperSeatsCount();
        carInfo += "\nVacant lower seats count : " + getVacantLowerSeatsCount();
        carInfo += "\nSeats count : " + getSeatsCount();
        carInfo += "\nVacant seats count : " + getVacantSeatsCount();
        return carInfo;
    }
}