package by.hrynhlaz.module3.controller.builder;

import by.hrynhlaz.module3.entity.Flower;
import by.hrynhlaz.module3.service.BaseFlowerService;
import by.hrynhlaz.module3.service.composite.FlowerEnum;
import by.hrynhlaz.module3.service.FlowerModel;
import org.apache.log4j.Logger;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class FlowerStAXBuilder implements FlowerBuilder {
    private static final Logger LOGGER = Logger.getLogger(FlowerStAXBuilder.class);
    private XMLInputFactory inputFactory;
    private EnumSet<FlowerEnum> withText;
    private BaseFlowerService<Flower> flowerService;

    public FlowerStAXBuilder(BaseFlowerService<Flower> flowerService) {
        this.flowerService = flowerService;
        inputFactory = XMLInputFactory.newInstance();
        withText = EnumSet.range(FlowerEnum.NAME, FlowerEnum. WATERING_PER_WEEK);
    }

    public List<FlowerModel> buildFlower (String fileName) {
        XMLStreamReader reader = null;
        FlowerModel flower = null;
        List<FlowerModel> flowers = new ArrayList<>();

        try (FileInputStream inputStream = new FileInputStream(new File(fileName))) {
            reader = inputFactory.createXMLStreamReader(inputStream);
            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    String name = reader.getLocalName();
                    if(FlowerEnum.valueOf(name.toUpperCase()) == FlowerEnum.FLOWER) {
                        flower = new FlowerModel(flowerService);
                        setValues(reader, name, flower);
                    }
                    if (flower != null) {
                        flowers.add(flower);
                        flower.print();
                    }
                }
            }
        } catch (XMLStreamException | IOException ex) {
            LOGGER.error(ex.getMessage());
        }
        return flowers;
    }

    private void setValues(XMLStreamReader reader, String localName, FlowerModel flower) throws XMLStreamException {
        while (reader.hasNext()) {
            switch (reader.next()) {
                case XMLStreamConstants.START_ELEMENT :
                    FlowerEnum temp = FlowerEnum.valueOf(reader.getLocalName().toUpperCase());
                    switch (temp) {
                        case AVERAGE_PLANT_SIZE :
                            flower.setAverageUnit(reader.getAttributeValue(null, "unit"));
                            break;
                        case TEMPERATURE:
                            flower.setTemperatureUnit(reader.getAttributeValue(null, "unit"));
                            break;
                        case WATERING_PER_WEEK:
                            flower.setWateringUnit(reader.getAttributeValue(null, "unit"));
                            break;
                    }

                    if (withText.contains(temp)) {
                        temp.setValue(flower, getXMLText(reader));
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT :
                    if(reader.getLocalName() == localName) {
                        return;
                    }
                    break;
            }
        }
    }

    private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }

}
