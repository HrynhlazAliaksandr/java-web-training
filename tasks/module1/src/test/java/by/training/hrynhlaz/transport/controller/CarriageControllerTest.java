package by.training.hrynhlaz.transport.controller;

import by.training.hrynhlaz.transport.controller.validator.ValidationResult;
import by.training.hrynhlaz.transport.repository.CarriageRepository;
import by.training.hrynhlaz.transport.repository.Repository;
import by.training.hrynhlaz.transport.service.CarriageService;
import by.training.hrynhlaz.transport.service.Service;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class CarriageControllerTest {
    private static final Logger LOGGER = Logger.getLogger(CarriageControllerTest.class);
    private Repository carriageRepository = new CarriageRepository();
    private Service carriageService = new CarriageService(carriageRepository);
    private CarriageController carriageController = new CarriageController(carriageService);

    private String convertMessToString(List<String> errMessages) {
        String message = "";
        for(String line : errMessages) {
            message += line + "\n";
        }
        return message;
    }

    @Test
    public void loadCarriagesFromFile() {
        ValidationResult validationResult;
        List<String> errMessages;
        String message = "";

        ClassLoader classLoader = getClass().getClassLoader();
        String path =  new File(classLoader.getResource("correct_data.txt").getFile()).getAbsolutePath();
        validationResult = carriageController.loadCarriagesFromFile(path);
        if(!validationResult.isValid()) {
            LOGGER.warn(convertMessToString(validationResult.getErrMessages()));
        }
        assertTrue(validationResult.isValid());

        path =  new File(classLoader.getResource("Incorrect_carriage_type_line2.txt").getFile()).getAbsolutePath();
        validationResult = carriageController.loadCarriagesFromFile(path);
        if(!validationResult.isValid()) {
            LOGGER.warn(convertMessToString(validationResult.getErrMessages()));
        }
        assertFalse(validationResult.isValid());

        path =  new File(classLoader.getResource("Incorrect_carnumber_line1.txt").getFile()).getAbsolutePath();
        validationResult = carriageController.loadCarriagesFromFile(path);
        if(!validationResult.isValid()) {
            LOGGER.warn(convertMessToString(validationResult.getErrMessages()));
        }
        assertFalse(validationResult.isValid());

        path =  new File(classLoader.getResource("empty_file.txt").getFile()).getAbsolutePath();
        validationResult = carriageController.loadCarriagesFromFile(path);
        if(!validationResult.isValid()) {
            LOGGER.warn(convertMessToString(validationResult.getErrMessages()));
        }
        assertFalse(validationResult.isValid());

    }
}