package by.hrynhlaz.module3.controller.command;

import java.util.List;

public interface Command<T> {
    List<T> execute(String path);
}
