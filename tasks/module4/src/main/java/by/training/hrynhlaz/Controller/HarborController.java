package by.training.hrynhlaz.Controller;

import by.training.hrynhlaz.model.Freighter;
import by.training.hrynhlaz.model.Harbor;
import by.training.hrynhlaz.builder.FreightersBuilder;
import java.util.Iterator;
import java.util.Queue;

public class HarborController {
    private FreightersBuilder freighterBuilder;
    private Harbor harbor;

    public HarborController(Harbor harbor, FreightersBuilder freighterBuilder) {
        this.harbor = harbor;
        this.freighterBuilder = freighterBuilder;
    }

    public Queue<Freighter> loadFreighters(String fileName) {
        return freighterBuilder.buildFreighter(fileName);
    }

    public void goFreighters(Queue<Freighter> freighters) {
        Freighter freighter;
        Iterator<Freighter> freighterIterator = freighters.iterator();
        while (freighterIterator.hasNext()) {
            freighter = freighterIterator.next();
            freighter.setHarbor(harbor);
            freighter.start();
        }
    }
}
