package by.hrynhlaz.module2.controller.parserchain;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;
import by.hrynhlaz.module2.service.composite.TextLeaf;
import by.hrynhlaz.module2.service.model.ParagraphModel;
import by.hrynhlaz.module2.service.model.SentenceModel;
import by.hrynhlaz.module2.service.ParagraphService;
import by.hrynhlaz.module2.service.SentenceService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceParser extends TextParser {
    private ParagraphService paragraphService;
    private SentenceService sentenceService;

    public SentenceParser(ParagraphService paragraphService, SentenceService sentenceService) {
        this.paragraphService = paragraphService;
        this.sentenceService = sentenceService;
    }

    @Override
    public LinkedTextComposite parseLine(String line) {
        Pattern pat = Pattern.compile("([A-Z]{1})(.)+?(\\.)(\\n)?");
        Matcher mat = pat.matcher(line);
        LinkedTextComposite paragraph = new ParagraphModel(paragraphService);
        while (mat.find()) {
            LinkedTextComposite sentence = new SentenceModel(sentenceService);
            LinkedTextComposite textElement = nextParse(mat.group());
            if (textElement != null) {
                for (TextLeaf element : textElement.getTextElements()) {
                    sentence.addText(element);
                }
            }
            paragraph.addTextNode(sentence);
        }
        return paragraph;
    }
}
