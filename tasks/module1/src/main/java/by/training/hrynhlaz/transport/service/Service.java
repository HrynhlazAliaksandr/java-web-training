package by.training.hrynhlaz.transport.service;

import by.training.hrynhlaz.transport.repository.specification.Specification;

import java.util.Comparator;
import java.util.List;

public interface Service<T> {
    public void saveCarriage(T carriage);
    public int calculateTotalPassengerCount();
    public double calculateTotalBaggageWeight();
    public List<T> sort(Comparator<T> comparator);
    public List<T> getCarriagesBySpec(Specification<T> spec);
}
