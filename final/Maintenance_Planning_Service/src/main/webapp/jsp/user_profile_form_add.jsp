<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
    <head>
        <title>Add new user profile</title>
    </head>
    <body>
        <form name="loginForm" method="POST" action="controller">
            <input type="hidden" name="command" value="edit_user_profile_add" />
            First name:<br/>
            <input type="text" name="firstName" value="${userProfile.getFirstName()}"/><br/>
            Last name:<br/>
            <input type="text" name="lastName" value="${userProfile.getLastName()}"/><br/>
            email:<br/>
            <input type="text" name="email" value="${userProfile.getEmail()}"/><br/>
            Phone number:<br/>
            <input type="text" name="phoneNumber" value="${userProfile.getPhoneNumber()}"/><br/>

            <input type="submit" value="edit"/>
        </form>
        <hr/>
    </body>
</html>
