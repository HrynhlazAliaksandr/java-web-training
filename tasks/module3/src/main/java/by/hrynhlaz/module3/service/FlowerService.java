package by.hrynhlaz.module3.service;

import by.hrynhlaz.module3.entity.Flower;
import by.hrynhlaz.module3.repository.Repository;

import java.util.List;

public class FlowerService implements BaseFlowerService<Flower> {
    private Repository<Flower> flowerRepository;

    public FlowerService(Repository<Flower> repository) {
        flowerRepository = repository;
    }

    @Override
    public long create(Flower entity) {
        return flowerRepository.create(entity);
    }

    @Override
    public Flower read(long id) {
        return flowerRepository.read(id);
    }

    @Override
    public List<Flower> getAll() {
        return flowerRepository.getAll();
    }

    @Override
    public void update(Flower entity) {

    }

    @Override
    public void delete(long id) {

    }
}
