package by.training.hrynhlaz.transport.entity;

public class RegularSeatsCar extends Carriage {
    //CarTypeEnum carType = CarTypeEnum.REGULAR_SEATS_CAR;
    int seatsCount = 86;
    int vacantSeatsCount = 86;

    public RegularSeatsCar() {
        this.carType = CarTypeEnum.REGULAR_SEATS_CAR;
        this.baggageCapacity= 100;
        this.baggageWeight = 0;
    }

    @Override
    public int getSeatsCount() {
        return seatsCount;
    }

    @Override
    public int getVacantSeatsCount() {
        return vacantSeatsCount;
    }

    public void setVacantSeatsCount(int count) {
        vacantSeatsCount = count;
    }

    @Override
    public String toString() {
        String carInfo = super.toString();
        carInfo += "\nSeats count : " + getSeatsCount();
        carInfo += "\nVacant seats count : " + getVacantSeatsCount();
        return carInfo;
    }
}
