package by.training.hrynhlaz.transport.repository;

import by.training.hrynhlaz.transport.entity.Carriage;
import by.training.hrynhlaz.transport.repository.specification.Specification;

import java.util.ArrayList;
import java.util.List;

public class CarriageRepository implements Repository<Carriage> {
    private List<Carriage> carriageList = new ArrayList<>();

    @Override
    public void add(Carriage carriage) {
        carriageList.add(carriage);
    }

    @Override
    public List<Carriage> getCarriageList() {
        return new ArrayList<>(carriageList);
    }

    @Override
    public List<Carriage> findBy(Specification<Carriage> spec) {
        List<Carriage> found = new ArrayList<>();

        for (Carriage carriage : carriageList) {
            if (spec.isSatisfiedBy(carriage)) {
                found.add(carriage);
            }
        }
        return found;
    }
}
