package by.hrynhlaz.module3.controller.command;

public enum CommandName {
    BUILD_FLOWERS_WITH_DOM, BUILD_FLOWERS_WITH_SAX, BUILD_FLOWERS_WITH_STAX, WRONG_REQUEST
}
