package by.hrynhlaz.module2.repository;

@FunctionalInterface
public interface Specification<T> {
    boolean match(T entity);
}
