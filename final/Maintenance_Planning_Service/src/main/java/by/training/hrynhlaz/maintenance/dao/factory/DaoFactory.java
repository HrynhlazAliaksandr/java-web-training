package by.training.hrynhlaz.maintenance.dao.factory;

import by.training.hrynhlaz.maintenance.dao.*;
import by.training.hrynhlaz.maintenance.dao.impl.*;

public class DaoFactory {
    private static final DaoFactory instance = new DaoFactory();
    private final UserDao sqlUserAccountDao = new SqlUserDao();
    private final UserProfileDao sqlUserProfileDao = new SqlUserProfileDao();
    private final UserRoleDao sqlUserRoleDao = new SqlUserRoleDao();
    private final BayDao sqlBayDao = new SqlBayDao();
    private final DeviceDao sqlDeviceDao = new SqlDeviceDao();
    private final SubstationDao sqlSubstationDao = new SqlSubstationDao();
    private final VLevelDao sqlVLevelDao = new SqlVLevelDao();

    private DaoFactory() {}

    public static DaoFactory getInstance() {
        return instance;
    }

    public UserDao getUserAccountDao() {
        return sqlUserAccountDao;
    }
    public UserProfileDao getUserProfileDao() {
        return sqlUserProfileDao;
    }
    public UserRoleDao getUserRoleDao() {
        return sqlUserRoleDao;
    }
    public BayDao getBayDao() {
        return sqlBayDao;
    }
    public DeviceDao getDeviceDao() {
        return sqlDeviceDao;
    }
    public SubstationDao getSubstationDao() {
        return sqlSubstationDao;
    }
    public VLevelDao getVLevelDao() { return sqlVLevelDao; }

}
