package by.training.hrynhlaz.maintenance;


import java.util.Locale;
import java.util.ResourceBundle;

public class MessageManager {
    private  final static ResourceBundle resourceBundle
            = ResourceBundle.getBundle("messages", Locale.getDefault());
    private MessageManager() { }
    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
