package by.hrynhlaz.module2.repository;

import by.hrynhlaz.module2.entity.Word;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class WordRepository implements Repository<Word>{
    private List<Word> words = new LinkedList<>();
    private AtomicLong sequencer = new AtomicLong(0);

    @Override
    public long create(Word entity) {
        if (entity.getWordId() == -1) {
            long newId = sequencer.incrementAndGet();
            entity.setWordId(newId);
            words.add(entity);
            return newId;
        }
        return -1;
    }

    @Override
    public Word read(long id) {
        for (Word word : words) {
            if (word.getWordId() == id) {
                return word;
            }
        }
        return null;
    }

    @Override
    public List<Word> getAll() {
        return words;
    }

    @Override
    public List<Word> find(Specification<Word> spec) {
        List<Word> result = new LinkedList<>();
        for (Word word : words) {
            if (spec.match(word)) {
                result.add(word);
            }
        }
        return result;
    }

    @Override
    public void update(Word entity) {

    }

    @Override
    public void delete(long id) {

    }
}
