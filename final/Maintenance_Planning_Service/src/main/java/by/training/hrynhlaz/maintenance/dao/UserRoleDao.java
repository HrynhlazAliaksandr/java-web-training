package by.training.hrynhlaz.maintenance.dao;

import by.training.hrynhlaz.maintenance.entity.UserRole;

public abstract class UserRoleDao extends BaseDao<Long, UserRole> {
}
