package by.hrynhlaz.module3.service;

import java.util.List;

public interface BaseFlowerService<T> {
    long create(T entity);

    T read(long id);

    void update(T entity);

    void delete(long id);

    List<T> getAll();
}
