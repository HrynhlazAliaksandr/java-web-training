package by.hrynhlaz.module2.controller.parserchain;

public interface ParserChain<T> {

    T parseLine(String line);

    ParserChain<T> linkWith(ParserChain<T> next);
}
