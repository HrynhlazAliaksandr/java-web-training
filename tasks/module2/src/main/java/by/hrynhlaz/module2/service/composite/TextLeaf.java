package by.hrynhlaz.module2.service.composite;

public interface TextLeaf {
    String getText();
    long save(long parentId);
    void load(long id);

}
