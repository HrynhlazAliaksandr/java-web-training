package by.hrynhlaz.module2.controller.validator;

import java.util.HashMap;
import java.util.Map;

public class ValidatorResult {
    private Map<Integer, String> validatorResult = new HashMap<>();

    public boolean isValidate() {
        return validatorResult.isEmpty();
    }

    public void addResult(int errCode, String errMessage) {
        validatorResult.put(errCode, errMessage);
    }
}
