package by.hrynhlaz.module2.service.composite;

import java.util.List;

public interface TextComposite extends TextLeaf {
    void addText(TextLeaf text);

    List<TextLeaf> getTextElements();

    int leafCount();
}
