package by.training.hrynhlaz.transport.controller.validator;

import by.training.hrynhlaz.transport.util.Verifying;

import java.util.Map;

public class CompartmentCarDataValidator extends CarriageDataValidator {
    @Override
    public ValidationResult validateCarProperty (Map<String, String> carPropertyList) {
        ValidationResult validationResult = super.validateCarProperty(carPropertyList);

        if (!carPropertyList.containsKey("vacantLowerSeatsCount")
                || (carPropertyList.get("vacantLowerSeatsCount") == null) ) {
            validationResult.addErrMessage(210, "Vacant Lower Seats Count property exists");
        } else {
            if (!Verifying.checkIntegerValue(carPropertyList.get("vacantLowerSeatsCount"))) {
                validationResult.addErrMessage(211,
                        "Invalid number format in VacantLowerSeatsCount property");
            }
        }

        if (!carPropertyList.containsKey("vacantUpperSeatsCount")
                || (carPropertyList.get("vacantUpperSeatsCount") == null) ) {
            validationResult.addErrMessage(220, "Vacant Upper Seats Count property exists");
        } else {
            if (!Verifying.checkIntegerValue(carPropertyList.get("vacantUpperSeatsCount"))) {
                validationResult.addErrMessage(221,
                        "Invalid number format in vacantUpperSeatsCount property");
            }
        }

        return validationResult;
    }
}
