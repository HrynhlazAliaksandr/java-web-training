package by.hrynhlaz.module2.controller;

import by.hrynhlaz.module2.service.FacadeTextService;
import by.hrynhlaz.module2.controller.validator.FileValidator;

public class TextController {
    private FacadeTextService textService;
    private FileReader reader;
    private FileValidator validator;

    public TextController(FacadeTextService textService, FileReader reader, FileValidator validator) {
        this.textService = textService;
        this.reader = reader;
        this.validator = validator;
    }

    public long saveTextFromFile(String filePath) {
        String inputText = "";
        if (validator.ValidateFile(filePath).isValidate()) {
            inputText = reader.readData(filePath);
        }
        return textService.saveTextToRepository(inputText);
    }

    public void loadText(long textId) {
        textService.loadTextFromRepository(textId);
    }

    public void sort() {
        textService.SortParagraphsByCountSent();
    }
}
