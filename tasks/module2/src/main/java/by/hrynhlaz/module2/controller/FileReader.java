package by.hrynhlaz.module2.controller;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

public class FileReader {
    private static final Logger LOGGER = Logger.getLogger(FileReader.class);

    public String readData(String path) {
        List<String> lines = Collections.emptyList();
        StringBuilder text = new StringBuilder();

        try {
            lines = Files.readAllLines(Paths.get(path));
        } catch(IOException e) {
            LOGGER.error("\nCould not read the file.", e);
        }

        for (String line : lines) {
            text.append(line + "\n");
        }

        return text.toString();
    }
}
