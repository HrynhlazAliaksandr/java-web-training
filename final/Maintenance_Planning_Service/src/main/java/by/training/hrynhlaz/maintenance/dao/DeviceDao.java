package by.training.hrynhlaz.maintenance.dao;

import by.training.hrynhlaz.maintenance.entity.Device;

public abstract class DeviceDao extends BaseDao<Long, Device> {
}
