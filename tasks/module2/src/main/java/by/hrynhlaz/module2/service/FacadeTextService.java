package by.hrynhlaz.module2.service;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;
import by.hrynhlaz.module2.service.composite.TextLeaf;
import by.hrynhlaz.module2.controller.loaderchain.ParagraphLoader;
import by.hrynhlaz.module2.controller.loaderchain.SentenceLoader;
import by.hrynhlaz.module2.controller.loaderchain.WordLoader;
import by.hrynhlaz.module2.controller.parserchain.ParagraphParser;
import by.hrynhlaz.module2.controller.parserchain.SentenceParser;
import by.hrynhlaz.module2.controller.parserchain.WordParser;
import org.apache.log4j.Logger;
import java.util.LinkedList;
import java.util.List;

public class FacadeTextService {
    private static final Logger LOGGER = Logger.getLogger(FacadeTextService.class);
    private TextService textService;
    private ParagraphService paragraphService;
    private SentenceService sentenceService;
    private WordService wordService;
    private LinkedTextComposite textModel;


    public FacadeTextService(TextService textService,
                             ParagraphService paragraphService,
                             SentenceService sentenceService,
                             WordService wordService) {
        this.textService = textService;
        this.paragraphService = paragraphService;
        this.sentenceService = sentenceService;
        this.wordService = wordService;
    }

    public void loadTextFromRepository(long textId) {
        ParagraphLoader paragraphLoader = new ParagraphLoader(textService, paragraphService);
        SentenceLoader sentenceLoader = new SentenceLoader(paragraphService, sentenceService);
        WordLoader wordLoader = new WordLoader(sentenceService, wordService);

        paragraphLoader.linkWith(sentenceLoader);
        sentenceLoader.linkWith(wordLoader);

        this.textModel = paragraphLoader.loadChain(textId);
        LOGGER.info(textModel.getText());
    }

    public long saveTextToRepository(String text) {
        ParagraphParser paragraphParser = new ParagraphParser(textService, paragraphService);
        SentenceParser sentenceParser = new SentenceParser(paragraphService, sentenceService);
        WordParser wordParser = new WordParser(sentenceService, wordService);

        paragraphParser.linkWith(sentenceParser);
        sentenceParser.linkWith(wordParser);

        LinkedTextComposite textModel = paragraphParser.parseLine(text);
        LOGGER.info(textModel.getText());
        return textModel.save(-1);
    }

    public List<LinkedTextComposite> SortParagraphsByCountSent() {
        LOGGER.info("Loaded text:\n" + textModel.getText());
        List<TextLeaf> list = textModel.getTextElements();
        List<LinkedTextComposite> compositeList = new LinkedList<>();
        for (TextLeaf leaf : list) {
            compositeList.add((LinkedTextComposite) leaf);
        }
        compositeList.sort(new SentenceCountComparator());
        StringBuilder sortedText = new StringBuilder();
        for (LinkedTextComposite textElement : compositeList) {
            sortedText.append("\t" + textElement.getText());
        }
        LOGGER.info("Sorted text:\n" + sortedText.toString());
        return compositeList;
    }

    public LinkedTextComposite getActualTextModel() {
        return textModel;
    }


}
