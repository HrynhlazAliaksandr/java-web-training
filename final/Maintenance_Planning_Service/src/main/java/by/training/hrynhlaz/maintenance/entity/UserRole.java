package by.training.hrynhlaz.maintenance.entity;

public class UserRole extends Entity {
    private long roleId;
    private String role;

    public UserRole() {}

    public UserRole(long roleId, String role) {
        this.roleId = roleId;
        this.role = role;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
