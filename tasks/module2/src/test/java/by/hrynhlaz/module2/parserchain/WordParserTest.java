package by.hrynhlaz.module2.parserchain;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;
import by.hrynhlaz.module2.service.composite.TextLeaf;
import by.hrynhlaz.module2.controller.parserchain.WordParser;
import by.hrynhlaz.module2.repository.SentenceRepository;
import by.hrynhlaz.module2.repository.WordRepository;
import by.hrynhlaz.module2.service.SentenceService;
import by.hrynhlaz.module2.service.WordService;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class WordParserTest {
    private static final Logger LOGGER = Logger.getLogger(WordParserTest.class);
    private SentenceService sentenceService = new SentenceService(new SentenceRepository());
    private WordService wordService = new WordService(new WordRepository());

    @Test
    public void parseLine() {
        String inputText = "It has survived not only five centuries, but also the leap into " +
                "electronic typesetting, remaining essentially unchanged.\n";

        int actualCount = 0;
        int expectedCount = 17;

        WordParser wordParser = new WordParser(sentenceService, wordService);
        LinkedTextComposite text = wordParser.parseLine(inputText);
        for (TextLeaf element : text.getTextElements()) {
            actualCount++;
        }

        assertEquals("Count words in the sentence: ", expectedCount, actualCount);
    }
}