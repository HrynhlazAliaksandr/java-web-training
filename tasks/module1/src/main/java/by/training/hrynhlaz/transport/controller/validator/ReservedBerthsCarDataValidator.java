package by.training.hrynhlaz.transport.controller.validator;

import by.training.hrynhlaz.transport.util.Verifying;

import java.util.Map;

public class ReservedBerthsCarDataValidator extends CompartmentCarDataValidator {
    @Override
    public ValidationResult validateCarProperty (Map<String, String> carPropertyList) {
        ValidationResult validationResult = super.validateCarProperty(carPropertyList);

        if (!carPropertyList.containsKey("vacantLowerSideSeatsCount")
                || (carPropertyList.get("vacantLowerSideSeatsCount") == null) ) {
            validationResult.addErrMessage(310, "Vacant Lower Side Seats Count property exists");
        } else {
            if (!Verifying.checkIntegerValue(carPropertyList.get("vacantLowerSideSeatsCount"))) {
                validationResult.addErrMessage(311,
                        "Invalid number format in VacantLowerSideSeatsCount property");
            }
        }

        if (!carPropertyList.containsKey("vacantUpperSideSeatsCount")
                || (carPropertyList.get("vacantUpperSideSeatsCount") == null) ) {
            validationResult.addErrMessage(320, "Vacant Upper Side Seats Count property exists");
        } else {
            if (!Verifying.checkIntegerValue(carPropertyList.get("vacantUpperSideSeatsCount"))) {
                validationResult.addErrMessage(321,
                        "Invalid number format in vacantUpperSideSeatsCount property");
            }
        }

        return  validationResult;
    }
}