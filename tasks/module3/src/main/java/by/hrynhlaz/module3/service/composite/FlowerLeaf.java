package by.hrynhlaz.module3.service.composite;

public interface FlowerLeaf {
    String getFlower();
    long save(long parentId);
    void load(long id);
}
