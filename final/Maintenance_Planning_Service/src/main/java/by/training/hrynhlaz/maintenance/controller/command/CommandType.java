package by.training.hrynhlaz.maintenance.controller.command;

public enum CommandType {
    LOGIN(new LoginCommand()),
    VIEW_USERS(new ViewUserListCommand()),
    SHOW_USER_PROFILE(new ShowUserProfileFormCommand()),
    EDIT_USER_PROFILE(new EditUserProfileCommand()),
    EDIT_USER_PROFILE_ADD(new AddUserProfileCommand()),
    DELETE_USER(new DeleteUserAccountCommand()),
    ADD_USER(new AddUserAccountCommand()),
    SHOW_ADD_USER_FORM(new ShowAddUserAccountFormCommand()),
    SHOW_EDIT_USER_FORM(new ShowEditUserAccountFormCommand()),
    EDIT_USER(new EditUserAccountCommand()),
    LOGOUT(new LogoutCommand());

    private ActionCommand command;

    CommandType(ActionCommand command) {
        this.command = command;
    }

    public ActionCommand getCommand() {
        return command;
    }
}
