package by.hrynhlaz.module3.controller;

import by.hrynhlaz.module3.controller.validator.ValidationResult;
import by.hrynhlaz.module3.controller.validator.ValidatorSAXXSD;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

@RunWith(JUnit4.class)
public class ValidatorSAXXSDTest {
    private ValidationResult vr;

    @Test
    public void validate() throws URISyntaxException {
        Path resourcePath = Paths.get(this.getClass().getClassLoader().getResource("Flowers.xsd").toURI());
        String XSDSchemaName = resourcePath.toString();
        ValidatorSAXXSD validator = new ValidatorSAXXSD(XSDSchemaName);
        resourcePath = Paths.get(this.getClass().getClassLoader().getResource("Valid_Flowers.xml").toURI());
        String XMLFileName = resourcePath.toString();

        vr = validator.validate(XMLFileName);
        Assert.assertTrue(vr.isValid());

        resourcePath = Paths.get(this.getClass().getClassLoader().getResource("Invalid_Flowers.xml").toURI());
        XMLFileName = resourcePath.toString();

        vr = validator.validate(XMLFileName);
        Assert.assertTrue(vr.containsKey(102));

        resourcePath = Paths.get(this.getClass().getClassLoader().getResource("Bad-formed_Flowers.xml").toURI());
        XMLFileName = resourcePath.toString();

        vr = validator.validate(XMLFileName);
        Assert.assertTrue(vr.containsKey(103));
    }
}