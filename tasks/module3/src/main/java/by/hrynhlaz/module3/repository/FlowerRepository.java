package by.hrynhlaz.module3.repository;

import by.hrynhlaz.module3.entity.Flower;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class FlowerRepository implements Repository<Flower> {
    private List<Flower> flowers = new LinkedList<>();
    private AtomicLong sequencer = new AtomicLong(0);

    @Override
    public long create(Flower entity) {
            long newId = sequencer.incrementAndGet();
            entity.setId(newId);
            flowers.add(entity);
            return newId;
    }

    @Override
    public Flower read(long id) {

        for (Flower flower : flowers) {
            if (flower.getId() == id) {
                return flower;
            }
        }
        return null;
    }

    @Override
    public List<Flower> getAll() {
        return flowers;
    }

    @Override
    public void update(Flower entity) {

    }

    @Override
    public void delete(long id) {

    }
}
