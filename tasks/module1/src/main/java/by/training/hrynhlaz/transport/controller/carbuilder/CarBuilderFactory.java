package by.training.hrynhlaz.transport.controller.carbuilder;

import by.training.hrynhlaz.transport.entity.CarTypeEnum;

public class CarBuilderFactory {
    public BaseCarBuilder build(CarTypeEnum carType) {

        BaseCarBuilder builder = null;
        switch(carType) {
            case COMPARTMENT_CAR :
                builder = new CompartmentCarBuilder();
                break;
            case RESERVED_BERTHS_CAR :
                builder = new ReservedBerthsCarBuilder();
                break;
            case REGULAR_SEATS_CAR :
                builder = new RegularSeatsCarBuilder();
        }
        return builder;
    }
}