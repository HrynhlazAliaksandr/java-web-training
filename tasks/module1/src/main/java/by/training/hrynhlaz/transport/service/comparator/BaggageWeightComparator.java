package by.training.hrynhlaz.transport.service.comparator;

import by.training.hrynhlaz.transport.entity.Carriage;

import java.util.Comparator;

public class BaggageWeightComparator implements Comparator<Carriage> {

    @Override
    public int compare(Carriage one, Carriage two) {
        return Double.compare(one.getBaggageWeight(), two.getBaggageWeight());
    }
}
