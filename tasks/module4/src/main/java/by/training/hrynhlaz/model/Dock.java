package by.training.hrynhlaz.model;

import org.apache.log4j.Logger;

public class Dock {
    private final static Logger LOGGER = Logger.getLogger(Dock.class);
    private int dockId;
    private Warehouse warehouse;

    public Dock(int dockId, Warehouse warehouse) {
        super();
        this.dockId = dockId;
        this.warehouse = warehouse;
    }

    public int getDockId() {
        return dockId;
    }

    public Warehouse getWarehouse() {
        return this.warehouse;
    }
}


