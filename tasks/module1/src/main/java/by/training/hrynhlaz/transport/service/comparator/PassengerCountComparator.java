package by.training.hrynhlaz.transport.service.comparator;

import by.training.hrynhlaz.transport.entity.Carriage;

import java.util.Comparator;

public class PassengerCountComparator implements Comparator<Carriage> {
    @Override
    public int compare(Carriage one, Carriage two) {
        return Integer.compare(two.getSeatsCount() - two.getVacantSeatsCount(),
                one.getSeatsCount() - one.getVacantSeatsCount());
    }
}
