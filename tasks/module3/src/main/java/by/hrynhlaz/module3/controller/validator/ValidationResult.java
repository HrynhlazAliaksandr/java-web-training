package by.hrynhlaz.module3.controller.validator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidationResult {
    private Map<Integer, String> validatorResult = new HashMap<>();

    public boolean isValid() {
        return validatorResult.isEmpty();
    }

    public void addResult(int errCode, String errMessage) {
        validatorResult.put(errCode, errMessage);
    }

    public boolean containsKey(Object key) {
        return validatorResult.containsKey(key);
    }

    public String[] getMessages() {
        if (isValid()) {
            return null;
        }
        String[] result = new String[validatorResult.size()];
        validatorResult.values().toArray(result);
        return result;
    }
}
