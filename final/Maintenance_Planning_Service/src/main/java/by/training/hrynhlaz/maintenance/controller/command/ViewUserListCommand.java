package by.training.hrynhlaz.maintenance.controller.command;

import by.training.hrynhlaz.maintenance.ConfigurationManager;
import by.training.hrynhlaz.maintenance.dto.UserDto;
import by.training.hrynhlaz.maintenance.service.UserAccountService;
import by.training.hrynhlaz.maintenance.service.exception.ServiceException;
import by.training.hrynhlaz.maintenance.service.factory.ServiceFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ViewUserListCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        ServiceFactory serviceFactory = ServiceFactory.getInstance();
        UserAccountService userAccountService = serviceFactory.getUserAccountService();
        String page = null;

        try {
            List<UserDto> all = userAccountService.selectAll();
            request.setAttribute("userList", all);
        } catch (ServiceException e) {
            //
        }
        page = ConfigurationManager.getProperty("path.page.users");

        return page;
    }
}

