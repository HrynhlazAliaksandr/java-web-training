package by.training.hrynhlaz.maintenance.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.stream.Stream;

@WebFilter(urlPatterns = { "/controller" }, servletNames = {"Controller"})
public class LangFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (servletRequest instanceof HttpServletRequest) {

            String lang = servletRequest.getParameter("lang");
            HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
            if (("en".equalsIgnoreCase(lang) || "ru".equalsIgnoreCase(lang))) {
                Cookie langCookie = new Cookie("lang", lang);
                langCookie.setPath(httpRequest.getContextPath());
                httpRequest.setAttribute("lang", lang);
                ((HttpServletResponse) servletResponse).addCookie(langCookie);
            } else {
                Optional<Cookie[]> cookies = Optional.ofNullable(httpRequest.getCookies());
                Cookie langCookie = cookies.map(Stream::of).orElse(Stream.empty())
                        .filter(cookie -> cookie.getName().equalsIgnoreCase("lang")).findFirst()
                        .orElse(new Cookie("lang", "en"));
                langCookie.setPath(httpRequest.getContextPath());
                httpRequest.setAttribute("lang", lang);
                ((HttpServletResponse) servletResponse).addCookie(langCookie);
            }

        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
