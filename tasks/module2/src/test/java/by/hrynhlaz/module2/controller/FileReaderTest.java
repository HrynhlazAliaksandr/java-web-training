package by.hrynhlaz.module2.controller;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;



@RunWith(JUnit4.class)
public class FileReaderTest {
    private static final Logger LOGGER = Logger.getLogger(FileReaderTest.class);

    private Path resourcePath;

    @Before
    public void prepareData() throws URISyntaxException {
        resourcePath = Paths.get(this.getClass().getClassLoader().getResource("Text.txt").toURI());
    }

    @Test
    public void readData() {
        FileReader reader = new FileReader();
        String text = reader.readData(resourcePath.toString());
        LOGGER.info(text);
        assertNotNull(text);
    }
}