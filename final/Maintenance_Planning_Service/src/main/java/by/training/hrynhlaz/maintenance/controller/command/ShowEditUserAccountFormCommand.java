package by.training.hrynhlaz.maintenance.controller.command;

import by.training.hrynhlaz.maintenance.ConfigurationManager;
import by.training.hrynhlaz.maintenance.entity.UserAccount;
import by.training.hrynhlaz.maintenance.entity.UserProfile;
import by.training.hrynhlaz.maintenance.entity.UserRole;
import by.training.hrynhlaz.maintenance.service.UserAccountService;
import by.training.hrynhlaz.maintenance.service.UserProfileService;
import by.training.hrynhlaz.maintenance.service.UserRoleService;
import by.training.hrynhlaz.maintenance.service.exception.ServiceException;
import by.training.hrynhlaz.maintenance.service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ShowEditUserAccountFormCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        UserAccount account = null;
        UserProfile profile = null;
        List<UserRole> userRoles = null;
        ServiceFactory serviceFactory = ServiceFactory.getInstance();
        long userId = Long.parseLong(request.getParameter("userIdParam"));
        UserAccountService userAccountService = serviceFactory.getUserAccountService();
        UserProfileService userProfileService = serviceFactory.getUserProfileService();
        UserRoleService userRoleService = serviceFactory.getUserRoleService();

        try {
            account = userAccountService.findById(userId);
            profile = userProfileService.findById(userId);
            userRoles = userRoleService.findAll();
            request.setAttribute("userAccount", account);
            request.setAttribute("userProfile", profile);
            request.setAttribute("userRoles", userRoles);
            page = ConfigurationManager.getProperty("path.page.userAccountEditForm");
        } catch (ServiceException e) {
            //
        }
        return page;
    }
}
