package by.training.hrynhlaz.transport.entity;

public class ReservedBerthsCar extends CompartmentCar {
    int lowerSideSeatsCount = 9;
    int upperSideSeatsCount = 9;
    int vacantLowerSideSeatsCount = 9;
    int vacantUpperSideSeatsCount = 9;

    public ReservedBerthsCar() {
        super();
        carType = CarTypeEnum.RESERVED_BERTHS_CAR;
        baggageCapacity = 150;
    }

    public int getVacantLowerSideSeatsCount() {
        return vacantLowerSideSeatsCount;
    }

    public void setVacantLowerSideSeatsCount(int count) {
        vacantLowerSideSeatsCount = count;
    }

    public int getVacantUpperSideSeatsCount() {
        return vacantUpperSideSeatsCount;
    }

    public void setVacantUpperSideSeatsCount(int count) {
        vacantUpperSideSeatsCount = count;
    }

    @Override
    public int getVacantSeatsCount() {
        return super.getVacantSeatsCount() + vacantLowerSideSeatsCount
                + vacantUpperSideSeatsCount;
    }

    @Override
    public int getSeatsCount() {
        return super.getSeatsCount() + lowerSideSeatsCount
                + upperSideSeatsCount;
    }

    @Override
    public String toString() {
        String carInfo = super.toString();
        carInfo += "\nVacant upper side seats count : " + getVacantUpperSideSeatsCount();
        carInfo += "\nVacant lower side seats count : " + getVacantLowerSideSeatsCount();
        return carInfo;
    }
}
