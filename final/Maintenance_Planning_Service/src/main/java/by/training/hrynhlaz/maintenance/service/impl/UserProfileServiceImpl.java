package by.training.hrynhlaz.maintenance.service.impl;

import by.training.hrynhlaz.maintenance.dao.EntityTransaction;
import by.training.hrynhlaz.maintenance.dao.UserProfileDao;
import by.training.hrynhlaz.maintenance.dao.exception.DaoException;
import by.training.hrynhlaz.maintenance.dao.factory.DaoFactory;
import by.training.hrynhlaz.maintenance.entity.UserProfile;
import by.training.hrynhlaz.maintenance.service.UserProfileService;
import by.training.hrynhlaz.maintenance.service.exception.ServiceException;

public class UserProfileServiceImpl implements UserProfileService {
    @Override
    public UserProfile findById(Long id) throws ServiceException {
        DaoFactory daoObjectFactory = DaoFactory.getInstance();
        UserProfileDao userProfileDao = daoObjectFactory.getUserProfileDao();
        EntityTransaction entityTransaction = new EntityTransaction();
        UserProfile userProfile = null;
        try {
            entityTransaction.begin(false, userProfileDao);
            userProfile = userProfileDao.findById(id);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return userProfile;
    }

    @Override
    public boolean update(UserProfile userProfile) throws ServiceException {
        boolean result = false;
        DaoFactory daoObjectFactory = DaoFactory.getInstance();
        UserProfileDao userProfileDao = daoObjectFactory.getUserProfileDao();
        EntityTransaction entityTransaction = new EntityTransaction();
        try {
            entityTransaction.begin(false, userProfileDao);
            result = userProfileDao.update(userProfile);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return result;
    }

    @Override
    public long addProfile(UserProfile newProfile) throws ServiceException {
        long result = -1;
        DaoFactory daoObjectFactory = DaoFactory.getInstance();
        UserProfileDao userProfileDao = daoObjectFactory.getUserProfileDao();
        EntityTransaction entityTransaction = new EntityTransaction();
        try {
            entityTransaction.begin(false, userProfileDao);
            result = userProfileDao.create(newProfile);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return result;
    }
}
