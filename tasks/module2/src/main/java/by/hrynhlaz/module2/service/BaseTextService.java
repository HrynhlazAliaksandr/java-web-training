package by.hrynhlaz.module2.service;

import by.hrynhlaz.module2.repository.Specification;

import java.util.List;

public interface BaseTextService<T> {

    long create(T entity);

    T read(long id);

    void update(T entity);

    void delete(long id);

    List<T> find (Specification<T> spec);

    List<T> getAll();
}
