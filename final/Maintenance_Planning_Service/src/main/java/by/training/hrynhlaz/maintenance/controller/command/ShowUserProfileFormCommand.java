package by.training.hrynhlaz.maintenance.controller.command;

import by.training.hrynhlaz.maintenance.ConfigurationManager;
import by.training.hrynhlaz.maintenance.dto.UserAccountDto;
import by.training.hrynhlaz.maintenance.entity.UserProfile;
import by.training.hrynhlaz.maintenance.service.UserProfileService;
import by.training.hrynhlaz.maintenance.service.exception.ServiceException;
import by.training.hrynhlaz.maintenance.service.factory.ServiceFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ShowUserProfileFormCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        UserProfile profile = null;
        ServiceFactory serviceFactory = ServiceFactory.getInstance();
        UserProfileService userProfileService = serviceFactory.getUserProfileService();
        HttpSession session = request.getSession();
        UserAccountDto user = (UserAccountDto) session.getAttribute("user");
        try {
            profile = userProfileService.findById(user.getUserId());
            request.setAttribute("userProfile", profile);
            page = ConfigurationManager.getProperty("path.page.userProfileForm");
        } catch (ServiceException e) {
            //
        }

        if(profile != null) {
            page = ConfigurationManager.getProperty("path.page.userProfileForm");
        } else {
            page = ConfigurationManager.getProperty("path.page.userProfileFormAdd");
        }

        return page;
    }
}
