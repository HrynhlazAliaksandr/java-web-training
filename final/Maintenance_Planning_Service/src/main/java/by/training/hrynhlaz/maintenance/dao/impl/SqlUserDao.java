package by.training.hrynhlaz.maintenance.dao.impl;

import by.training.hrynhlaz.maintenance.dao.UserDao;
import by.training.hrynhlaz.maintenance.dao.exception.DaoException;
import by.training.hrynhlaz.maintenance.dto.UserDto;
import by.training.hrynhlaz.maintenance.entity.UserAccount;
import by.training.hrynhlaz.maintenance.dto.UserAccountDto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SqlUserDao extends UserDao {

    private static final String SELECT_BY_LOGIN_AND_PASS_QUERY =
            "SELECT user_id, login, role_name " +
                    "FROM user_account " +
                    "INNER JOIN user_role " +
                    "ON user_account.role_id = user_role.role_id " +
                    "WHERE login = ? AND password = ?";
    private static final String SELECT_BY_ID_QUERY =
            "SELECT user_id, login, password, role_id FROM user_account " +
                    "WHERE user_id = ?";
    private static final String INSERT_QUERY =
            "INSERT INTO user_account(login, password, role_id) VALUES(?, ?, ?)";
    private static final String UPDATE_QUERY =
            "UPDATE user_account SET login=?, password=?, role_id=? WHERE user_id = ?";
    private static final String DELETE_QUERY =
            "DELETE FROM user_account WHERE user_id = ?";
    private static final String SELECT_ALL_QUERY =
            "SELECT user_id, login, password, role_id FROM user_account";
    private static final String SELECT_ALL_QUERY_JOIN =
            "SELECT ua.user_id, ua.login, up.first_name, up.last_name, up.email, up.phone_number, ur.role_name " +
                    "FROM user_role as ur " +
                    "RIGHT JOIN (user_account as ua LEFT JOIN user_profile as up ON ua.user_id = up.user_id) " +
                    "ON ur.role_id = ua.role_id";

    @Override
    public List<UserAccount> findAll() throws DaoException {
        List<UserAccount> result = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = this.connection.prepareStatement(SELECT_ALL_QUERY);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                UserAccount entity = new UserAccount();
                entity.setUserId(resultSet.getLong("user_id"));
                entity.setLogin(resultSet.getString("login"));
                entity.setPassword(resultSet.getString("password"));
                entity.setRoleId(resultSet.getLong("role_id"));
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public Long create(UserAccount entity) throws DaoException {
        long id = 0;
        PreparedStatement statement = null;
        try{
            statement = this.connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            int i = 0;
            statement.setString(++i, entity.getLogin());
            statement.setString(++i, entity.getPassword());
            statement.setLong(++i, entity.getRoleId());
            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if(generatedKeys.next()){
                id = generatedKeys.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return id;
    }

    @Override
    public boolean delete(long id) throws DaoException {
        PreparedStatement statement = null;
        boolean result = false;
        try {
            statement = connection.prepareStatement(DELETE_QUERY);
            statement.setLong(1, id);
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public boolean update(UserAccount entity) throws DaoException {
        PreparedStatement statement = null;
        boolean result = false;
        try {
            statement = this.connection.prepareStatement(UPDATE_QUERY);
            int i = 0;
            statement.setString(++i, entity.getLogin());
            statement.setString(++i, entity.getPassword());
            statement.setLong(++i, entity.getRoleId());
            statement.setLong(++i, entity.getUserId());
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public UserAccount findById(Long id) throws DaoException {
        PreparedStatement statement = null;
        UserAccount userAccount = null;
        try {
            statement = this.connection.prepareStatement(SELECT_BY_ID_QUERY);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()) {
                userAccount = new UserAccount();
                userAccount.setUserId(resultSet.getLong("user_id"));
                userAccount.setLogin(resultSet.getString("login"));
                userAccount.setPassword(resultSet.getString("password"));
                userAccount.setRoleId(resultSet.getLong("role_id"));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return userAccount;
    }

    @Override
    public void close(Statement statement) throws DaoException {
        super.close(statement);
    }

    @Override
    public UserAccountDto login(String login, String password) throws DaoException {
        PreparedStatement statement = null;
        UserAccountDto userAccount = null;
        try {
            statement = this.connection.prepareStatement(SELECT_BY_LOGIN_AND_PASS_QUERY);
            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()) {
                userAccount = new UserAccountDto();
                userAccount.setUserId(resultSet.getLong("user_id"));
                userAccount.setLogin(resultSet.getString("login"));
                userAccount.setRole(resultSet.getString("role_name"));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return userAccount;
    }

    @Override
    public void logout(String login) throws DaoException {

    }

    @Override
    public List<UserDto> selectAll() throws DaoException {
        List<UserDto> result = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = this.connection.prepareStatement(SELECT_ALL_QUERY_JOIN);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                UserDto dto = new UserDto();
                dto.setUserId(resultSet.getLong("ua.user_id"));
                dto.setLogin(resultSet.getString("ua.login"));
                dto.setFirstName(resultSet.getString("up.first_name"));
                dto.setLastName(resultSet.getString("up.last_name"));
                dto.setEmail(resultSet.getString("up.email"));
                dto.setPhoneNumber(resultSet.getString("up.phone_number"));
                dto.setRole(resultSet.getString("ur.role_name"));
                result.add(dto);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }
}

