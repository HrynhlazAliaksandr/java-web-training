package by.hrynhlaz.module2.controller.parserchain;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;
import by.hrynhlaz.module2.service.composite.TextLeaf;
import by.hrynhlaz.module2.service.model.ParagraphModel;
import by.hrynhlaz.module2.service.model.TextModel;
import by.hrynhlaz.module2.service.ParagraphService;
import by.hrynhlaz.module2.service.TextService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParagraphParser extends TextParser {
    private TextService textService;
    private ParagraphService paragraphService;

    public ParagraphParser(TextService textService, ParagraphService paragraphService) {
        this.textService = textService;
        this.paragraphService = paragraphService;
    }

    @Override
    public LinkedTextComposite parseLine(String line) {
        Pattern pat = Pattern.compile("\t.+\n?");
        Matcher mat = pat.matcher(line);
        LinkedTextComposite text = new TextModel(textService);
            while (mat.find()) {
                LinkedTextComposite paragraph = new ParagraphModel(paragraphService);
                LinkedTextComposite textElement = nextParse(mat.group());
                if (textElement != null) {
                    for (TextLeaf element : textElement.getTextElements()) {
                        paragraph.addText(element);
                    }
                }
                text.addTextNode(paragraph);
            }
        return text;
    }
}
