package by.hrynhlaz.module2.repository;

import java.util.List;

public interface Repository<T>  {

    long create(T entity);

    void update(T entity);

    void delete(long id);

    T read(long id);

    List<T> find(Specification<T> spec);

    List<T> getAll();

}
