package by.hrynhlaz.module3.controller;

import by.hrynhlaz.module3.controller.command.Command;
import by.hrynhlaz.module3.controller.validator.FileValidator;
import by.hrynhlaz.module3.controller.validator.ValidationResult;
import by.hrynhlaz.module3.controller.validator.ValidatorSAXXSD;
import by.hrynhlaz.module3.service.FlowerModel;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class FlowerController {
    private final static Logger LOGGER = Logger.getLogger(FlowerController.class);
    private CommandProvider provider;
    private FileValidator fileValidator;
    private ValidatorSAXXSD SAXValidator;

    public FlowerController(CommandProvider provider,
                            FileValidator fileValidator,
                            ValidatorSAXXSD SAXValidator) {
        this.provider = provider;
        this.fileValidator = fileValidator;
        this.SAXValidator = SAXValidator;
    }

    public List<FlowerModel> executeCommand(String request){
        List<FlowerModel> flowers = new ArrayList<>();
        ValidationResult vr = null;
        char paramDelimeter = ':';
        String commandName = request.substring(0, request.indexOf(paramDelimeter));
        String fileName = request.substring(request.indexOf(paramDelimeter) + 1);
        vr = fileValidator.validateFile(fileName);
        if (!vr.isValid()) {
            for (String line : vr.getMessages()) {
                LOGGER.error(line);
            }
            return flowers;
        }
        vr = SAXValidator.validate(fileName);
        if (!vr.isValid()) {
            for (String line : vr.getMessages()) {
                LOGGER.error(line);
            }
            return flowers;
        }
        Command<FlowerModel> executionCommand = provider.getCommand(commandName);
        flowers = executionCommand.execute(fileName);
        return flowers;
    }
}
