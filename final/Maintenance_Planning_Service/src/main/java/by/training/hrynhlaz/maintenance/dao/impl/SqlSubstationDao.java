package by.training.hrynhlaz.maintenance.dao.impl;

import by.training.hrynhlaz.maintenance.dao.SubstationDao;
import by.training.hrynhlaz.maintenance.dao.exception.DaoException;
import by.training.hrynhlaz.maintenance.entity.Substation;
import by.training.hrynhlaz.maintenance.entity.UserProfile;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SqlSubstationDao extends SubstationDao {

    private static final String SELECT_BY_ID_QUERY =
            "SELECT substation_id, substation_name FROM substation " +
                    "WHERE substation_id = ?";
    private static final String INSERT_QUERY =
            "INSERT INTO substation(substation_name) VALUES(?)";
    private static final String UPDATE_QUERY =
            "UPDATE substation SET substation_name = ? WHERE substation_id = ?";
    private static final String DELETE_QUERY =
            "DELETE FROM substation WHERE substation_id = ?";
    private static final String SELECT_ALL_QUERY =
            "SELECT substation_id, substation_name FROM substation";

    @Override
    public List<Substation> findAll() throws DaoException {
        List<Substation> result = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = this.connection.prepareStatement(SELECT_ALL_QUERY);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Substation entity = new Substation();
                entity.setSubstationId(resultSet.getLong("substation_id"));
                entity.setSubstationName(resultSet.getString("substation_name"));
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public Long create(Substation entity) throws DaoException {
        long id = 0;
        PreparedStatement statement = null;
        try{
            statement = this.connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            int i = 0;
            statement.setString(++i, entity.getSubstationName());
            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if(generatedKeys.next()){
                id = generatedKeys.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return id;
    }

    @Override
    public boolean delete(long id) throws DaoException {
        PreparedStatement statement = null;
        boolean result = false;
        try {
            statement = connection.prepareStatement(DELETE_QUERY);
            statement.setLong(1, id);
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public boolean update(Substation entity) throws DaoException {
        PreparedStatement statement = null;
        boolean result = false;
        try {
            statement = this.connection.prepareStatement(UPDATE_QUERY);
            int i = 0;
            statement.setString(++i, entity.getSubstationName());
            statement.setLong(++i, entity.getSubstationId());
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public Substation findById(Long id) throws DaoException {
        PreparedStatement statement = null;
        Substation substation = null;
        try {
            statement = this.connection.prepareStatement(SELECT_BY_ID_QUERY);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()) {
                substation = new Substation();
                substation.setSubstationId(resultSet.getLong("substation_id"));
                substation.setSubstationName(resultSet.getString("substation_name"));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return substation;
    }

    @Override
    public void close(Statement statement) throws DaoException {
        super.close(statement);
    }
}
