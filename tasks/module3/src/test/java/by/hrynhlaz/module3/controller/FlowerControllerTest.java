package by.hrynhlaz.module3.controller;

import by.hrynhlaz.module3.controller.builder.FlowerBuilder;
import by.hrynhlaz.module3.controller.builder.FlowerDomBuilder;
import by.hrynhlaz.module3.controller.builder.FlowerSAXBuilder;
import by.hrynhlaz.module3.controller.builder.FlowerStAXBuilder;
import by.hrynhlaz.module3.controller.command.BuildFlowersWithDOM;
import by.hrynhlaz.module3.controller.command.Command;
import by.hrynhlaz.module3.controller.command.CommandName;
import by.hrynhlaz.module3.controller.validator.FileValidator;
import by.hrynhlaz.module3.controller.validator.ValidatorSAXXSD;
import by.hrynhlaz.module3.entity.Flower;
import by.hrynhlaz.module3.repository.FlowerRepository;
import by.hrynhlaz.module3.repository.Repository;
import by.hrynhlaz.module3.service.BaseFlowerService;
import by.hrynhlaz.module3.service.FlowerModel;
import by.hrynhlaz.module3.service.FlowerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class FlowerControllerTest {

    @Test
    public void executeCommand() throws URISyntaxException {
        Repository<Flower> flowerRepository = new FlowerRepository();
        BaseFlowerService<Flower> flowerService = new FlowerService(flowerRepository);
        List<FlowerModel> flowers = null;
        Path resourcePath = Paths.get(this.getClass().getClassLoader().getResource("Flowers.xsd").toURI());
        String XSDSchemaName = resourcePath.toString();
        ValidatorSAXXSD SAXValidator = new ValidatorSAXXSD(XSDSchemaName);
        FileValidator fileValidator = new FileValidator();
        FlowerBuilder flowerDomBuilder = new FlowerDomBuilder(flowerService);
        FlowerBuilder flowerSAXBuilder = new FlowerSAXBuilder(flowerService);
        FlowerBuilder flowerStAXBuilder = new FlowerStAXBuilder(flowerService);
        Command<FlowerModel> BuildWithDom = new BuildFlowersWithDOM(flowerDomBuilder);
        Command<FlowerModel> BuildWithSAX = new BuildFlowersWithDOM(flowerSAXBuilder);
        Command<FlowerModel> BuildWithStAX = new BuildFlowersWithDOM(flowerStAXBuilder);
        CommandProvider provider = new CommandProvider();
        provider.setCommand(CommandName.BUILD_FLOWERS_WITH_DOM, BuildWithDom);
        provider.setCommand(CommandName.BUILD_FLOWERS_WITH_SAX, BuildWithSAX);
        provider.setCommand(CommandName.BUILD_FLOWERS_WITH_STAX, BuildWithStAX);
        FlowerController flowerController = new FlowerController(provider, fileValidator, SAXValidator);

        resourcePath = Paths.get(this.getClass().getClassLoader().getResource("Valid_Flowers.xml").toURI());
        String XMLFileName = resourcePath.toString();

        // Build list of flowers with DOMBuilder
        flowers = flowerController.executeCommand("BUILD_FLOWERS_WITH_DOM:" + XMLFileName);
        int expected = 10;
        int actual = flowers.size();
        assertEquals(expected, actual);

        // Build list of flowers with SAXBuilder
        flowers = flowerController.executeCommand("BUILD_FLOWERS_WITH_SAX:" + XMLFileName);
        expected = 10;
        actual = flowers.size();
        assertEquals(expected, actual);

        // Build list of flowers with StAXBuilder
        flowers = flowerController.executeCommand("BUILD_FLOWERS_WITH_STAX:" + XMLFileName);
        expected = 10;
        actual = flowers.size();
        assertEquals(expected, actual);

        resourcePath = Paths.get(this.getClass().getClassLoader().getResource("Invalid_Flowers.xml").toURI());
        XMLFileName = resourcePath.toString();

        // XML validator testing. Not valid XML file. Return error messages.
        flowers = flowerController.executeCommand("BUILD_FLOWERS_WITH_DOM:" + XMLFileName);
        expected = 0;
        actual = flowers.size();
        assertEquals(expected, actual);


        // File validator testing. Not such file. Return error messages.
        flowers = flowerController.executeCommand("BUILD_FLOWERS_WITH_DOM:" + "bad_file_name.xml");
        expected = 0;
        actual = flowers.size();
        assertEquals(expected, actual);
    }


}