package by.hrynhlaz.module3.controller;

import by.hrynhlaz.module3.controller.builder.FlowerDomBuilder;
import by.hrynhlaz.module3.entity.Flower;
import by.hrynhlaz.module3.repository.FlowerRepository;
import by.hrynhlaz.module3.repository.Repository;
import by.hrynhlaz.module3.service.BaseFlowerService;
import by.hrynhlaz.module3.service.FlowerModel;
import by.hrynhlaz.module3.service.FlowerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class FlowerDomBuilderTest {

    @Test
    public void buildFlower() throws Exception{
        Repository<Flower> flowerRepository = new FlowerRepository();
        BaseFlowerService<Flower> flowerService = new FlowerService(flowerRepository);
        FlowerDomBuilder builder = new FlowerDomBuilder(flowerService);
        Path resourcePath = Paths.get(this.getClass().getClassLoader().getResource("Valid_Flowers.xml").toURI());
        List<FlowerModel> flowers = builder.buildFlower(resourcePath.toString());
        int expected = 10;
        int actual = flowers.size();
        assertEquals(expected, actual);
    }
}