package by.hrynhlaz.module2.entity;

public class Sentence {
    private long sentenceId;
    private long paragraphId;

    public Sentence() {
        sentenceId = -1;
    }

    public long getSentenceId() {
        return sentenceId;
    }

    public void setSentenceId(long sentenceId) {
        this.sentenceId = sentenceId;
    }

    public long getParagraphId() {
        return paragraphId;
    }

    public void setParagraphId(long paragraphId) {
        this.paragraphId = paragraphId;
    }
}
