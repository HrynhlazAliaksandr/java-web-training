package by.training.hrynhlaz.maintenance.controller.command;

import by.training.hrynhlaz.maintenance.ConfigurationManager;
import by.training.hrynhlaz.maintenance.dto.UserAccountDto;
import by.training.hrynhlaz.maintenance.entity.UserProfile;
import by.training.hrynhlaz.maintenance.service.UserProfileService;
import by.training.hrynhlaz.maintenance.service.exception.ServiceException;
import by.training.hrynhlaz.maintenance.service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class AddUserProfileCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
        long result = -1;
        String page = null;
        ServiceFactory serviceFactory = ServiceFactory.getInstance();
        UserProfileService userProfileService = serviceFactory.getUserProfileService();
        HttpSession session = request.getSession();
        UserAccountDto user = (UserAccountDto) session.getAttribute("user");
        UserProfile currentProfile = new UserProfile();
        currentProfile.setUserId(user.getUserId());
        currentProfile.setFirstName(request.getParameter("firstName"));
        currentProfile.setLastName(request.getParameter("lastName"));
        currentProfile.setEmail(request.getParameter("email"));
        currentProfile.setPhoneNumber(request.getParameter("phoneNumber"));
        try {
            result = userProfileService.addProfile(currentProfile);
        } catch (ServiceException e) {
            //
        }

        if(result == -1) {
            page = ConfigurationManager.getProperty("path.page.login");
        } else {
            page = ConfigurationManager.getProperty("path.page.main");
        }

        return page;
    }
}






