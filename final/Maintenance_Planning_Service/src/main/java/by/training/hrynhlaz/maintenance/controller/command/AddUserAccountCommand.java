package by.training.hrynhlaz.maintenance.controller.command;

import by.training.hrynhlaz.maintenance.ConfigurationManager;
import by.training.hrynhlaz.maintenance.entity.UserAccount;
import by.training.hrynhlaz.maintenance.entity.UserProfile;
import by.training.hrynhlaz.maintenance.service.UserAccountService;
import by.training.hrynhlaz.maintenance.service.exception.ServiceException;
import by.training.hrynhlaz.maintenance.service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;

public class AddUserAccountCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        boolean result = false;
        String page = null;
        ServiceFactory serviceFactory = ServiceFactory.getInstance();
        UserAccountService userAccountService = serviceFactory.getUserAccountService();
        UserAccount newUser = new UserAccount();
        newUser.setLogin(request.getParameter("login"));
        newUser.setPassword(request.getParameter("password"));
        newUser.setRoleId(Long.parseLong(request.getParameter("role")));
        UserProfile newProfile = new UserProfile();
        newProfile.setFirstName(request.getParameter("firstName"));
        newProfile.setLastName(request.getParameter("lastName"));
        newProfile.setEmail(request.getParameter("email"));
        newProfile.setPhoneNumber(request.getParameter("phoneNumber"));

        try {
            result = userAccountService.addAccount(newUser, newProfile);
        } catch (ServiceException e) {
            //
        }

        if(result) {
            page = ConfigurationManager.getProperty("path.page.main");
        } else {
            page = ConfigurationManager.getProperty("path.page.main");
        }



        return page;
    }
}
