package by.hrynhlaz.module2.service;

import by.hrynhlaz.module2.entity.Sentence;
import by.hrynhlaz.module2.repository.SentenceRepository;
import by.hrynhlaz.module2.repository.Specification;
import java.util.List;

public class SentenceService implements BaseTextService<Sentence>{
    private SentenceRepository sentenceRepository;

    public SentenceService(SentenceRepository repository) {
        sentenceRepository = repository;
    }

    @Override
    public long create(Sentence entity) {
        return sentenceRepository.create(entity);
    }

    @Override
    public Sentence read(long id) {
        return sentenceRepository.read(id);
    }

    @Override
    public void update(Sentence entity) {
        sentenceRepository.update(entity);
    }

    @Override
    public void delete(long id) {
        sentenceRepository.delete(id);
    }

    @Override
    public List<Sentence> find(Specification<Sentence> spec) {
        return sentenceRepository.find(spec);
    }

    @Override
    public List<Sentence> getAll() {
        return sentenceRepository.getAll();
    }
}
