package by.hrynhlaz.module3.service;

public enum Parameter {
    NAME("name"),
    UNIT("unit"),
    VALUE("value");

    private String value;
    private Parameter(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
