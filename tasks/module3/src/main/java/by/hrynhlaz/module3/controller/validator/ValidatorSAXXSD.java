package by.hrynhlaz.module3.controller.validator;

import org.apache.log4j.Logger;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.xml.sax.SAXException;
import java.io.File;
import java.io.IOException;

public class ValidatorSAXXSD {
    private static final Logger LOGGER = Logger.getLogger(ValidatorSAXXSD.class);
    private ValidationResult validationResult = new ValidationResult();
    private String XSDSchemaName;

    public ValidatorSAXXSD(String XSDSchemaName) {
        this.XSDSchemaName = XSDSchemaName;
    }

    public ValidationResult validate(String XMLFileName) {
        String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        SchemaFactory factory = SchemaFactory.newInstance(language);
        File schemaLocation = new File(XSDSchemaName);
        try {
            Schema schema = factory.newSchema(schemaLocation);
            Validator validator = schema.newValidator();
            Source source = new StreamSource(XMLFileName);

            FlowerErrorHandler fh = new FlowerErrorHandler();
            validator.setErrorHandler(fh);

            validator.validate(source);

        } catch(SAXException e) {
            /*LOGGER.warn("validation "+ XMLFileName + " is not valid. "
                    + e.getMessage());*/
        } catch(IOException e) {
            LOGGER.error("File "+ XMLFileName + " does not exist. "
                    + e.getMessage());
        } finally {
            return validationResult;
        }
    }


    class FlowerErrorHandler extends DefaultHandler {
        public FlowerErrorHandler() throws IOException {

        }
        public void warning(SAXParseException e) {
            LOGGER.warn("-101-" + getLineAddress(e) + "-" + e.getMessage());
            validationResult.addResult(101, e.getMessage());
        }
        public void error(SAXParseException e) {
            LOGGER.warn("-102-" + getLineAddress(e) + "-" + e.getMessage());
            validationResult.addResult(102, e.getMessage());
        }
        public void fatalError(SAXParseException e) {
            LOGGER.warn("-103-" + getLineAddress(e) + "-" + e.getMessage());
            validationResult.addResult(103, e.getMessage());
        }
        private String getLineAddress(SAXParseException e) {
            return e.getLineNumber() + " : " + e.getColumnNumber();
        }
    }
}
