package by.training.hrynhlaz.maintenance.service;

import by.training.hrynhlaz.maintenance.dto.UserAccountDto;
import by.training.hrynhlaz.maintenance.dto.UserDto;
import by.training.hrynhlaz.maintenance.entity.UserAccount;
import by.training.hrynhlaz.maintenance.entity.UserProfile;
import by.training.hrynhlaz.maintenance.service.exception.ServiceException;

import java.util.List;

public interface UserAccountService {
    boolean updateAccount(UserAccount account) throws ServiceException;
    UserAccount findById(long id) throws ServiceException;
    UserAccountDto login(String login, String password) throws ServiceException;
    boolean deleteAccount(long id) throws ServiceException;
    boolean addAccount(UserAccount userAccount, UserProfile userProfile) throws ServiceException;
    List<UserDto> selectAll() throws ServiceException;
    void logout(String login) throws ServiceException;
}
