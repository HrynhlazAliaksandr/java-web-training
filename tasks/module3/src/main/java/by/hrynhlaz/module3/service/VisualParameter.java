package by.hrynhlaz.module3.service;

public enum VisualParameter {
    STEM_COLOR("stem_color"),
    LEAF_COLOR("leaf_color"),
    AVERAGE_PLANT_SIZE("average_plant_size");

    private String value;
    private VisualParameter(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}