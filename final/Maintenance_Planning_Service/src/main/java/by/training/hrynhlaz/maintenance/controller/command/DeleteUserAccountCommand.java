package by.training.hrynhlaz.maintenance.controller.command;

import by.training.hrynhlaz.maintenance.ConfigurationManager;
import by.training.hrynhlaz.maintenance.service.UserAccountService;
import by.training.hrynhlaz.maintenance.service.exception.ServiceException;
import by.training.hrynhlaz.maintenance.service.factory.ServiceFactory;
import javax.servlet.http.HttpServletRequest;

public class DeleteUserAccountCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        boolean result = false;
        String page = null;
        ServiceFactory serviceFactory = ServiceFactory.getInstance();
        UserAccountService userAccountService = serviceFactory.getUserAccountService();
        String inputParam = request.getParameter("userIdParam");
        long userId = Long.parseLong(inputParam);
        try {
            result = userAccountService.deleteAccount(userId);
        } catch (ServiceException e) {
            //
        }

        if(!result) {
            page = ConfigurationManager.getProperty("path.page.login");
        } else {
            page = ConfigurationManager.getProperty("path.page.main");
        }

        return page;
    }
}
