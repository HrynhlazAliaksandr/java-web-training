package by.training.hrynhlaz.model;

public class DockException extends Exception {
    public DockException() {
        super();
    }
    public DockException(String message, Throwable cause) {
        super(message, cause);
    }
    public DockException(Throwable cause) {
        super(cause);
    }
    public DockException(String message) {
        super(message);
    }
}
