package by.training.hrynhlaz.transport.controller.validator;

import java.util.*;

public class ValidationResult {
    private Map<Integer, String> errorMap = new HashMap<>();

    public void addErrMessage(int errCode, String errMessage) {
        errorMap.put(errCode, errMessage);
    }

    public boolean isValid() {
        return errorMap.isEmpty();
    }

    public List<String> getErrMessages() {
        List<String> errMessageList = new ArrayList<>();
        Set<Map.Entry<Integer, String>> set = errorMap.entrySet();

        for(Map.Entry<Integer, String> me : set) {
            errMessageList.add(me.getKey().toString() + " : " + me.getValue());
        }
        return errMessageList;
    }
}
