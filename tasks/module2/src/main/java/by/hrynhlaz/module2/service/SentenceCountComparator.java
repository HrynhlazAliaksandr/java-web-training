package by.hrynhlaz.module2.service;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;
import java.util.Comparator;

public class SentenceCountComparator implements Comparator<LinkedTextComposite> {

    @Override
    public int compare(LinkedTextComposite o1, LinkedTextComposite o2) {
        return Integer.compare(o1.leafCount(), o2.leafCount());
    }
}
