package by.hrynhlaz.module2.controller.loaderchain;

public interface LoaderChain<T> {
    T loadChain(long parentId);

    LoaderChain<T> linkWith(LoaderChain<T> next);
}
