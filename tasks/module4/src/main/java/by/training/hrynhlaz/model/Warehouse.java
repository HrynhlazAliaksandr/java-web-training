package by.training.hrynhlaz.model;

import org.apache.log4j.Logger;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Warehouse {
    private final static int CAPACITY = 10;
    private final static Logger LOGGER = Logger.getLogger(Warehouse.class);
    private Queue<Box> rack = new LinkedList<>();
    private Lock lock = new ReentrantLock();
    private Condition rackFullCondition = lock.newCondition();
    private Condition rackEmptyCondition = lock.newCondition();

    public void loadOnRack(Box box) throws InterruptedException {
        try {
            lock.lock();
            while (rack.size() == CAPACITY) {
                rackFullCondition.await();
            }
            rack.add(box);
            TimeUnit.MILLISECONDS.sleep(new Random().nextInt(500));
            LOGGER.info("Box #" + box.getBoxId() + " uploaded on rack");
            rackEmptyCondition.signalAll();
        } finally {
            lock.unlock();
        }
    }

    public Box UnloadFromRack() throws InterruptedException {
        try {
            lock.lock();
            while (rack.size() == 0) {
                rackEmptyCondition.await();
            }
            TimeUnit.MILLISECONDS.sleep(new Random().nextInt(300));
            return rack.poll();
        } finally {
            rackFullCondition.signalAll();
            lock.unlock();
        }
    }
}
