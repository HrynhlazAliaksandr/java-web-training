package by.hrynhlaz.module2.controller;

import by.hrynhlaz.module2.repository.ParagraphRepository;
import by.hrynhlaz.module2.repository.SentenceRepository;
import by.hrynhlaz.module2.repository.TextRepository;
import by.hrynhlaz.module2.repository.WordRepository;
import by.hrynhlaz.module2.service.*;
import by.hrynhlaz.module2.controller.validator.FileValidator;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class TextControllerTest {
    private static final Logger LOGGER = Logger.getLogger(TextControllerTest.class);
    private TextService textService = new TextService(new TextRepository());
    private ParagraphService paragraphService = new ParagraphService(new ParagraphRepository());
    private SentenceService sentenceService = new SentenceService(new SentenceRepository());
    private WordService wordService = new WordService(new WordRepository());
    private FacadeTextService facadeTextService = new FacadeTextService(
                    textService, paragraphService, sentenceService, wordService);
    private FileReader reader = new FileReader();
    private FileValidator validator = new FileValidator();
    private Path resourcePath;
    private TextController controller = new TextController(facadeTextService, reader, validator);
    private long newTextId;

    @Before
    public void prepareData() throws URISyntaxException {
        resourcePath = Paths.get(this.getClass().getClassLoader().getResource("Text.txt").toURI());
    }

    @Test
    public void saveTextFromFile() {
        LOGGER.info("-------- SAVING TEST ---------------");
        newTextId = controller.saveTextFromFile(resourcePath.toString());
        assertTrue(newTextId != -1);
        LOGGER.info("Saving new text to repository with id: " + newTextId);
    }

    @Test
    public void loadText() {
        LOGGER.info("-------- LOADING TEST ---------------");
        newTextId = controller.saveTextFromFile(resourcePath.toString());
        controller.loadText(newTextId);
        assertNotNull(facadeTextService.getActualTextModel());
    }

    @Test
    public void sort() {
        LOGGER.info("-------- SORTING TEST ---------------");
        newTextId = controller.saveTextFromFile(resourcePath.toString());
        controller.loadText(newTextId);
        controller.sort();
    }
}