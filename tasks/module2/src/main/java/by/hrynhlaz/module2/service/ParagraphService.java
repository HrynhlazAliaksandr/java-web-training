package by.hrynhlaz.module2.service;

import by.hrynhlaz.module2.entity.Paragraph;
import by.hrynhlaz.module2.repository.ParagraphRepository;
import by.hrynhlaz.module2.repository.Specification;
import java.util.List;

public class ParagraphService implements BaseTextService<Paragraph>{
    private ParagraphRepository paragraphRepository;

    public ParagraphService(ParagraphRepository repository) {
        paragraphRepository = repository;
    }

    @Override
    public long create(Paragraph entity) {
        return paragraphRepository.create(entity);
    }

    @Override
    public Paragraph read(long id) {
        return paragraphRepository.read(id);
    }

    @Override
    public void update(Paragraph entity) {
        paragraphRepository.update(entity);
    }

    @Override
    public void delete(long id) {
        paragraphRepository.delete(id);
    }

    @Override
    public List<Paragraph> find(Specification<Paragraph> spec) {
        return paragraphRepository.find(spec);
    }

    @Override
    public List<Paragraph> getAll() {
        return paragraphRepository.getAll();
    }
}
