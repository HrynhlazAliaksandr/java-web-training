package by.training.hrynhlaz.transport.controller;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class DataFileReader {
    private static final Logger LOGGER = Logger.getLogger(DataFileReader.class);

    public List<String> readData(String filename) {
        List<String> lines = null;
            try {
                lines = Files.readAllLines(Paths.get(filename));
            } catch(IOException e) {
                LOGGER.warn("\nCould not read the file.", e);
            }
        return lines;
    }
}
