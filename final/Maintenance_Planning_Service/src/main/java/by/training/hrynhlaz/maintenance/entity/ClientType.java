package by.training.hrynhlaz.maintenance.entity;

public enum ClientType {
    GUEST, USER, ADMINISTRATOR
}
