<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:directive.attribute name="fieldName" required="true" description="field name to display" />
<jsp:directive.attribute name="beanName" required="true" description="some object" />
<c:if test="${not empty fieldName and not empty beanName}">

    <div class="field">
        <c:set var="bean" value="${requestScope[beanName]}" />
        <c:set var="inpId" value="input_${beanName}_password" />

        <label class="label" for="${inpId}">
            <fmt:message key="${beanName}.password" />:
        </label>
        <div class="control has-icons-left ">
            <input class="input" type="password" name="${beanName}.password" />
            <span class="icon is-small is-left">
                    <i class="fas fa-lock"></i>
            </span>
        </div>
    </div>
</c:if>