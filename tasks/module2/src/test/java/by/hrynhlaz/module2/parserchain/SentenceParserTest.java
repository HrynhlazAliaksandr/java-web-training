package by.hrynhlaz.module2.parserchain;

import by.hrynhlaz.module2.service.composite.LinkedTextComposite;
import by.hrynhlaz.module2.service.composite.TextLeaf;
import by.hrynhlaz.module2.controller.parserchain.SentenceParser;
import by.hrynhlaz.module2.repository.ParagraphRepository;
import by.hrynhlaz.module2.repository.SentenceRepository;
import by.hrynhlaz.module2.service.ParagraphService;
import by.hrynhlaz.module2.service.SentenceService;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class SentenceParserTest {
    private static final Logger LOGGER = Logger.getLogger(SentenceParserTest.class);
    private ParagraphService paragraphService = new ParagraphService(new ParagraphRepository());
    private SentenceService sentenceService = new SentenceService(new SentenceRepository());

    @Test
    public void parseLine() {
        String inputText = "It has survived not only five centuries, but also the leap into electronic " +
                "typesetting, remaining essentially unchanged. It was popularised in the with the release " +
                "of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing " +
                "software like Aldus PageMaker including versions of Lorem Ipsum.\n";

        int actualCount = 0;
        int expectedCount = 2;

        SentenceParser sentenceParser = new SentenceParser(paragraphService, sentenceService);
        LinkedTextComposite text = sentenceParser.parseLine(inputText);
        for (TextLeaf element : text.getTextElements()) {

            actualCount++;
        }
        assertEquals("Count sentences in the paragraph: ", expectedCount, actualCount);
    }
}