package by.training.hrynhlaz.transport.service;

import by.training.hrynhlaz.transport.controller.CarriageController;
import by.training.hrynhlaz.transport.controller.validator.ValidationResult;
import by.training.hrynhlaz.transport.entity.Carriage;
import by.training.hrynhlaz.transport.repository.CarriageRepository;
import by.training.hrynhlaz.transport.repository.Repository;
import by.training.hrynhlaz.transport.repository.specification.VacantSeatsMoreThenSpecification;
import by.training.hrynhlaz.transport.service.comparator.PassengerCountComparator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class CarriageServiceTest {
    private Repository carriageRepository = new CarriageRepository();
    private Service carriageService = new CarriageService(carriageRepository);
    private CarriageController carriageController = new CarriageController(carriageService);
    private ValidationResult validationResult;

    @Before
    public void init() {
        ClassLoader classLoader = getClass().getClassLoader();
        String path =  new File(classLoader.getResource("correct_data.txt").getFile()).getAbsolutePath();
        carriageController.loadCarriagesFromFile(path);
    }

    @Test
    public void calculateTotalPassengerCount() {
        assertEquals("Total passenger count", 97,
                carriageService.calculateTotalPassengerCount());
    }

    @Test
    public void calculateTotalBaggageWeight() {
        assertEquals("Total Baggage weight", 90,
                carriageService.calculateTotalBaggageWeight(), 1);
    }

    @Test
    public void sort() {
        List<Carriage> carriageList
                = carriageService.sort(new PassengerCountComparator());
        int[] carriageNumbers = new int[carriageList.size()];
        if(!carriageList.isEmpty()) {
            int i = 0;
            for(Carriage carriage : carriageList) {
                carriageNumbers[i] = carriage.getCarNumber();
                i++;
            }
        }
        int[] extendsArray = {2, 1, 3};
        assertArrayEquals("Sort by passenger count", extendsArray, carriageNumbers);
    }

    @Test
    public void getCarriagesBySpec() {
        List<Carriage> carriageList
                = carriageService.getCarriagesBySpec(
                        new VacantSeatsMoreThenSpecification(20));
        int[] carriageNumbers = new int[carriageList.size()];
        if(!carriageList.isEmpty()) {
            int i = 0;
            for(Carriage carriage : carriageList) {
                carriageNumbers[i] = carriage.getCarNumber();
                i++;
            }
        }
        int[] extendsArray = {2, 3};
        assertArrayEquals("Find by VacantSeatsMoreThan specification",
                extendsArray, carriageNumbers);
    }
}