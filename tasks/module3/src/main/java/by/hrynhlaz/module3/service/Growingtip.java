package by.hrynhlaz.module3.service;

public enum Growingtip {
    TEMPERATURE("temperature"),
    LIGHTING("lighting"),
    WATERING_PER_WEEK("watering_per_week");

    private String value;
    private Growingtip(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}