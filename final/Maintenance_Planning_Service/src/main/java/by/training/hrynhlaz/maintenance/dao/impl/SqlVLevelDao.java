package by.training.hrynhlaz.maintenance.dao.impl;

import by.training.hrynhlaz.maintenance.dao.VLevelDao;
import by.training.hrynhlaz.maintenance.dao.exception.DaoException;
import by.training.hrynhlaz.maintenance.entity.UserProfile;
import by.training.hrynhlaz.maintenance.entity.VLevel;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SqlVLevelDao extends VLevelDao {

    private static final String SELECT_BY_ID_QUERY =
            "SELECT voltage_level_id, voltage_level_name, voltage_level_col, substation_id FROM voltage_level " +
                    "WHERE voltage_level_id = ?";
    private static final String INSERT_QUERY =
            "INSERT INTO voltage_level(voltage_level_name, voltage_level_col, substation_id) VALUES(?, ?, ?)";
    private static final String UPDATE_QUERY =
            "UPDATE voltage_level SET voltage_level_name = ?, voltage_level_col = ?, substation_id = ? WHERE voltage_level_id = ?";
    private static final String DELETE_QUERY =
            "DELETE FROM voltage_level WHERE voltage_level_id = ?";
    private static final String SELECT_ALL_QUERY =
            "SELECT voltage_level_id, voltage_level_name, voltage_level_col, substation_id FROM voltage_level";

    @Override
    public List<VLevel> findAll() throws DaoException {
        List<VLevel> result = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = this.connection.prepareStatement(SELECT_ALL_QUERY);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                VLevel entity = new VLevel();
                entity.setVLevelId(resultSet.getLong("voltage_level_id"));
                entity.setVLevelName(resultSet.getString("voltage_level_name"));
                entity.setVLevel(resultSet.getString("voltage_level_col"));
                entity.setSubstationId(resultSet.getLong("substation_id"));
                result.add(entity);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public Long create(VLevel entity) throws DaoException {
        long id = 0;
        PreparedStatement statement = null;
        try{
            statement = this.connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            int i = 0;
            statement.setString(++i, entity.getVLevelName());
            statement.setString(++i, entity.getVLevel());
            statement.setLong(++i, entity.getSubstationId());
            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if(generatedKeys.next()){
                id = generatedKeys.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return id;
    }

    @Override
    public boolean delete(long id) throws DaoException {
        PreparedStatement statement = null;
        boolean result = false;
        try {
            statement = connection.prepareStatement(DELETE_QUERY);
            statement.setLong(1, id);
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public boolean update(VLevel entity) throws DaoException {
        PreparedStatement statement = null;
        boolean result = false;
        try {
            statement = this.connection.prepareStatement(UPDATE_QUERY);
            int i = 0;
            statement.setString(++i, entity.getVLevelName());
            statement.setString(++i, entity.getVLevel());
            statement.setLong(++i, entity.getSubstationId());
            statement.setLong(++i, entity.getVLevelId());
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return result;
    }

    @Override
    public VLevel findById(Long id) throws DaoException {
        PreparedStatement statement = null;
        VLevel vLevel = null;
        try {
            statement = this.connection.prepareStatement(SELECT_BY_ID_QUERY);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()) {
                vLevel = new VLevel();
                vLevel.setVLevelId(resultSet.getLong("voltage_level_id"));
                vLevel.setVLevelName(resultSet.getString("voltage_level_name"));
                vLevel.setVLevel(resultSet.getString("voltage_level_col"));
                vLevel.setSubstationId(resultSet.getLong("substation_id"));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            close(statement);
        }
        return vLevel;
    }

    @Override
    public void close(Statement statement) throws DaoException {
        super.close(statement);
    }
}
