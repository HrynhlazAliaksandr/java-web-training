package by.training.hrynhlaz.maintenance.service;

import by.training.hrynhlaz.maintenance.entity.UserRole;
import by.training.hrynhlaz.maintenance.service.exception.ServiceException;

import java.util.List;

public interface UserRoleService {
    List<UserRole> findAll() throws ServiceException;
}
